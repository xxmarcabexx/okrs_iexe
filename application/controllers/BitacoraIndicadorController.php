<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("America/Mexico_City");

class BitacoraIndicadorController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('BitacoraIndicadorModel');
        $this->load->model('IndicadorModel');
        $this->load->model('AnexosIndicadoresModel');
        $this->load->model('UsuariosModel');
        $this->load->model('LoginModel');


        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('url_helper');
    }

    public function index()
    {
        $this->load->view('login');
    }


    public function insert()
    {
        $data = $this->input->post();
        $dataInsert = array(
            "idIndicador" => $data["idIndicador"],
            "descripcion" => $data["descripcion"],
            "ultimoAvance" => $data["ultimoAvance"],
            "avance" => $data["avance"],
            "user" => $data["user"],
            "aprobado" => $data["aprobado"],
        );
        $dataInsert['fecha'] = date('Y-m-d H:i');
        $idBitacoraIndicador = $this->BitacoraIndicadorModel->insert($dataInsert);
        $dataIndicador = $this->IndicadorModel->getById($data['idIndicador']);
        $avancePorcentaje = ($data['avance'] * 100) / $dataIndicador[0]->final;
        //$this->IndicadorModel->updateAvancePorcentaje($data['idIndicador'], $avancePorcentaje);
        if (isset($data['archivos'])) {
            foreach ($data['archivos'] as $anexos) {
                //$ane = explode(".", $anexos);
                $dataAsistencia = array(
                    "idBitacoraIndicador" => $idBitacoraIndicador,
                    "file" => $anexos,
                    "estatus" => 0,
                    "idIndicador" => $data["idIndicador"]
                );
                $this->AnexosIndicadoresModel->insert($dataAsistencia);
            }
        }
        echo $avancePorcentaje;
    }

    public function validaAprobado()
    {
        $idIndicador = $this->input->post('idIndicador');
        $response = $this->BitacoraIndicadorModel->validaAprobado($idIndicador);
        if (count($response) == 1) {
            #Nunca se ha metido
            if ($response[0]->aprobado == 0 || $response[0]->aprobado == 2) {
                #No esta aprobado no se puede hacer nada
                echo 0;
            } else {
                #Continuamos como normalmente
                echo 1;
            }
        } else if (count($response) == 0) {
            echo 1;
        }
    }


    public function getById()
    {
        $idBitacora = $data = $this->input->post('idBitacora');
        $response = $this->BitacoraIndicadorModel->getById($idBitacora);
        foreach ($response as $bit) {
            $anexos = $this->AnexosIndicadoresModel->getByIdKr($idBitacora);
            $bit->anexos = $anexos;
            $idIndicador  = $bit->idIndicador;
        }
        $r = $this->UsuariosModel->getByUser($response[0]->user);
        $response[0]->capturista = $r[0]->nombre;

        $dataIndi = $this->IndicadorModel->getById($idIndicador);
        $response[0]->tituloInd = $dataIndi[0]->nombreIndicador;

        echo json_encode($response);
    }

    public function getByIdIndicador()
    {
        $idIndicador = $data = $this->input->post('idIndicador');
        $dataBitacora = $this->BitacoraIndicadorModel->getByIdIndicador($idIndicador);
        if (count($dataBitacora) > 0) {
            foreach ($dataBitacora as $bitacora) {
                $indicadorResponse = $this->IndicadorModel->getById($bitacora->idIndicador);
                $bitacora->tituloIndicador = $indicadorResponse[0]->nombreIndicador;

                switch ($bitacora->aprobado) {
                    case 0:
                        $bitacora->aprobado = "Por autorizar";
                        break;
                    case 1:
                        $bitacora->aprobado = "Autorizado";
                        break;
                    case 2:
                        $bitacora->aprobado = "No autorizado";
                        break;
                    case 3:
                        $bitacora->aprobado = "Visto no autorizado";
                        break;
                }

                if (!isset($bitacora->motivo)) {
                    $bitacora->motivo = "------";
                }

                if (isset($bitacora->user)) {
                    $userResponse = $this->LoginModel->getByUser($bitacora->user);
                    $bitacora->user = $userResponse[0]->nombre . " " . $userResponse[0]->apellidoP . " " . $userResponse[0]->apellidoM;
                } else {
                    $bitacora->user = "------";
                }

                if (isset($bitacora->userNoAutorizo)) {
                    $userResponse = $this->LoginModel->getByUser($bitacora->userNoAutorizo);
                    $bitacora->userNoAutorizo = $userResponse[0]->nombre . " " . $userResponse[0]->apellidoP . " " . $userResponse[0]->apellidoM;
                } else {
                    $bitacora->userNoAutorizo = "------";
                }
                if (isset($bitacora->userAprobado)) {
                    $userResponse = $this->LoginModel->getByUser($bitacora->userAprobado);
                    $bitacora->userAprobado = $userResponse[0]->nombre . " " . $userResponse[0]->apellidoP . " " . $userResponse[0]->apellidoM;
                } else {
                    $bitacora->userAprobado = "------";
                }
                $dataAdjuntos = $this->AnexosIndicadoresModel->getByIdKr($bitacora->idBitacora);
                $bitacora->adjuntos = $dataAdjuntos;
            }
            echo json_encode($dataBitacora);
        } else {
            echo 0;
        }

    }

    public function aprobar()
    {
        $idBitacora = $data = $this->input->post('idBitacora');
        $userAprobado = $data = $this->input->post('userAprobado');
        $this->BitacoraIndicadorModel->aprobar($idBitacora, $userAprobado);
        echo $idBitacora;
    }


    public function valida()
    {

    }

    public function uploadFileTemp()
    {
        $upload_folder = 'pdfstempInd';
        foreach ($_FILES as $i) {
            $nombre_archivo = $i['name'];
            $ane = explode(".", $nombre_archivo);
            $tipo_archivo = $i['type'];
            $tamano_archivo = $i['size'];
            $tmp_archivo = $i['tmp_name'];
            $archivador = $upload_folder . '/' . $ane[0] . date("Y-m-d") . "." . $ane[1];
            move_uploaded_file($tmp_archivo, $archivador);
        }
        echo $ane[0] . date("Y-m-d") . "." . $ane[1];
    }

    public function borrarArchivoTemporal()
    {
        $archivo = $this->input->post('archivo');
        $upload_folder = 'pdfstempInd';
        unlink($upload_folder . '/' . $archivo);
    }

    public function rechazar()
    {
        $idBitacora = $this->input->post('idBitacora');
        $data = $this->input->post();
        $dataUpdate = array(
            "aprobado" => 2,
            "motivo" => $data['motivo'],
            "userNoAutorizo" => $data['userNoAutorizo'],
        );
        $this->BitacoraIndicadorModel->rechazar($idBitacora, $dataUpdate);
        #cambiamos los pdf a que sean visibles
        $this->AnexosIndicadoresModel->rechazar($idBitacora);
    }

    public function cancelado()
    {
        $idIndicador = $this->input->post('idIndicador');
        $response = $this->BitacoraIndicadorModel->cancelado($idIndicador);
        if (count($response) == 1) {
            #Nunca se ha metido
            if ($response[0]->aprobado == 2) {
                #No esta aprobado no se puede hacer nada
                $r = $this->UsuariosModel->getByUser($response[0]->user);
                $response[0]->userCancel = $r[0]->nombre." ".$r[0]->apellidoP." ".$r[0]->apellidoM;
                echo json_encode($response);
            } else {
                #Continuamos como normalmente
                echo 0;
            }
        } else if (count($response) == 0) {
            echo 0;
        }
    }

    public function validaCancelado()
    {
        $idBitacora = $this->input->post('idBitacoraEnvio');
        $response = $this->BitacoraIndicadorModel->validaCancelado($idBitacora);
        echo $response;
    }

}
