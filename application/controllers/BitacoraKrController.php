<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("America/Mexico_City");

class BitacoraKrController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('BitacoraKrModel');
        $this->load->model('LoginModel');
        $this->load->model('UsuariosModel');
        $this->load->model('KeyResultModel');
        $this->load->model('AnexosModel');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('url_helper');
        $this->load->library('session');

    }

    public function index()
    {
        $this->load->view('login');
    }


    public function insert()
    {
        $data = $this->input->post();
        if ($this->session->userdata('tipo') == 'admin' || $this->session->userdata('tipo') == 'superadmin') {
            $dataInsert = array(
                "idKeyResult" => $data["idKeyResult"],
                "descripcion" => $data["descripcion"],
                "ultimoAvance" => $data["ultimoAvance"],
                "avance" => $data["avance"],
                "user" => $data["user"],
                "aprobado" => 1,
            );
            $dataInsert['fecha'] = date('Y-m-d H:i');
            $this->BitacoraKrModel->insert($dataInsert);
            $this->KeyResultModel->updateAvance($data["idKeyResult"], $data["avance"]);
            $dataKr = $this->KeyResultModel->getById($data['idKeyResult']);
            if ($dataKr[0]->medicioncomienzo > 0) {
                $avancePorcentaje = (($data['avance'] - $dataKr[0]->medicioncomienzo) * 100) / ($dataKr[0]->medicionfinal - $dataKr[0]->medicioncomienzo);
            } elseif ($dataKr[0]->medicioncomienzo == 0) {
                $avancePorcentaje = ($data['avance'] * 100) / $dataKr[0]->medicionfinal;
            }
            $this->KeyResultModel->updateAvancePorcentaje($data["idKeyResult"], $avancePorcentaje);
            echo $avancePorcentaje;
        } else {
            $dataInsert = array(
                "idKeyResult" => $data["idKeyResult"],
                "descripcion" => $data["descripcion"],
                "ultimoAvance" => $data["ultimoAvance"],
                "avance" => $data["avance"],
                "user" => $data["user"],
                "aprobado" => $data["aprobado"],
            );
            $dataInsert['fecha'] = date('Y-m-d H:i');
            $idBitacoraKr = $this->BitacoraKrModel->insert($dataInsert);
            $dataIndicador = $this->KeyResultModel->getById($data['idKeyResult']);
            $avancePorcentaje = ($data['avance'] * 100) / $dataIndicador[0]->medicionfinal;
            $this->KeyResultModel->updateAvancePorcentaje($data['idKeyResult'], $avancePorcentaje);
            if (isset($data['archivos'])) {
                foreach ($data['archivos'] as $anexos) {
                    $dataAsistencia = array(
                        "idBitacoraKr" => $idBitacoraKr,
                        "file" => $anexos,
                        "estatus" => 0,
                        "idKr" => $data["idKeyResult"]
                    );
                    $this->AnexosModel->insert($dataAsistencia);
                }
            }
            echo $avancePorcentaje;
        }

    }


    public function uploadFileTemp()
    {
        $upload_folder = 'pdfstemp';
        foreach ($_FILES as $i) {
            $nombre_archivo = $i['name'];
            $ane = explode(".", $nombre_archivo);
            $tipo_archivo = $i['type'];
            $tamano_archivo = $i['size'];
            $tmp_archivo = $i['tmp_name'];
            $archivador = $upload_folder . '/' . $ane[0] . date("Y-m-d") . "." . $ane[1];
            move_uploaded_file($tmp_archivo, $archivador);
        }
        echo $ane[0] . date("Y-m-d") . "." . $ane[1];
    }

    public function borrarArchivoTemporal()
    {
        $archivo = $this->input->post('archivo');
        $upload_folder = 'pdfstemp';
        unlink($upload_folder . '/' . $archivo);
    }

    public function validaAprobado()
    {
        $idKr = $this->input->post('idKr');
        $response = $this->BitacoraKrModel->validaAprobado($idKr);
        if (count($response) == 1) {
            #Nunca se ha metido
            if ($response[0]->aprobado == 0 || $response[0]->aprobado == 2) {
                #No esta aprobado no se puede hacer nada
                echo 0;
            } else {
                #Continuamos como normalmente
                echo 1;
            }
        } else if (count($response) == 0) {
            echo 1;
        }
    }

    public function cancelado()
    {
        $idKr = $this->input->post('idKr');
        $response = $this->BitacoraKrModel->cancelado($idKr);
        if (count($response) == 1) {
            #Nunca se ha metido
            if ($response[0]->aprobado == 2) {
                #No esta aprobado no se puede hacer nada
                $r = $this->UsuariosModel->getByUser($response[0]->user);
                $response[0]->userCancel = $r[0]->nombre;
                echo json_encode($response);
            } else {
                #Continuamos como normalmente
                echo 0;
            }
        } else if (count($response) == 0) {
            echo 0;
        }
    }


    public function getById()
    {
        $idBitacora = $data = $this->input->post('idBitacora');
        $response = $this->BitacoraKrModel->getById($idBitacora);
        foreach ($response as $bit) {
            $anexos = $this->AnexosModel->getByIdKr($idBitacora);
            $bit->anexos = $anexos;
            $keyResult  = $bit->idKeyResult;
        }
        $dataKeyR = $this->KeyResultModel->getById($keyResult);
        $r = $this->UsuariosModel->getByUser($response[0]->user);
        $response[0]->capturista = $r[0]->nombre;
        $response[0]->tituloKr = $dataKeyR[0]->descripcion;
        echo json_encode($response);
    }

    public function aprobar()
    {
        $idBitacora = $data = $this->input->post('idBitacora');
        $this->BitacoraKrModel->aprobar($idBitacora);
        #cambiamos los pdf a que sean visibles
        $this->AnexosModel->aprobar($idBitacora);
        echo $idBitacora;
    }

    public function rechazar()
    {
        $idBitacora = $this->input->post('idBitacora');
        $data = $this->input->post();
        $dataUpdate = array(
            "aprobado" => 2,
            "motivo" => $data['motivo'],
            "userNoAutorizo" => $data['userNoAutorizo'],
        );
        $this->BitacoraKrModel->rechazar($idBitacora, $dataUpdate);
        #cambiamos los pdf a que sean visibles
        $this->AnexosModel->rechazar($idBitacora);
    }

    public function validaCancelado()
    {
        $idBitacora = $this->input->post('idBitacoraEnvio');
        $response = $this->BitacoraKrModel->validaCancelado($idBitacora);
        echo $response;
    }


    public function getByKr()
    {
        $idKeyResult = $data = $this->input->post('idKeyResult');
        $dataBitacora = $this->BitacoraKrModel->getByIdIk($idKeyResult);
        if (count($dataBitacora) > 0) {
            foreach ($dataBitacora as $bitacora) {
                /*$indicadorResponse = $this->IndicadorModel->getById($bitacora->idIndicador);
                $bitacora->tituloIndicador = $indicadorResponse[0]->nombreIndicador;*/
                $bitacora->tituloIndicador = "Ejemplo";

                switch ($bitacora->aprobado) {
                    case 0:
                        $bitacora->aprobado = "Por autorizar";
                        break;
                    case 1:
                        $bitacora->aprobado = "Autorizado";
                        break;
                    case 2:
                        $bitacora->aprobado = "No autorizado";
                        break;
                    case 3:
                        $bitacora->aprobado = "Visto no autorizado";
                        break;
                }

                if (!isset($bitacora->motivo)) {
                    $bitacora->motivo = "------";
                }

                if (isset($bitacora->user)) {
                    $userResponse = $this->LoginModel->getByUser($bitacora->user);
                    $bitacora->user = $userResponse[0]->nombre . " " . $userResponse[0]->apellidoP . " " . $userResponse[0]->apellidoM;
                } else {
                    $bitacora->user = "------";
                }

                if (isset($bitacora->userNoAutorizo)) {
                    $userResponse = $this->LoginModel->getByUser($bitacora->userNoAutorizo);
                    $bitacora->userNoAutorizo = $userResponse[0]->nombre . " " . $userResponse[0]->apellidoP . " " . $userResponse[0]->apellidoM;
                } else {
                    $bitacora->userNoAutorizo = "------";
                }
                if (isset($bitacora->userAprobado)) {
                    $userResponse = $this->LoginModel->getByUser($bitacora->userAprobado);
                    $bitacora->userAprobado = $userResponse[0]->nombre . " " . $userResponse[0]->apellidoP . " " . $userResponse[0]->apellidoM;
                } else {
                    $bitacora->userAprobado = "------";
                }

            }
            echo json_encode($dataBitacora);
        } else {
            echo 0;
        }

    }


}
