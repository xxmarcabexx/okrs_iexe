<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("America/Mexico_City");
class KeyResultController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('KeyResultModel');
        $this->load->model('ObjetivosModel');
        $this->load->model('BitacoraKrModel');
        $this->load->model('PlanesModel');
        $this->load->model('AccionesModel');

        $this->load->model('ChatModel');
        $this->load->model('BitacoraMovimientosModel');


        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('url_helper');
        $this->load->library('session');


    }

    public function insert()
    {
        $data = $this->input->post();
        $data['estado'] = 1;
        $result = $this->KeyResultModel->insert($data);
        $data = array(
            'movimiento' => 'Alta de usuario',
            'usuario' => $this->session->userdata('idUser'),
            'fecha' => date('Y-m-d'),
            'hora' => date('H:i')
        );
        $this->BitacoraMovimientosModel->insert($data);
        echo ($result != null) ? $result : 0;
    }

    public function InsertMultiple($idObjetivo)
    {
        $data = $this->input->post();
        foreach ($data as $datos) {
            foreach ($datos as $d) {
                $dataInsert = array(
                    'idObjetivo' => $idObjetivo,
                    'descripcion' => $d['nombre'],
                    'metrica' => $d['medicion'],
                    'orden' => 'Ascendente',
                    'medicioncomienzo' => $d['vinicial'],
                    'medicionfinal' => $d['vfinal'],
                    'avance' => 0,
                    'estado' => 1,
                    'avancePorcentaje' => 0
                );
                $this->KeyResultModel->insert($dataInsert);
            }

        }
        echo 1;


    }

    public function add($idObjetivo)
    {
        if ($this->session->userdata('usuario') != null && $this->session->userdata('tipo') != 'capturista') {

            $response = $this->ElementosMenu();
            $dataObj = $this->ObjetivosModel->getById($idObjetivo);
            $data = array(
                'idObjetivo' => $idObjetivo,
                'planes' => $response,
                'admin' => false,
                'nObjetivo' => $dataObj[0]->mv
            );
            $this->load->view('agrega_kr', $data);
        } else {
            redirect(base_url());
        }
    }

    public function getByObjetivos()
    {
        $data = $this->input->post('idObjetivo');
        $result = $this->KeyResultModel->getByObjetivos($data);
        echo json_encode($result);
    }

    public function getById()
    {
        $data = $this->input->post('idKr');
        $result = $this->KeyResultModel->getById($data);
        echo json_encode($result);
    }

    public function getObjetivoByIdKr()
    {
        $idKr = $this->input->post('idKeyResult');
        $result = $this->KeyResultModel->getById($idKr);
        $idObjetivo = $result[0]->idObjetivo;
        $dataObjetivo = $this->ObjetivosModel->getById($idObjetivo);
        echo json_encode($dataObjetivo);
    }

    public function detalle($idKey)
    {
        $response = $this->ElementosMenu();
        $dataKeyResult = $this->KeyResultModel->getById($idKey);
        $dataBitacora = $this->BitacoraKrModel->getByIdKr($idKey);
        $dataChat = $this->ChatModel->getChatByIdKey($idKey);


        $data = array(
            'admin' => true,
            'chat' => $dataChat,
            'planes' => $response,
            'keyresult' => $dataKeyResult,
            'bitacoraKr' => $dataBitacora
        );
        $this->load->view('detalle_kr', $data);
    }

    public function detalleKrPrincipal($idKey)
    {
        $response = $this->ElementosMenu();
        $dataObjetivo = $this->KeyResultModel->getById($idKey);
        $dataKeyResult = $this->AccionesModel->getByIdKr($idKey);


        #A objetivos lo vamos a poner como la kr y a key result com olas acciones
        $data = array(
            'admin' => true,
            'planes' => $response,
            'objetivo' => $dataObjetivo,
            'keyresult' => $dataKeyResult,
            'mv' => null
        );

        $this->load->view('detalle_krAnual', $data);
    }

    public function edit()
    {
        $sumaPorcentajes = 0;
        $datos = $this->input->post('objeto');
        if($datos['medicioncomienzo']>0){
            $avancePorcentaje = (($datos['avance']-$datos['medicioncomienzo']) * 100) / ($datos['medicionfinal']- $datos['medicioncomienzo']);
        }elseif($datos['medicioncomienzo']== 0){
            $avancePorcentaje = ($datos['avance'] * 100) / $datos['medicionfinal'];
        }
        $datos['avancePorcentaje'] = $avancePorcentaje;
        $idKre = $datos['idKeyResult'];
        unset($datos['idKerResult']);
        $this->KeyResultModel->edita($datos, $idKre);
        $idObjetivo = $this->KeyResultModel->getObjetivoById($idKre);
        $dataKr = $this->KeyResultModel->getByObjetivos($idObjetivo[0]->idObjetivo);
        foreach ($dataKr as $kr) {
            $sumaPorcentajes += $kr->avancePorcentaje;
        }
        $porcentajeObjetivo = $sumaPorcentajes / count($dataKr);
        $dataObjetivo = array(
            "avancePorcentaje" => $porcentajeObjetivo
        );

        $this->ObjetivosModel->update($idObjetivo[0]->idObjetivo, $dataObjetivo);
        $response = array(
            "avancePorcentaje" => $avancePorcentaje,
            "idObjetivo" => $idObjetivo[0]->idObjetivo,
            "porcentajeObjetivo" => $porcentajeObjetivo
        );
        $dataBitacora = array(
            "movimiento"=>'Se edito la key result',
            "usuario"=> $this->session->userdata('idUser'),
            "fecha"=> date("Y-m-d"),
            "hora"=> date("H:i")

        );
        $this->BitacoraMovimientosModel->insert($dataBitacora);
        echo json_encode($response);
    }

    public function editarAvance()
    {
        $data = $this->input->post();
        $response = $this->KeyResultModel->updateAvance($data['idKeyResult'], $data['avance']);
        /*aqui vamos a hacer todo el proceso sobre la actualizacion del objetivo y plan*/
        $KeyResul = $this->KeyResultModel->getById($data['idKeyResult']);
        $dataKeyResult = $this->KeyResultModel->getByObjetivos($KeyResul[0]->idObjetivo);
        $progresoIndividual = 0;
        foreach ($dataKeyResult as $kr) {

            if ($kr->metrica != 'Porcentaje') {
                $progreso = (($kr->avance) * 100) / $kr->medicionfinal;
            } else {
                $progreso = $kr->avance;
            }
            $progresoIndividual = $progresoIndividual + $progreso;
        }

        $progresoIndividual = ($progresoIndividual) / count($dataKeyResult);
        $data = array(
            'avance' => $progresoIndividual
        );

        $this->ObjetivosModel->update($KeyResul[0]->idObjetivo, $data);


        ///////////////////////////////////////////////////
        $objetivo = $this->ObjetivosModel->getById($KeyResul[0]->idObjetivo);

        $dataObjetivo = $this->ObjetivosModel->getObjetivosByPlan($objetivo[0]->idmv);
        $progresoIndividual = 0;
        foreach ($dataObjetivo as $obj) {
            $progreso = $obj->avance;
            $progresoIndividual = $progresoIndividual + $progreso;
        }
        $progresoIndividual = $progresoIndividual / count($dataObjetivo);
        $data = array(
            'avance' => $progresoIndividual
        );
        var_dump($data);
        $this->PlanesModel->update($dataObjetivo[0]->idmv, $data);
        echo 1;
    }


    #Funciones independientes
    public function ElementosMenu()
    {
        $dataPlanes = $this->PlanesModel->get();
        foreach ($dataPlanes as $planes) {
            #Hacemos consulta sobre las key result de ese objetivo
            $dataObjetivos = $this->ObjetivosModel->getObjetivosByPlan($planes->idMv);
            $planes->objetivos = $dataObjetivos;
            foreach ($dataObjetivos as $objetivos) {
                $dataKeyResult = $this->KeyResultModel->getByObjetivos($objetivos->idObjetivo);
                $objetivos->kr = $dataKeyResult;
            }
        }
        return $dataPlanes;
    }


    public function elimina($idKr)
    {
        $sumaPorcentajes = 0;
        $data = $this->input->post();
        $idObjetivo = $this->KeyResultModel->getObjetivoById($idKr);

        $this->KeyResultModel->deleteById($idKr, $data);



        $dataKr = $this->KeyResultModel->getByObjetivos($idObjetivo[0]->idObjetivo);
        foreach ($dataKr as $kr) {
            $sumaPorcentajes += $kr->avancePorcentaje;
        }
        $porcentajeObjetivo = $sumaPorcentajes / count($dataKr);
        $dataObjetivo = array(
            "avancePorcentaje" => $porcentajeObjetivo
        );

        $this->ObjetivosModel->update($idObjetivo[0]->idObjetivo, $dataObjetivo);
        $dataResponse = array(
            "idObjetivo"=> $idObjetivo[0]->idObjetivo,
            "porcentajeObjetivo" =>$porcentajeObjetivo
        );
        echo json_encode($dataResponse);

    }


}
