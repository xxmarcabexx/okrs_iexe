<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("America/Mexico_City");

class MinutasController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("PlanesModel");
        $this->load->model("AcuerdosModel");
        $this->load->model('BitacoraAcuerdoModel');
        $this->load->model('UsuariosPlanesModel');

        $this->load->model("ObjetivosModel");
        $this->load->model('KeyResultModel');
        $this->load->model('UsuariosModel');

        $this->load->model('MinutasModel');
        $this->load->model('PdfMinutaModel');


        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('url_helper');
        $this->load->library('session');
    }

    public function index()
    {
        if ($this->session->userdata('usuario') != '' || $this->session->userdata('usuario') != NULL) {
            $tipo = $this->session->userdata('tipo');
            $dataUser = $this->UsuariosModel->getByUser($this->session->userdata('idUser'));
            if ($dataUser[0]->acuerdos == 1) {
                $dataPlan = $this->UsuariosPlanesModel->getPlanesByUser($dataUser[0]->user);
                if ($tipo == 'capturista') {
                    $arrayMinutas = array();
                    foreach ($dataPlan as $plan) {
                        $dataMinutas = $this->MinutasModel->getByIdPlan($plan->plan);
                        if (count($dataMinutas) > 0) {
                            foreach ($dataMinutas as $minutas) {
                                $dataPlanes = $this->PlanesModel->getById($minutas->idPlan);
                                $minutas->plan = $dataPlanes[0]->mv;
                                $dataAcuerdos = $this->AcuerdosModel->getByidMinutas($minutas->idMinuta);
                                $minutas->acuerdos = $dataAcuerdos;
                                if (count($dataAcuerdos) > 0) {
                                    foreach ($dataAcuerdos as $Ac) {
                                        #Verificamos si hay algun Acuerdo Con avance
                                        $dataBitacoraAcuerdo = $this->BitacoraAcuerdoModel->getOneByIdAcuerdo($Ac->idAcuerdo);
                                        $Ac->bitacora = $dataBitacoraAcuerdo;
                                    }
                                }
                                array_push($arrayMinutas, $minutas);
                            }
                        }
                    }
                    $data = array(
                        'minutas' => $arrayMinutas,
                    );
                    $this->load->view('lista_minutas', $data);
                } elseif ($tipo == 'superadmin') {
                    //$arrayMinutas = array();
                    //$dataPlan = $this->UsuariosPlanesModel->getPlanesByUser($dataUser[0]->user);
                    //foreach ($dataPlan as $plan) {
                    $dataMinutas = $this->MinutasModel->get();
                    foreach ($dataMinutas as $minutas) {
                        if ($minutas->idPlan != 0) {
                            $dataPlanes = $this->PlanesModel->getById($minutas->idPlan);
                            $minutas->plan = $dataPlanes[0]->mv;
                            $dataAcuerdos = $this->AcuerdosModel->getByidMinutas($minutas->idMinuta);
                            $minutas->acuerdos = $dataAcuerdos;
                            if (count($dataAcuerdos) > 0) {
                                foreach ($dataAcuerdos as $Ac) {
                                    #Verificamos si hay algun Acuerdo Con avance
                                    $dataBitacoraAcuerdo = $this->BitacoraAcuerdoModel->getOneByIdAcuerdo($Ac->idAcuerdo);
                                    $Ac->bitacora = $dataBitacoraAcuerdo;
                                }
                            }
                        }
                    }
                    //array_push($arrayMinutas, $minutas);
                    //}
                    $data = array(
                        'minutas' => $dataMinutas,
                    );
                    $this->load->view('lista_minutas', $data);
                } else {
                    $arrayMinutas = array();
                    $dataPlan = $this->UsuariosPlanesModel->getPlanesByUser($dataUser[0]->user);
                    foreach ($dataPlan as $plan) {
                        $dataMinutas = $this->MinutasModel->getByIdPlan($plan->plan);
                        if (count($dataMinutas) > 0) {
                            foreach ($dataMinutas as $minutas) {
                                $dataPlanes = $this->PlanesModel->getById($minutas->idPlan);
                                $minutas->plan = $dataPlanes[0]->mv;
                                $dataAcuerdos = $this->AcuerdosModel->getByidMinutas($minutas->idMinuta);
                                $minutas->acuerdos = $dataAcuerdos;
                                if (count($dataAcuerdos) > 0) {
                                    foreach ($dataAcuerdos as $Ac) {
                                        #Verificamos si hay algun Acuerdo Con avance
                                        $dataBitacoraAcuerdo = $this->BitacoraAcuerdoModel->getOneByIdAcuerdo($Ac->idAcuerdo);
                                        $Ac->bitacora = $dataBitacoraAcuerdo;
                                    }
                                }
                                array_push($arrayMinutas, $minutas);
                            }
                        }
                    }
                    $data = array(
                        'minutas' => $arrayMinutas,
                    );
                    $this->load->view('lista_minutas', $data);
                }
            } else {
                $data = array(
                    'minutas' => null
                );
                $this->load->view('lista_minutas', $data);

            }
        } else {
            redirect(base_url());
        }
    }

    public function gobernador()
    {
        $dataPlanes = $this->PlanesModel->get();

        $response = $this->ElementosMenu();


        $dataMinutas = $this->MinutasModel->get();
        foreach ($dataMinutas as $minutas) {
            $dataPlanes = $this->PlanesModel->getById($minutas->idPlan);
            $minutas->plan = $dataPlanes[0]->mv;

            $dataAcuerdos = $this->AcuerdosModel->getByidMinutas($minutas->idMinuta);
            $minutas->acuerdos = $dataAcuerdos;
        }
        $data = array(
            'planes' => $response,
            'planesGraf' => $dataPlanes,
            'minutas' => $dataMinutas
        );
        $this->load->view('lista_minutasGob', $data);
    }

    public function alta()
    {
        if ($this->session->userdata('usuario') != null && $this->session->userdata('tipo') != 'capturista') {
            $dataPlan = $this->PlanesModel->get();
            foreach ($dataPlan as $planes) {
                $dataObjetivos = $this->ObjetivosModel->getObjetivosByPlan($planes->idMv);
                $planes->objetivos = $dataObjetivos;
                foreach ($dataObjetivos as $objetivos) {
                    $dataKr = $this->KeyResultModel->getByObjetivos($objetivos->idObjetivo);
                    $objetivos->kr = $dataKr;
                }
            }
            $data = array(
                'planes' => $dataPlan
            );
            $this->load->view('nueva_minuta', $data);
        } else {
            redirect(base_url());
        }
    }

    public function upFile()
    {
        $upload_folder = 'pdfsminutas';
        foreach ($_FILES as $i) {
            $nombre_archivo = $i['name'];
            $ane = explode(".", $nombre_archivo);
            $tipo_archivo = $i['type'];
            $tamano_archivo = $i['size'];
            $tmp_archivo = $i['tmp_name'];
            $archivador = $upload_folder . '/' . $ane[0] . date("Y-m-d") . "." . $ane[1];
            move_uploaded_file($tmp_archivo, $archivador);
        }
        echo $ane[0] . date("Y-m-d") . "." . $ane[1];
    }


    public function deleteFile()
    {
        $archivo = $this->input->post('archivo');
        $upload_folder = 'pdfsminutas';
        unlink($upload_folder . '/' . $archivo);
    }

    public function deleteFileData()
    {
        $archivo = $this->input->post('archivo');
        $upload_folder = 'pdfsminutas';
        //unlink($upload_folder.'/'.$archivo);
        $this->PdfMinutaModel->deletByPdf($archivo);
    }

    public function insert()
    {
        $dataInsert = $this->input->post();
        $dataInsert['hora'] = str_replace(' ', '', $dataInsert['hora']);
        $porciones = explode(":", $dataInsert['hora']);
        $dataInsert['hora'] = $porciones[0] . ":" . $porciones[1];

        $porciones = explode("/", $dataInsert['fecha']);
        $dataInsert['fecha'] = $porciones[2] . "-" . $porciones[1] . "-" . $porciones[0];
        if (isset($dataInsert['acuerdos'])) {
            $acuerdos = $dataInsert['acuerdos'];
            $banderaAcuerdo = 1;
        } else {
            $banderaAcuerdo = 0;
        }
        if (isset($dataInsert['anexsos'])) {
            $anexos = $dataInsert['anexsos'];
            $banderaAnexo = 1;
        } else {
            $banderaAnexo = 0;
        }

        unset($dataInsert['acuerdos']);
        unset($dataInsert['anexsos']);
        $dataInsert['status'] = 1;

        $dataMInutas = $this->MinutasModel->insert($dataInsert);

        if ($banderaAcuerdo == 1)
            for ($i = 0; $i < count($acuerdos); $i++) {
                $fechaAcuerdos = $acuerdos[$i]['fecha'];
                $fechaAcuerdos = explode("/", $fechaAcuerdos);
                $fechaAcuerdos = $fechaAcuerdos[2] . "-" . $fechaAcuerdos[1] . "-" . $fechaAcuerdos[0];

                $dataAcuerdos = array(
                    "idMInuta" => $dataMInutas,
                    "actividad" => $acuerdos[$i]['actividad'],
                    "responsable" => $acuerdos[$i]['responsable'],
                    "fecha" => $fechaAcuerdos,
                    "fechaAlta" => date("Y-m-d"),
                    "avance" => 0,
                    "status" => 1
                );
                $this->AcuerdosModel->insert($dataAcuerdos);

            }

        if ($banderaAnexo == 1)
            for ($i = 0; $i < count($anexos); $i++) {
                $dataAnexos = array(
                    "idMInuta" => $dataMInutas,
                    "pdf" => $anexos[$i]
                );
                $this->PdfMinutaModel->insert($dataAnexos);
            }
        echo 1;

    }

    public function deleteAcuerdo()
    {
        $idAcuerdo = $this->input->post('idAcuerdo');
        $this->AcuerdosModel->deleteByIdAcuerdo($idAcuerdo);
        echo 1;
    }

    public function edita($idMinuta)
    {
        if ($this->session->userdata('usuario') != null && $this->session->userdata('tipo') != 'capturista') {
            $dataPlan = $this->PlanesModel->get();
            $dataMinuta = $this->MinutasModel->getByIdEdit($idMinuta);
            $dataPdfMinutas = $this->PdfMinutaModel->getByIdMinuta($idMinuta);
            $dataAcuerdos = $this->AcuerdosModel->getByidMinutas($idMinuta);
            $data = array(
                'planes' => $dataPlan,
                'minuta' => $dataMinuta,
                'pdf' => $dataPdfMinutas,
                'acuerdos' => $dataAcuerdos
            );
            $this->load->view('edita_minuta', $data);
        } else {
            redirect(base_url());
        }

    }


    public function elimina($idMinuta)
    {
        echo $idMinuta;
        $response = $this->MinutasModel->delete($idMinuta);
        echo $response;
    }

    public function edit($idMinuta)
    {
        $this->PdfMinutaModel->deleteAllByIdMunuta($idMinuta);
        $this->AcuerdosModel->deleteAllByIdMunuta($idMinuta);
        $this->MinutasModel->deleteAllByIdMinuta($idMinuta);
        $dataInsert = $this->input->post();
        $dataInsert['hora'] = str_replace(' ', '', $dataInsert['hora']);
        $porciones = explode(":", $dataInsert['hora']);
        $dataInsert['hora'] = $porciones[0] . ":" . $porciones[1];

        $porciones = explode("/", $dataInsert['fecha']);

        $dataInsert['fecha'] = $porciones[2] . "-" . $porciones[1] . "-" . $porciones[0];
        if (isset($dataInsert['acuerdos'])) {
            $acuerdos = $dataInsert['acuerdos'];
            $banderaAcuerdo = 1;
        } else {
            $banderaAcuerdo = 0;
        }
        if (isset($dataInsert['anexsos'])) {
            $anexos = $dataInsert['anexsos'];
            $banderaAnexo = 1;
        } else {
            $banderaAnexo = 0;
        }

        unset($dataInsert['acuerdos']);
        unset($dataInsert['anexsos']);
        $dataInsert['status'] = 1;
        $dataMInutas = $this->MinutasModel->insert($dataInsert);


        if ($banderaAcuerdo == 1)
            for ($i = 0; $i < count($acuerdos); $i++) {
                $fechaAcuerdos = $acuerdos[$i]['fecha'];
                $fechaAcuerdos = explode("/", $fechaAcuerdos);
                $fechaAcuerdos = $fechaAcuerdos[2] . "-" . $fechaAcuerdos[1] . "-" . $fechaAcuerdos[0];

                $dataAcuerdos = array(
                    "idMInuta" => $dataMInutas,
                    "actividad" => $acuerdos[$i]['actividad'],
                    "responsable" => $acuerdos[$i]['responsable'],
                    "fecha" => $fechaAcuerdos,
                    "fechaAlta" => date("Y-m-d"),
                    "avance" => $acuerdos[$i]['avance'],
                    "status" => 1
                );
                $this->AcuerdosModel->insert($dataAcuerdos);

            }

        if ($banderaAnexo == 1)
            for ($i = 0; $i < count($anexos); $i++) {
                $dataAnexos = array(
                    "idMInuta" => $dataMInutas,
                    "pdf" => $anexos[$i]
                );
                $this->PdfMinutaModel->insert($dataAnexos);
            }
        echo $dataMInutas;
    }

    public function getPdfByIdMinuta()
    {
        $dataMInuta = $this->input->post("idMinuta");
        $dataPdfMinutas = $this->PdfMinutaModel->getByIdMinuta($dataMInuta);
        echo json_encode($dataPdfMinutas);

    }

    public function ElementosMenu()
    {
        $dataPlanes = $this->PlanesModel->get();
        foreach ($dataPlanes as $planes) {
            #Hacemos consulta sobre las key result de ese objetivo
            $dataObjetivos = $this->ObjetivosModel->getObjetivosByPlan($planes->idMv);
            $planes->objetivos = $dataObjetivos;
            foreach ($dataObjetivos as $objetivos) {
                $dataKeyResult = $this->KeyResultModel->getByObjetivos($objetivos->idObjetivo);
                $objetivos->kr = $dataKeyResult;
            }
        }
        return $dataPlanes;
    }


}
