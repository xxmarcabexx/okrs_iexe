<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ObjetivosController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('MvModel');
        $this->load->model('AccionesModel');

        $this->load->model('ObjetivosModel');
		$this->load->model('UsuariosKrModel');
		$this->load->model('BitacoraMovimientosModel');
        $this->load->model('BitacoraAccionesModel');


        $this->load->model('KeyResultModel');
		$this->load->model('BitacoraKrModel');
		$this->load->model('PlanesModel');
		$this->load->model('ChatModel');

		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('url_helper');
		$this->load->library('session');

	}

	public function index()
	{
		#obtenemos los kr que le corresponden solamente
		if ($this->session->userdata('usuario') != '' || $this->session->userdata('usuario') != NULL) {
			$tipo = $this->session->userdata('tipo');
			if ($tipo == 'capturista') {
			    $krArray = array();
				$us = $this->session->userdata('idUser');
				$kruser = $this->UsuariosKrModel->getKrByUser($us);
				foreach ($kruser as $kru) {
					$kr = $kru->kr;
					$response  = $this->KeyResultModel->getObjetivoById($kr);
                    $arrayObjetivos[] = $response[0]->idObjetivo;
				}
                if(isset($arrayObjetivos))
                $arrayObjetivos = array_unique($arrayObjetivos);

                if(isset($arrayObjetivos)){
				    $objetivosUsuarios = array();
                    foreach ($arrayObjetivos as $obj) {
                        $dataObjetivos = $this->ObjetivosModel->getById($obj);
                        foreach ($dataObjetivos as $objetivos) {
                            $dataKeyResult = $this->KeyResultModel->getByObjetivos($objetivos->idObjetivo);
                            $dataChats = $this->ChatModel->getChatByIdObjetivo($objetivos->idObjetivo);
                            $objetivos->chat = $dataChats;

                            $progresoIndividual = 0;
                            if (count($dataKeyResult) > 0) {
                                foreach ($dataKeyResult as $kr) {
                                    if ($kr->metrica != 'Porcentaje') {
                                        $progreso = (($kr->avance) * 100) / $kr->medicionfinal;
                                    } else {
                                        $progreso = $kr->avance;
                                    }
                                    $kr->progreso = $progreso;
                                    $progresoIndividual = $progresoIndividual + $progreso;

                                    #Verificamos si hay algun Kr Con avance
                                    $dataBitacoraKr = $this->BitacoraKrModel->getOneByIdKr($kr->idKeyResult);
                                    $kr->bitacora = $dataBitacoraKr;
                                    $dataAcciones = $this->AccionesModel->getByIdKrActivos($kr->idKeyResult);
                                    if(count($dataAcciones)>0) {
                                        $kr->acciones = $dataAcciones;
                                    }
                                }

                                $progresoObjetivo = ($progresoIndividual * 100) / (count($dataKeyResult) * 100);
                                $objetivos->progreso = $progresoObjetivo;
                            } else {
                                $objetivos->progreso = 0;
                            }
                            $objetivos->kr = $dataKeyResult;
                            $objetivosUsuarios[] = $objetivos;
                        }
                    }

                    $data = array(
                        'objetivos' => $objetivosUsuarios,
                    );
			    }else{
				    $data = array(
				        'objetivos' => null
                    );
                }
			} elseif ($tipo == 'superadmin') {
				$dataObjetivos = $this->ObjetivosModel->get();
				$totalActualizar = 0;
				foreach ($dataObjetivos as $objetivos) {
					$dataKeyResult = $this->KeyResultModel->getByObjetivos($objetivos->idObjetivo);
					$dataChats = $this->ChatModel->getChatByIdObjetivo($objetivos->idObjetivo);

					$dataTotal = $this->ObjetivosModel->totalActualiza($objetivos->idObjetivo);
					$totalActualizar += count($dataTotal);
					//var_dump($dataChats);
					$objetivos->chat = $dataChats;
					$progresoIndividual = 0;
					if (count($dataKeyResult) > 0) {
						foreach ($dataKeyResult as $kr) {
							$dataBitacoraKr = $this->BitacoraKrModel->getOneByIdKr($kr->idKeyResult);
							$kr->bitacora = $dataBitacoraKr;

                            $dataAcciones = $this->AccionesModel->getByIdKrActivos($kr->idKeyResult);
                            if(count($dataAcciones)>0) {
                                $kr->acciones = $dataAcciones;
                            }
						}
						if(isset($kr->acciones)) {
                            foreach ($kr->acciones as $acciones) {
                                $dataBitacoraKrInterno = $this->BitacoraAccionesModel->getOneByIdKr($acciones->idAcciones);
                                $acciones->bitacoraA = $dataBitacoraKrInterno;
                            }
                        }
					} else {
						/*$objetivos->progreso = 0;*/
					}
					$objetivos->kr = $dataKeyResult;
					$objetivos->totalActualizar = $totalActualizar;
					$totalActualizar = 0;
				}
				$data = array(
					'objetivos' => $dataObjetivos,
				);
			}else if($tipo=='admin'){
                $dataObjetivos = $this->ObjetivosModel->getSuperAdmin($this->session->userdata('usuario'));
                $totalActualizar = 0;
                foreach ($dataObjetivos as $objetivos) {
                    $dataKeyResult = $this->KeyResultModel->getByObjetivos($objetivos->idObjetivo);
                    $dataChats = $this->ChatModel->getChatByIdObjetivo($objetivos->idObjetivo);

                    $dataTotal = $this->ObjetivosModel->totalActualiza($objetivos->idObjetivo);
                    $totalActualizar += count($dataTotal);
                    //var_dump($dataChats);
                    $objetivos->chat = $dataChats;
                    $progresoIndividual = 0;
                    if (count($dataKeyResult) > 0) {
                        foreach ($dataKeyResult as $kr) {
                            $dataBitacoraKr = $this->BitacoraKrModel->getOneByIdKr($kr->idKeyResult);
                            $kr->bitacora = $dataBitacoraKr;

                            $dataAcciones = $this->AccionesModel->getByIdKrActivos($kr->idKeyResult);
                            if(count($dataAcciones)>0) {
                                $kr->acciones = $dataAcciones;
                            }
                        }
                        if(isset($kr->acciones)) {
                            foreach ($kr->acciones as $acciones) {
                                $dataBitacoraKrInterno = $this->BitacoraAccionesModel->getOneByIdKr($acciones->idAcciones);
                                $acciones->bitacoraA = $dataBitacoraKrInterno;
                            }
                        }
                    } else {
                        /*$objetivos->progreso = 0;*/
                    }
                    $objetivos->kr = $dataKeyResult;
                    $objetivos->totalActualizar = $totalActualizar;
                    $totalActualizar = 0;
                }
                $data = array(
                    'objetivos' => $dataObjetivos,
                );
            }
			$this->load->view('lista_objetivos', $data);
		}
	}

	public function alta()
	{
		if ($this->session->userdata('usuario') != null && $this->session->userdata('tipo') != 'capturista') {
			$dataMv = $this->MvModel->get();
			$data = array(
				'dataMv' => $dataMv,
				'admin' => false,
			);
			$this->load->view('nuevo_objetivo', $data);
		} else {
			redirect(base_url());
		}
	}

	public function edita($idObjetivo)
	{
		$dataMv = $this->MvModel->get();
		$dataObjetivo = $this->ObjetivosModel->getById($idObjetivo);

        $fi = explode("-", $dataObjetivo[0]->finicio);
        $dataObjetivo[0]->finicio = $fi[2]."/".$fi[1]."/".$fi[0];

        $ff = explode("-", $dataObjetivo[0]->ffinal);
        $dataObjetivo[0]->ffinal = $ff[2]."/".$ff[1]."/".$ff[0];

		$data = array(
			'dataMv' => $dataMv,
			'objetivo' => $dataObjetivo,
			'idObjetivo' => $idObjetivo,
			'admin' => false,
		);
		$this->load->view('edita_objetivo', $data);
	}

	public function elimina($idObjetivo)
	{
		$data = $this->input->post();
		$response = $this->ObjetivosModel->deleteById($idObjetivo, $data);
		echo $response;
	}


	public function insert()
	{
		$data = json_decode($this->input->post('obj'));
		$data->estado=1;

		$porciones = explode("/", $data->finicio);
		$data->finicio= $porciones[2]."-".$porciones[1]."-".$porciones[0];

		$porciones = explode("/", $data->ffinal);
		$data->ffinal= $porciones[2]."-".$porciones[1]."-".$porciones[0];

		$result = $this->ObjetivosModel->insert($data);
		$data = array(
			'movimiento' => 'Alta de objetivo',
			'usuario' => $this->session->userdata('idUser'),
			'fecha' => date('Y-m-d'),
			'hora' => date('H:i')
		);
		$this->BitacoraMovimientosModel->insert($data);
		echo ($result != null) ? $result : 0;
	}


	public function update($idObjetivo = null)
	{
		if (isset($idObjetivo)) {
			$data = json_decode($this->input->post('obj'));

			$porciones = explode("/", $data->finicio);
			$data->finicio= $porciones[2]."-".$porciones[1]."-".$porciones[0];

			$porciones = explode("/", $data->ffinal);
			$data->ffinal= $porciones[2]."-".$porciones[1]."-".$porciones[0];

			$result = $this->ObjetivosModel->update($idObjetivo, $data);
			echo ($result != 0) ? $idObjetivo : 0;
		}
	}

	public function detalle($idObjetivo)
	{
		if ($this->session->userdata('usuario') != null) {
			$response = $this->ElementosMenu();
			$dataObjetivo = $this->ObjetivosModel->getById($idObjetivo);
			$dataChat = $this->ChatModel->getChatByIdObjetivo($idObjetivo);

			$dataKeyResult = $this->KeyResultModel->getByObjetivos($idObjetivo);
			$dataMv = $this->MvModel->getById($dataObjetivo[0]->idmv);

			$data = array(
				'admin' => true,
				'chat' => $dataChat,
				'planes' => $response,
				'objetivo' => $dataObjetivo,
				'keyresult' => $dataKeyResult,
				'mv' => $dataMv
			);
			$this->load->view('detalle_objetivo', $data);
		} else {
			redirect(base_url());
		}
	}


	#Funciones independientes
	public function ElementosMenu()
	{
		$dataPlanes = $this->PlanesModel->get();
		foreach ($dataPlanes as $planes) {
			#Hacemos consulta sobre las key result de ese objetivo
			$dataObjetivos = $this->ObjetivosModel->getObjetivosByPlan($planes->idMv);
			$planes->objetivos = $dataObjetivos;
			foreach ($dataObjetivos as $objetivos) {
				$dataKeyResult = $this->KeyResultModel->getByObjetivos($objetivos->idObjetivo);
				$objetivos->kr = $dataKeyResult;
			}
		}
		return $dataPlanes;
	}


}
