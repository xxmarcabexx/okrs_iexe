<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("America/Mexico_City");

class UsuariosController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('PlanesModel');
        $this->load->model('ChatModel');
        $this->load->model('IndicadorModel');
        $this->load->model('BitacoraIndicadorModel');
        $this->load->model('BitacoraMovimientosModel');
        $this->load->model('UsuariosKrModel');
        $this->load->model('UsuariosPlanesModel');

        $this->load->model('UsuariosModel');
        $this->load->model('ObjetivosModel');
        $this->load->model('KeyResultModel');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('url_helper');
        $this->load->library('session');

    }

    public function index()
    {
        if ($this->session->userdata('usuario') != null && $this->session->userdata('tipo') == 'superadmin') {
            $dataUsuarios = $this->UsuariosModel->getActivos();
            $data = array(
                'usuarios' => $dataUsuarios,
            );

            $this->load->view('lista_usuarios', $data);
        } else {
            redirect(base_url());
        }
    }

    public function alta()
    {
        if ($this->session->userdata('usuario') != null && $this->session->userdata('tipo') == 'superadmin') {
            $dataPlan = $this->PlanesModel->get();
            foreach ($dataPlan as $planes) {
                $dataObjetivos = $this->ObjetivosModel->getObjetivosByPlan($planes->idMv);
                $planes->objetivos = $dataObjetivos;
                foreach ($dataObjetivos as $objetivos) {
                    $dataKr = $this->KeyResultModel->getByObjetivos($objetivos->idObjetivo);
                    $objetivos->kr = $dataKr;
                }
            }
            $data = array(
                'planes' => $dataPlan
            );
            $this->load->view('nuevo_usuario', $data);
        } else {
            redirect(base_url());
        }
    }

    public function getUserByClaveUser()
    {
        $data = $this->input->post();
        $response = $this->UsuariosModel->getUserByClaveUser($data);
        if (count($response) > 0) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function getUser()
    {
        $data = $this->input->post();
        $response = $this->UsuariosModel->getUser($data);
        if (count($response) > 0) {
            echo 1;
        } else {
            echo 0;
        }
    }


    public function insert()
    {
        $data = $this->input->post();
        $data['fecha'] = date('Y-m-d');
        $data['status'] = 1;
        $data['acuerdos']= intval($data['acuerdos']);
        if ($data['idRol'] == 4) {
            $capuristaKr = (isset($data['kr'])) ? $data['kr'] : null;
            $capuristaPl = (isset($data['planes'])) ? $data['planes'] : null;
            unset($data['planes']);
            unset($data['kr']);
            $this->UsuariosModel->insert($data);
            if (isset($capuristaKr) || $capuristaKr != '') {
                foreach ($capuristaKr as $ckr) {
                    $datackr = array(
                        'usuario' => $data['user'],
                        'kr' => $ckr
                    );
                    $this->UsuariosKrModel->insert($datackr);
                }
            }
            if (isset($capuristaPl) || $capuristaPl != '') {
                foreach ($capuristaPl as $cpl) {
                    $datackr = array(
                        'usuario' => $data['user'],
                        'plan' => $cpl
                    );
                    $this->UsuariosPlanesModel->insert($datackr);
                }
            }
        }elseif ($data['idRol'] == 3) {
            $capuristaPl = (isset($data['planes'])) ? $data['planes'] : null;
            unset($data['planes']);
            unset($data['kr']);
            $this->UsuariosModel->insert($data);

            if (isset($capuristaPl) || $capuristaPl != '') {
                foreach ($capuristaPl as $cpl) {
                    $datackr = array(
                        'usuario' => $data['user'],
                        'plan' => $cpl
                    );
                    $this->UsuariosPlanesModel->insert($datackr);
                }
            }
        } else {
            unset($data['planes']);
            unset($data['kr']);
            $this->UsuariosModel->insert($data);
        }

        $dataMovimiento = array(
            'movimiento' => 'Alta de usuario: ' . $data['user'],
            'usuario' => $this->session->userdata('idUser'),
            'fecha' => date('Y-m-d'),
            'hora' => date('H:i')
        );
        $this->BitacoraMovimientosModel->insert($dataMovimiento);

        echo 1;
    }

    public function update()
    {
        $data = $this->input->post();
        $data['acuerdos']= intval($data['acuerdos']);

        if ($data['idRol'] == 4) {
            $capuristaKr = (isset($data['kr'])) ? $data['kr'] : null;
            $capuristaPl = (isset($data['planes'])) ? $data['planes'] : null;
            unset($data['planes']);
            unset($data['kr']);
            $this->UsuariosModel->update($data, $data['user']);

            #eliminamos los kr anteriores
            $this->UsuariosKrModel->deleteByUser($data['user']);
            if (isset($capuristaKr) || $capuristaKr != '') {
                foreach ($capuristaKr as $ckr) {
                    $datackr = array(
                        'usuario' => $data['user'],
                        'kr' => $ckr
                    );
                    $this->UsuariosKrModel->insert($datackr);
                }
            }

            #eliminamos los planes anteriores
            $this->UsuariosPlanesModel->deleteByUser($data['user']);
            if (isset($capuristaPl) || $capuristaPl != '') {
                foreach ($capuristaPl as $cpl) {
                    $datackr = array(
                        'usuario' => $data['user'],
                        'plan' => $cpl
                    );
                    $this->UsuariosPlanesModel->insert($datackr);
                }
            }
        }elseif ($data['idRol'] == 3) {
            $capuristaPl = (isset($data['planes'])) ? $data['planes'] : null;
            unset($data['planes']);
            unset($data['kr']);
            $this->UsuariosModel->update($data, $data['user']);

            #eliminamos los planes anteriores
            $this->UsuariosPlanesModel->deleteByUser($data['user']);
            if (isset($capuristaPl) || $capuristaPl != '') {
                foreach ($capuristaPl as $cpl) {
                    $datackr = array(
                        'usuario' => $data['user'],
                        'plan' => $cpl
                    );
                    $this->UsuariosPlanesModel->insert($datackr);
                }
            }
        } else {
            unset($data['planes']);
            unset($data['kr']);
            $this->UsuariosKrModel->deleteByUser($data['user']);
            $this->UsuariosPlanesModel->deleteByUser($data['user']);
            $this->UsuariosModel->update($data, $data['user']);
        }

        $dataMovimiento = array(
            'movimiento' => 'Edicio de usuario: ' . $data['user'],
            'usuario' => $this->session->userdata('idUser'),
            'fecha' => date('Y-m-d'),
            'hora' => date('H:i')
        );
        $this->BitacoraMovimientosModel->insert($dataMovimiento);

        echo 1;
    }

    public function delete()
    {
        $data = $this->input->post();
        $response = $this->UsuariosModel->delete($data['user']);
        echo 1;
    }

    public function editar($user)
    {

        $data['user'] = urldecode($user);
        $dataUser = $this->UsuariosModel->getUser($data);
        if ($dataUser[0]->idRol == 4) {
            $responsePlanes = $this->UsuariosPlanesModel->getPlanesByUser($dataUser[0]->user);
            if (count($responsePlanes) > 0) {
                foreach ($responsePlanes as $planes) {
                    $dataPl[] = $this->PlanesModel->getById($planes->plan);
                }
                $dataUser[0]->planes = $dataPl;
            }

            $responseKr = $this->UsuariosKrModel->getKrByUser($dataUser[0]->user);
            if (count($responseKr) > 0) {
                foreach ($responseKr as $kr) {
                    $dataKr[] = $this->KeyResultModel->getById($kr->kr);
                }
                $dataUser[0]->kr = $dataKr;
            }
        }elseif ($dataUser[0]->idRol == 3) {
            $responsePlanes = $this->UsuariosPlanesModel->getPlanesByUser($dataUser[0]->user);
            if (count($responsePlanes) > 0) {
                foreach ($responsePlanes as $planes) {
                    $dataPl[] = $this->PlanesModel->getById($planes->plan);
                }
                $dataUser[0]->planes = $dataPl;
            }
        }


        $dataPlan = $this->PlanesModel->get();
        foreach ($dataPlan as $planes) {
            $dataObjetivos = $this->ObjetivosModel->getObjetivosByPlan($planes->idMv);
            $planes->objetivos = $dataObjetivos;
            foreach ($dataObjetivos as $objetivos) {
                $dataKr = $this->KeyResultModel->getByObjetivos($objetivos->idObjetivo);
                $objetivos->kr = $dataKr;
            }
        }

        $data = array(
            'user' => $dataUser,
            'planes' => $dataPlan
        );
        $this->load->view('edita_usuario', $data);
    }

    #Funciones independientes
    public function ElementosMenu()
    {
        $dataPlanes = $this->PlanesModel->get();
        foreach ($dataPlanes as $planes) {
            #Hacemos consulta sobre las key result de ese objetivo
            $dataObjetivos = $this->ObjetivosModel->getObjetivosByPlan($planes->idMv);
            $planes->objetivos = $dataObjetivos;
            foreach ($dataObjetivos as $objetivos) {
                $dataKeyResult = $this->KeyResultModel->getByObjetivos($objetivos->idObjetivo);
                $objetivos->kr = $dataKeyResult;
            }
        }
        return $dataPlanes;
    }


}
