<?php

class AccionesModel extends CI_Model
{
    public $tabla;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->tabla = "acciones";
    }

    public function getByIdAccion($idAccion){
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->where("idAcciones", $idAccion);
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }


    public function getTotalByIdKr($idKr){
        $this->db->select('COUNT(*) as total');
        $this->db->from($this->tabla);
        $this->db->where("idKr", $idKr);
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

    public function getByIdKr($idKr){
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->where("idKr", $idKr);
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

    public function getByIdKrActivos($idKr){
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->where("idKr", $idKr);
        $this->db->where("status", 1);
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

    public function insert($data)
    {
        if ($this->db->insert($this->tabla, $data))
            return true;
        else
            return null;
    }

    public function getAll(){
        $this->db->select('acciones.accion, objetivos.objetivo, objetivos.descripcion, keyresult.descripcion as descripcionKr, acciones.avance, acciones.idAcciones');
        $this->db->from($this->tabla);
        $this->db->join("keyresult", $this->tabla.".idKr = keyresult.idKeyResult");
        $this->db->join("objetivos", "objetivos.idObjetivo = keyresult.idObjetivo");
        $this->db->where("objetivos.anual", 1);
        $this->db->where("objetivos.estado", 1);
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

    public function updateAvance($idAccion, $avance){
        $this->db->set('avance', $avance);
        $this->db->where('idAcciones', $idAccion);
        $this->db->update($this->tabla);
        return 1;
    }

    public function deleteAccion($idAccion){
        $this->db->set('status', 0);
        $this->db->where('idAcciones', $idAccion);
        $this->db->update($this->tabla);
        return 1;
    }
}
?>