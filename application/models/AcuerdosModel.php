<?php

class AcuerdosModel extends CI_Model
{
	public $tabla;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->tabla = "acuerdos";
	}

	public function insert($data){
		if($this->db->insert($this->tabla, $data))
			return true;
		else
			return null;
	}

    public function getById($idAcuerdo){
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->where('idAcuerdo', $idAcuerdo);
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

	public function getByidMinutas($idMinuta){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('idMinuta', $idMinuta);
        $this->db->where('status', 1);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

    public function getByIdAcuerdos($idAcuerdo){
        $this->db->select('idMInuta');
        $this->db->from($this->tabla);
        $this->db->where('idAcuerdo', $idAcuerdo);
        $this->db->where('status', 1);
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

    public function deleteByIdAcuerdo($idAcuerdo){
        $this->db->set('status', '0', FALSE);
        $this->db->where('idAcuerdo', $idAcuerdo);
        $this->db->update($this->tabla);
    }
    public function deleteAllByIdMunuta($idMinuta){
        $this->db->where('idMinuta', $idMinuta);
        $this->db->delete($this->tabla);
    }

    public function updateAvance($idAcuerdo, $avance){
        $this->db->set('avance', $avance);
        $this->db->where('idAcuerdo', $idAcuerdo);
        $this->db->update($this->tabla);
        return 1;
    }






	/*public function aprobar($idBitacora){
		$this->db->set('estatus', '1', FALSE);
		$this->db->where('idBitacoraKr', $idBitacora);
		$this->db->update($this->tabla);
		return 1;
	}

	public function rechazar($idBitacora){
		$this->db->set('estatus', '2', FALSE);
		$this->db->where('idBitacoraKr', $idBitacora);
		$this->db->update($this->tabla);
		return 1;
	}*/
	/*public function get(){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->join('rol', "rol.idRol = ".$this->tabla.".idRol");
		$consulta = $this->db->get();
		$resultado = $consulta->result();

		return $resultado;
	}

	public function getUserByClaveUser($data){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('user', $data['user']);
		$this->db->where('clave', $data['clave']);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}*/

	/*public function getByObjetivos($idObjetivo){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('idObjetivo', $idObjetivo);
		$consulta = $this->db->get();
		$resultado = $consulta->result();

		return $resultado;
	}


	public function getById($idKr){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('idKeyResult', $idKr);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

	public function updateAvance($idKr, $avance){
		$this->db->set('avance', $avance);
		$this->db->where('idKeyResult', $idKr);
		$this->db->update($this->tabla);
		return 1;
	}*/



}
