<?php

class BitacoraIndicadorModel extends CI_Model
{
	public $tabla;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->tabla = "bitacoraindicadores";
	}

	public function insert($data){
		if($this->db->insert($this->tabla, $data))
			return $this->db->insert_id();
		else
			return null;
	}

	public function validaAprobado($idIndicador){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('idIndicador', $idIndicador);
		$this->db->order_by("fecha", "desc");
		$this->db->limit(1);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

	public function getByIdKr($idKr){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('idKeyResult', $idKr);
		$this->db->order_by("fecha", "desc");
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

	public function getById($idBitacora){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('idBitacora', $idBitacora);
		$this->db->order_by("fecha", "desc");
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

	public function getOneByIdIndicador($idInd){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('idIndicador', $idInd);
		//$this->db->where('aprobado', 0);
		$this->db->order_by("fecha", "desc");
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

    public function getByIdIndicador($idInd){
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->where('idIndicador', $idInd);
        $this->db->order_by("fecha", "desc");
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

	public function aprobar($idBitacora, $userAprobado){
		$this->db->set('aprobado', '1', FALSE);
        $this->db->set('userAprobado', $userAprobado);
		$this->db->where('idBitacora', $idBitacora);
		$this->db->update($this->tabla);
		return 1;
	}

public function rechazar($idBitacora, $data){
		$this->db->where('idBitacora', $idBitacora);
		$this->db->update($this->tabla, $data);
		return 1;
	}
	

	public function cancelado($idIndicador){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('idIndicador', $idIndicador);
		$this->db->order_by("fecha", "desc");
		$this->db->limit(1);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

public function validaCancelado($idBitacora){
		$this->db->set('aprobado', '3', FALSE);
		$this->db->where('idBitacora', $idBitacora);
		$this->db->update($this->tabla);
		return 1;
	}


}
