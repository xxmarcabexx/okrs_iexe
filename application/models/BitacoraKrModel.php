<?php

class BitacoraKrModel extends CI_Model
{
	public $tabla;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->tabla = "bitacorakeyresult";
	}

	public function insert($data){
		if($this->db->insert($this->tabla, $data))
			return $this->db->insert_id();
		else
			return null;
	}

	public function validaAprobado($idKr){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('idKeyResult', $idKr);
		$this->db->order_by("fecha", "desc");
		$this->db->limit(1);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}
	public function cancelado($idKr){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('idKeyResult', $idKr);
		$this->db->order_by("fecha", "desc");
		$this->db->limit(1);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

	public function getByIdKr($idKr){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('idKeyResult', $idKr);
		$this->db->order_by("fecha", "desc");
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

	public function getById($idBitacora){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('idBitacora', $idBitacora);
		$this->db->order_by("fecha", "desc");
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

	public function getOneByIdKr($idKr){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('idKeyResult', $idKr);
		//$this->db->where('aprobado', 0);
		//$this->db->or_where('aprobados', 2);
		$this->db->order_by("fecha", "desc");
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

	public function aprobar($idBitacora){
		$this->db->set('aprobado', '1', FALSE);
		$this->db->where('idBitacora', $idBitacora);
		$this->db->update($this->tabla);
		return 1;
	}

	public function rechazar($idBitacora, $data){
		$this->db->where('idBitacora', $idBitacora);
		$this->db->update($this->tabla, $data);
		return 1;
	}

	public function validaCancelado($idBitacora){
		$this->db->set('aprobado', '3', FALSE);
		$this->db->where('idBitacora', $idBitacora);
		$this->db->update($this->tabla);
		return 1;
	}

    public function getByIdIk($idKr){
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->where('idKeyResult', $idKr);
        $this->db->order_by("fecha", "desc");
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }



}
