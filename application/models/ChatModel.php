<?php

class ChatModel extends CI_Model
{
	public $tabla;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->tabla = "chat";
	}
	public function insert($data){
		if($this->db->insert($this->tabla, $data))
			return 1;
		else
			return null;
	}

	public function getChatByIdPlan($idPlan){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('idTipo', $idPlan);
		$this->db->where('tipo', 'plan');
        $this->db->where('status', 1);


        $consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

	public function getChatByIdObjetivo($idObjetivo){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->join('usuarios', 'usuarios.user = '.$this->tabla.".idUsuario");
		$this->db->where($this->tabla.'.idTipo', $idObjetivo);
		$this->db->where($this->tabla.'.tipo', 'objetivo');
        $this->db->where($this->tabla.'.status', 1);

        $consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

	public function getChatByIdKey($idKey){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('idTipo', $idKey);
		$this->db->where('tipo', 'kr');
        $this->db->where('status', 1);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

	public function delete($idTipo){
        $this->db->where('idTipo', $idTipo);
        $this->db->where('tipo', "objetivo");
        if($this->db->update($this->tabla, array('status' => 0)))
            return 1;
        else
            return 0;
    }





}
