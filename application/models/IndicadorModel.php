<?php

class IndicadorModel extends CI_Model
{
	public $tabla;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->tabla = "indicadores";
	}
	public function insert($data){
		if($this->db->insert($this->tabla, $data))
			return 1;
		else
			return null;
	}

	public function getIndicadoresByIdPlan($idPlan){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('idPlan', $idPlan);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

	public function getById($idIndicador){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('idIndicadores', $idIndicador);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

	public function updateAvance($idIndicdor, $avance, $porcentaje){
		$this->db->set('avance', $avance);
        $this->db->set('avancePorcentaje', $porcentaje);
		$this->db->where('idIndicadores', $idIndicdor);
		$this->db->update($this->tabla);
		return 1;
	}

	public function  updateAvancePorcentaje($idIndicdor, $avancePorcentaje){
		$this->db->set('avancePorcentaje', $avancePorcentaje);
		$this->db->where('idIndicadores', $idIndicdor);
		$this->db->update($this->tabla);
		return 1;
	}

	public function cambio($idIndicador, $data){
		$this->db->where('idIndicadores', $idIndicador);
		if($this->db->update($this->tabla, $data))
			return 1;
		else
			return 0;
	}
	public function ObtienePromedioByPlan($idPlan){
		$this->db->select("AVG(avancePorcentaje) as promedio");
		$this->db->from($this->tabla);
		$this->db->where("idPlan", $idPlan);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

	/*public function get(){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}



	*/

}
