<?php

class KeyResultModel extends CI_Model
{
	public $tabla;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->tabla = "keyresult";
	}

	public function get(){
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->where('estado', 1);
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

	public function insert($data){
		if($this->db->insert($this->tabla, $data))
			return 1;
		else
			return null;
	}

	public function getByObjetivos($idObjetivo){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('idObjetivo', $idObjetivo);
		$this->db->where('estado', 1);
		$consulta = $this->db->get();
		$resultado = $consulta->result();

		return $resultado;
	}

	public function edita($data, $idkr){
        $this->db->where('idKeyResult', $idkr);
        $this->db->update($this->tabla, $data);
        return 1;
    }
    public function getByIdObj($idObjetivo){
	    $this->db->select("*");
	    $this->db->from($this->tabla);
	    $this->db->where("idObjetivo", $idObjetivo);
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

    public function getByIdObjActivos($idObjetivo){
        $this->db->select("*");
        $this->db->from($this->tabla);
        $this->db->where("idObjetivo", $idObjetivo);
        $this->db->where("estado", 1);
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

	public function getById($idKr){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('idKeyResult', $idKr);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

	public function getObjetivoById($idKr){
		$this->db->select('idObjetivo');
		$this->db->from($this->tabla);
		$this->db->where('idKeyResult', $idKr);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

	public function updateAvance($idKr, $avance){
		$this->db->set('avance', $avance);
		$this->db->where('idKeyResult', $idKr);
		$this->db->update($this->tabla);
		return 1;
	}

	public function updateAvancePorcentaje($idKr, $avance){
		$this->db->set('avancePorcentaje', $avance);
		$this->db->where('idKeyResult', $idKr);
		$this->db->update($this->tabla);
		return 1;
	}

	public function deleteById($idKr){
		$this->db->where('idKeyResult', $idKr);
		if($this->db->update($this->tabla, array('estado' => 0)))
			return 1;
		else
			return 0;
	}


}
