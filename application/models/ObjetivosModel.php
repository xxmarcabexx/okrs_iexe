<?php

class ObjetivosModel extends CI_Model
{
	public $tabla;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->tabla = "objetivos";
	}

	public function get(){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('estado', 1);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}
	public function getSuperAdmin($idUser){
        $this->db->select("objetivos.idObjetivo, objetivos.anual, objetivos.idmv, objetivos.objetivo, objetivos.descripcion, objetivos.finicio, objetivos.ffinal, objetivos.avance, objetivos.estado, objetivos.avancePorcentaje");
        $this->db->from($this->tabla);
        $this->db->join("plan", $this->tabla.".idmv = plan.idMv");
        $this->db->join("usuarioplanes", "usuarioplanes.plan = plan.idMv");
        $this->db->where('usuarioplanes.usuario', $idUser);
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

	public function getById($idObjetivo){
		$this->db->select('objetivos.anual,plan.mv,objetivos.idObjetivo,objetivos.idmv,objetivos.objetivo,objetivos.descripcion,objetivos.finicio,objetivos.ffinal,objetivos.avance,objetivos.estado, objetivos.avancePorcentaje');
		$this->db->from($this->tabla);
		$this->db->join('plan', "plan.idMv = ".$this->tabla.".idMv");
		$this->db->where('objetivos.idObjetivo', $idObjetivo);
		$this->db->where('objetivos.estado', 1);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

	public function getObjetivosByPlan($plan){
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where('idmv', $plan);
		$this->db->where('estado', 1);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}

    public function getObjetivosByPlanNoAnual($plan){
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->where('idmv', $plan);
        $this->db->where('estado', 1);
        $this->db->where('anual', 0);
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

    public function getAnual($plan){
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->where('idmv', $plan);
        $this->db->where('estado', 1);
        $this->db->where('anual', 1);
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

    public function getAllAnual(){
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->where('estado', 1);
        $this->db->where('anual', 1);
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }



    public function insert($data){
		if($this->db->insert($this->tabla, $data))
			return $this->db->insert_id();
		else
			return null;
	}

	public function update($idObjetivo, $data){
		$this->db->where('idObjetivo', $idObjetivo);
		if($this->db->update($this->tabla, $data))
			return 1;
		else
			return 0;
	}
	public function deleteById($idObjetivo, $data){
		$this->db->where('idObjetivo', $idObjetivo);
		if($this->db->update($this->tabla, array('estado' => 0)))
			return 1;
		else
			return 0;
	}

	public function totalActualiza($idObjetivo){
		$this->db->select('objetivos.idmv, objetivos.objetivo, keyresult.idKeyResult, bitacorakeyresult.aprobado');
		$this->db->from($this->tabla);
		$this->db->join('keyresult', 'keyresult.idObjetivo = '.$this->tabla.'.idObjetivo');
		$this->db->join('bitacorakeyresult', "bitacorakeyresult.idKeyResult= keyresult.idKeyResult");
		$this->db->where('objetivos.idObjetivo', $idObjetivo);
		$this->db->where('bitacorakeyresult.aprobado', 0);
		$this->db->where('keyresult.estado', 1);
		$consulta = $this->db->get();
		$resultado = $consulta->result();
		return $resultado;
	}
}
