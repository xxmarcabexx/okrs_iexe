<!-- footer content -->
<footer>
	<div class="pull-right">
	</div>
	<div class="clearfix"></div>
</footer>
<!-- /footer content -->
</div>
</div>

<!-- jQuery -->
<script src="<?php echo base_url();?>assets/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url();?>assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?php echo base_url();?>assets/vendors/nprogress/nprogress.js"></script>

<script src="<?php echo base_url();?>assets/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>

<!-- Custom Theme Scripts -->
<script src="<?php echo base_url();?>assets/build/js/custom.min.js"></script>

<!--Chart-->
<script src="<?php echo base_url();?>assets/vendors/Chart.js/dist/Chart.min.js"></script>

<script src="<?php echo base_url();?>assets/vendors/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>


<script src="<?php echo base_url();?>assets/build/js/raphael-2.1.4.min.js"></script>
<script src="<?php echo base_url();?>assets/build/js/justgage.js"></script>


<script src="<?php echo base_url();?>assets/build/js/wickedpicker.js"></script>

</body>
</html>
