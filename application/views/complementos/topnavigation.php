
<div class="top_nav">
	<div class="nav_menu" style="background: #003A5D !important; color: white !important;">
		<nav>
			<div class="nav toggle">
				<a id="menu_toggle" style="color: white !important;"><i class="fa fa-bars"></i></a>
			</div>

			<ul class="nav navbar-nav navbar-right" style="color: white !important;">
				<li class="">
					<a href="javascript:;" class="user-profile dropdown-toggle text-right" data-toggle="dropdown" aria-expanded="false">
						<i class="fa fa-user onlineState" aria-hidden="true"> (Online)</i>
						<?php echo $this->session->userdata('usuario');?>
                        <br>
                        <?php if($this->session->userdata('tipo') =='lider'){
                            echo "<i class='fa fa-user-plus'></i> (Gobernador)";
                        }elseif($this->session->userdata('tipo') =='superadmin'){
                            echo "<i class='fa fa-bank'></i> (Superadmin)";
                        }elseif($this->session->userdata('tipo') =='admin'){
                            echo "<i class='fa fa-briefcase'></i> (Lider)";
                        }elseif($this->session->userdata('tipo') =='capturista'){
                            echo "<i class='fa fa-keyboard-o'></i> (Enlace)";
                        }
                        ?>
						<span class=" fa fa-angle-down"></span>
					</a>
					<ul class="dropdown-menu dropdown-usermenu pull-right">
						<li><a href="<?php echo base_url();?>"><i class="fa fa-sign-out pull-right"></i>Salir</a></li>
					</ul>
				</li>
				<!--<li role="presentation" class="dropdown open">
					<a href="<?php echo base_url(); ?>lista_objetivos">
						<i class="fa fa-exchange"></i>
					</a>
				</li>-->
			</ul>
		</nav>
	</div>
</div>
<style>
	.top_nav .nav>li>a:hover {
		background: #0d4465 !important;
		color: white !important;
	}
	.nav.navbar-nav>li>a {
		color: white !important;
	}
	top_nav .nav .open>a, .top_nav .nav .open>a:focus, .top_nav .nav .open>a:hover, .top_nav .nav>li>a:focus, .top_nav .nav>li>a:hover {
		background: #D9DEE4;
		background: #0d4465 !important;
		color: white !important;
	}

	.success{
		color: #0ad254;
	}
	.warning{
		color: #fb535b;
	}

</style>
