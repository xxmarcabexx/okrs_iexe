<?php require_once 'complementos/head.php';
$admin = true;
?>

<link href="<?php echo base_url(); ?>assets/build/css/dashboard.css" rel="stylesheet">
</head>

<div id="fondoLoader" style="background-color: rgba(0,0,0,0.2); display: none;
    margin: 0 auto;
    width: 100%;
    height: 100%;z-index: 1; position: fixed;">
    <img src="<?php echo base_url();?>assets/build/images/500.gif" style="margin-left: auto; margin-right: auto; display: block; margin-top: 20%;">
</div>
<body class="nav-md">
<div class="container body">
	<div class="main_container">
		<?php require_once 'complementos/menu.php' ?>
		<?php require_once 'complementos/topnavigation.php' ?>
		<div class="right_col" role="main">

			<div class="" id="carga">
				<div class="page-title">
					<div class="title_left">
						<h3>Mis compromisos</h3>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="dashboard_graph">

							<div class="row x_title">
								<div class="col-md-6">
									<!--<h3>Graficas Generales de los Planes
									</h3>-->
								</div>
							</div>

							<?php
							if (count($planesGraf) > 0)
							foreach ($planesGraf as $pl) {
							?>
							<div style="cursor: pointer; margin-top: 25px;" title="<?php echo $pl->idMv;?>" class="col-md-3 col-md-offset-1 col-sm-3 col-xs-12 bg-white text-center graficas planess">
								<div id='<?php echo "gauge" . $pl->idMv; ?>'></div>
								<h5  style="font-weight: normal; font-size: 12px;"><?php echo $pl->mv;?></h5>

							</div>
							<?php } ?>

							<div class="clearfix"></div>
						</div>
					</div>

				</div>
				<br/>


			</div>
		</div>


		<?php require_once 'complementos/footer.php' ?>


		<script>
			$(document).ready(function () {
				<?php if (count($planesGraf) > 0)
				foreach ($planesGraf as $pl) { ?>
				var g = new JustGage({
					id: "<?php echo 'gauge' . $pl->idMv?>",
					value: <?php echo $pl->avance?>,
					min: 0,
					max: 100,
					titlePosition: "below",
					valueFontColor: "#003A5D",
					pointer: true,
					levelColors: [
						"#A3D65C",
						"#A3D65C",
						"#A3D65C"
					],
					pointerOptions: {
						toplength: -20,
						bottomlength: 6,
						bottomwidth: 6,
						color: '#00AEAA',
						stroke: '#00AEAA',
						stroke_width: 1,
						stroke_linecap: 'round'
					},
					relativeGaugeSize: true,

				});
				<?php } ?>


				$(".planess").click(function () {
					$.ajax({
						type: "POST",
						url: 'detalle_plan/'+$(this).attr('title'),
						data:{},
						success: function(datos){
							$('#carga').html(datos);
						},
                        xhr: function(){
                            var xhr = $.ajaxSettings.xhr() ;
                            xhr.onloadstart = function(e) {
                                $("#fondoLoader").show();
                                console.log("Esta cargando");
                            };
                            xhr.onloadend = function (e) {
                                $("#fondoLoader").fadeOut(500);
                                console.log("Termino de cargar");
                            }
                            return xhr ;
                        }
					});
				});


			});
		</script>
