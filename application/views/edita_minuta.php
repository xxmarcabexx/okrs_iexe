<?php require_once 'complementos/head.php' ?>


<script src="<?php echo base_url(); ?>assets/build/js/edita_minuta.js"></script>
<link href="<?php echo base_url(); ?>assets/build/css/edita_minuta.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/build/css/wickedpicker.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/build/datepicker/css/datepicker.min.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url(); ?>assets/build/datepicker/js/datepicker.min.js"></script>
<!-- Include English language -->
<script src="<?php echo base_url(); ?>assets/build/datepicker/js/i18n/datepicker.es.js"></script>





</head>

<body class="nav-md">
<div id="fondoLoader" style="background-color: rgba(0,0,0,0.2); display: none;
    margin: 0 auto;
    width: 100%;
    height: 100%;z-index: 1; position: fixed;">
    <img src="<?php echo base_url();?>assets/build/images/500.gif" style="margin-left: auto; margin-right: auto; display: block; margin-top: 20%;">
</div>
<div class="container body">
	<div class="main_container">
		<?php require_once 'complementos/menu.php' ?>

		<!-- top navigation -->
		<?php require_once 'complementos/topnavigation.php' ?>
		<!-- /top navigation -->

		<!-- page content -->
		<div class="right_col" role="main">
			<div class="">
				<div class="page-title">
					<div class="title_left">
                            <h3>Módulo de acuerdos</h3><input value="<?php echo $minuta[0]->idMinuta;?>" id="minutaId" style="display: none;">
					</div>
				</div>
				<div class="clearfix"></div>

				<div class="row">
                    <input type="text" id="avancePorcentaje" value="<?php echo $minuta[0]->avancePorcentaje;?>" style="display: none;">

					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<h2>Edición de acuerdos</h2>
								<div class="clearfix"></div>
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="form-horizontal form-label-left">
										<div class="form-group" id="grupoKr">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Plan<span class="required"></span>
											</label>
											<div class="col-md-9 col-sm-9 col-xs-12">
												<select class="form-control has-feedback-left" id="plan" name="plan">
													<option value=0 name=0>Seleccione a que plan pertenece el acuerdo</option>
                                                    <option selected name="<?php echo $minuta[0]->idMv; ?>"  value="<?php echo (isset($minuta[0]->codigo))? $minuta[0]->codigo : "0";?>" ><?php echo $minuta[0]->mv;?></option>
													<?php foreach ($planes as $plan) { ?>
                                                        <?php if( $plan->idMv != $minuta[0]->idMv ){ ?>
														<option name="<?php echo $plan->idMv; ?>" value="<?php echo (isset($plan->codigo))?"$plan->codigo":"0";?>"><?php echo $plan->mv;?></option>
													<?php } } ?>
												</select>
												<span class="fa fa-sort form-control-feedback left" aria-hidden="true"></span>
												<small id="msj_plan"></small>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12"
												   for="first-name">Codigo de acuerdo<span class="required">*</span>
											</label>
											<div class="col-md-9 col-sm-9 col-xs-12">
												<input type="text" id="codigo" value="<?php echo $minuta[0]->codigo;?>"
													   class="form-control col-md-7 col-xs-12 has-feedback-left" readonly>
												<span class="fa fa-user form-control-feedback left"
													  aria-hidden="true"></span>
												<small id="msj_usuario"></small>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Objetivo<span
													class="required"></span>
											</label>
											<div class="col-md-9 col-sm-9 col-xs-12">
												<textarea class="form-control" id="objetivo"><?php echo $minuta[0]->objetivo;?></textarea>
												<small id="msj_clave"></small>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Asunto a tratar<span
													class="required"></span>
											</label>
											<div class="col-md-9 col-sm-9 col-xs-12">
												<textarea class="form-control" id="asunto"><?php echo $minuta[0]->asunto;?></textarea>
												<small id="msj_clave_valida"></small>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-horizontal form-label-left">
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12"
												   for="first-name">Fecha<span class="required">*</span>
											</label>
											<div class="col-md-9 col-sm-9 col-xs-12">
												<input type="text" id="fecha" data-language='es' value="<?php echo $minuta[0]->fecha;?>"
													   class="datepicker-here form-control col-md-7 col-xs-12 has-feedback-left">
												<span class="fa fa-user form-control-feedback left"
													  aria-hidden="true"></span>
												<small id="msj_usuario"></small>
											</div>
                                        </div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12"
												   for="first-name">Hora<span class="required">*</span>
											</label>
											<div class="col-md-9 col-sm-9 col-xs-12">
												<input type="text" id="hora" name="timepicker" value="<?php echo $minuta[0]->hora;?>"
													   class="form-control col-md-7 col-xs-12 has-feedback-left timepicker">
												<span class="fa fa-user form-control-feedback left"
													  aria-hidden="true"></span>
												<small id="msj_usuario"></small>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-md-3 col-sm-3 col-xs-12"
												   for="first-name">Lugar<span class="required">*</span>
											</label>
											<div class="col-md-9 col-sm-9 col-xs-12">
												<input type="text" id="lugar" value="<?php echo $minuta[0]->lugar;?>"
													   class="form-control col-md-7 col-xs-12 has-feedback-left">
												<span class="fa fa-user form-control-feedback left"
													  aria-hidden="true"></span>
												<small id="msj_usuario"></small>
											</div>
										</div>
									</div>
								</div>
							</div>

                            <div class="row" style="margin-bottom: 15px; margin-top: 15px;">
                                <div class="col-md-4">
                                    <button class="btn btn-success btn-sm" id="agregarAdjuntos"><i class="fa fa-plus-circle"></i>Adjuntar archivo</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <small id="mensajeAexos" style="color: red;"></small>
                                </div>
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="project_progress" id="barraCarga" style="display: none;">
                                        <small id="porcentajeProgreso">
                                        </small>
                                        <div class="progress progress_sm">
                                            <small id="porcentajeProgreso" style="color: red;"></small>
                                            <div class="progress-bar bg-green" id="barraProgresoCarga" role="progressbar"
                                                 data-transitiongoal="20" aria-valuenow="56" style="width: 57%;"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="row" id="formAnexoData">
                                        <?php foreach ($pdf as $pdfMinuta){?>
                                        <div id="anexoData<?php echo $pdfMinuta->idPdfMinuta;?>" style="height: 50px">
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <input type="text" value="<?php echo $pdfMinuta->pdf;?>" readonly class="form-control anexoFile" name="idAnexo" id="ane<?php echo $pdfMinuta->idPdfMinuta;?>">
                                                <span class="fa fa-line-chart form-control-feedback left" aria-hidden="true"></span>
                                            </div>
                                            <div class="col-md-4 text-center" style="margin-top: 5px;">
                                                <button class="btn btn-danger btn-xs elimAnexData" value="<?php echo $pdfMinuta->idPdfMinuta;?>" >
                                                    <i class="fa fa-trash"></i>Eliminar
                                                </button>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <div class="row" id="formAnexo">
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin-bottom: 15px; margin-top: 15px;">
                                <div class="col-md-4">
                                    <button class="btn btn-success btn-sm" id="agregarAcuerdos"><i class="fa fa-plus-circle"></i>Agregar acuerdo</button>
                                </div>
                            </div>

                            <div class="col-md-10 col-md-offset-1">
                                <div class="row" id="formAcuerdosData">
                                    <?php foreach ($acuerdos as $acu){
                                        $fechita = explode("-", $acu->fecha);
                                        ?>
                                    <div id="acuerdoData<?php echo $acu->idAcuerdo; ?>" style="height: 50px">
                                        <input type="text" value="<?php echo $acu->avance;?>" style="display: none;">
                                        <div class="col-md-3">
                                            <input class="form-control has-feedback-left" value="<?php echo $acu->actividad; ?>" type="text" placeholder="Actividad">
                                            <span class="fa fa-chevron-circle-right form-control-feedback left" aria-hidden="true"></span>
                                        </div>

                                        <div class="col-md-3">
                                            <input class="form-control has-feedback-left" value="<?php echo $acu->responsable; ?>" type="text" placeholder="Responsable">
                                            <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                        </div>

                                        <div class="col-md-5">
                                            <select class="col-md-4 text-center" style="height: 30px;" id="año">
                                                <option selected><?php echo $fechita[0]; ?></option>
                                                <option>2019</option>
                                                <option>2020</option>
                                                <option>2021</option>
                                                <option>2022</option>
                                            </select>
                                            <select class="col-md-4 text-center" style="height: 30px;" id="mes">
                                                <option selected><?php echo $fechita[1]; ?></option>

                                                <option>01</option>
                                                <option>02</option>
                                                <option>03</option>
                                                <option>04</option>
                                                <option>05</option>
                                                <option>06</option>
                                                <option>07</option>
                                                <option>08</option>
                                                <option>09</option>
                                                <option>10</option>
                                                <option>11</option>
                                                <option>12</option>
                                            </select>
                                            <select class="col-md-4 text-center" style="height: 30px;" id="dia">
                                                <option selected><?php echo $fechita[2]; ?></option>
                                                <option>01</option>
                                                <option>02</option>
                                                <option>03</option>
                                                <option>04</option>
                                                <option>05</option>
                                                <option>06</option>
                                                <option>07</option>
                                                <option>08</option>
                                                <option>09</option>
                                                <option>10</option>
                                                <option>11</option>
                                                <option>12</option>
                                                <option>13</option>
                                                <option>14</option>
                                                <option>15</option>
                                                <option>16</option>
                                                <option>17</option>
                                                <option>18</option>
                                                <option>19</option>
                                                <option>20</option>
                                                <option>21</option>
                                                <option>22</option>
                                                <option>23</option>
                                                <option>24</option>
                                                <option>25</option>
                                                <option>26</option>
                                                <option>27</option>
                                                <option>28</option>
                                                <option>29</option>
                                                <option>30</option>
                                                <option>31</option>
                                            </select>
                                            <!--<input class="form-control has-feedback-left" value="<?php echo $acu->fecha; ?>" type="text" id="acuerdoFechaidAcuerdo" placeholder="Fecha">
                                            <span class="fa fa-calendar form-control-feedback left" aria-hidden="true"></span>-->
                                        </div>

                                        <div class="col-md-1 text-center">
                                            <button class="btn btn-danger btn-xs elimAcuerdoData" value="<?php echo $acu->idAcuerdo; ?>">
                                                <i class="fa fa-trash"></i>Eliminar
                                            </button>
                                        </div>
                                    </div>
                                    <?php } ?>

                                </div>
                                <div class="row" id="formAcuerdos"></div>
                            </div>


                            <div class="row" style=" margin-top: 35px;">
                                <div class="col-md-6 text-right">
                                    <button class="btn btn-success btn-sm" id="guardar"><i class="fa fa-save"></i> Guardar</button>
                                </div>
                                <div class="col-md-6 text-left">
                                    <button class="btn btn-danger btn-sm" id="regresar"><i class="fa fa-save"></i> Regresar</button>
                                </div>
                            </div>

						</div>
					</div>
				</div>
			</div>
		</div>


		<div class="modal fade" id="myModal" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body text-center">
						<p>Key Result agregado correctamente.</p>
						<p>¿Deseas agregar otro?</p>
					</div>
					<div class="row">
						<div class="col-md-6 text-right">
							<button data-dismiss="modal" class="btn btn-primary" id="btnNkr">Nueva Kr</button>
						</div>
						<div class="col-md-6 text-left">
							<button class="btn btn-danger btnFinalizar">Finalizar</button>
						</div>
					</div>
				</div>

			</div>
		</div>
		<?php require_once 'complementos/footer.php' ?>
