<?php require_once 'complementos/head.php';

?>

<script src="<?php echo base_url();?>assets/build/js/editar_usuario.js"></script>
<link href="<?php echo base_url();?>assets/build/css/agrega_usuario.css" rel="stylesheet">

</head>

<body class="nav-md">
<div id="fondoLoader" style="background-color: rgba(0,0,0,0.2); display: none;
    margin: 0 auto;
    width: 100%;
    height: 100%;z-index: 1; position: fixed;">
    <img src="<?php echo base_url();?>assets/build/images/500.gif" style="margin-left: auto; margin-right: auto; display: block; margin-top: 20%;">
</div>
<div class="container body">
	<div class="main_container">
		<?php require_once 'complementos/menu.php' ?>

		<!-- top navigation -->
		<?php require_once 'complementos/topnavigation.php' ?>
		<!-- /top navigation -->

		<!-- page content -->
		<div class="right_col" role="main">
			<div class="">
				<div class="page-title">
					<div class="title_left">
						<h3>Módulo Usuarios</h3>
					</div>
				</div>
				<div class="clearfix"></div>

				<div class="row">

					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<h2>Edición de usuario</h2>
								<div class="clearfix"></div>
							</div>

							<div class="form-horizontal form-label-left" id="step-1">
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12"
										   for="first-name">Usuario<span class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input type="text" id="usuario" value="<?php echo $user[0]->user;?>" readonly="true"
											   class="form-control col-md-7 col-xs-12 has-feedback-left">
										<span class="fa fa-user form-control-feedback left"
											  aria-hidden="true"></span>
										<small id="msj_usuario"></small>
									</div>
								</div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tipo de usuario<span
                                                class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control has-feedback-left" id="tipo">
                                            <option value=0 >Selecciona el tipo de usuario</option>
                                            <option value=1 <?php echo ($user[0]->idRol==1) ? "selected" : ""; ?>>Gobernador</option>
                                            <option value=2 <?php echo ($user[0]->idRol==2) ? "selected" : ""; ?>>Superadmin</option>
                                            <option value=3 <?php echo ($user[0]->idRol==3) ? "selected" : ""; ?>>Lider</option>
                                            <option value=4 <?php echo ($user[0]->idRol==4) ? "selected" : ""; ?> >Enlace</option>
                                        </select>
                                        <span class="fa fa-sort form-control-feedback left"
                                              aria-hidden="true"></span>
                                        <small id="msj_tipo"></small>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                           for="first-name">Ver acuerdos:<span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12"  style="margin-top: 5px; margin-left: 10px;">
                                        <input type="checkbox" id="acuerdosV" <?php echo ($user[0]->acuerdos==1)? "checked": "";?>>
                                    </div>
                                </div>


								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Contraseña<span
											class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input type="password" id="clave" value="<?php echo $user[0]->clave;?>"
											   class="form-control col-md-7 col-xs-12 has-feedback-left">
										<span class="fa fa-lock form-control-feedback left"
											  aria-hidden="true"></span>
										<small id="msj_clave"></small>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Validar contraseña<span
											class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input type="password" id="clave_valida" value="<?php echo $user[0]->clave;?>"
											   class="form-control col-md-7 col-xs-12 has-feedback-left">
										<span class="fa fa-lock form-control-feedback left"
											  aria-hidden="true"></span>
										<small id="msj_clave_valida"></small>
									</div>
								</div>

								<div class="form-group" id="grupoPlanes" style="display: <?php echo ($user[0]->idRol==4 || $user[0]->idRol==3) ? "block" : "none"; ?>">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Planes<span
											class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<select class="form-control has-feedback-left" id="pl" name="pl">
											<option value=0>Selecciona que planes puede actualizar el capturista</option>
											<?php foreach ($planes as $plan){?>
												<option value="<?php echo $plan->idMv;?>"><?php echo $plan->mv?></option>
											<?php } ?>
										</select>
										<span class="fa fa-sort form-control-feedback left"
											  aria-hidden="true"></span>
										<small id="msj_kr"></small>
									</div>
								</div>
								<div class="form-group" id="grupoTagPl" style="display: <?php echo ($user[0]->idRol==4 || $user[0]->idRol==3) ? "block" : "none"; ?>">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"><span
											class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<div class="col-md-12" id="tagpl">
											<?php
											if (isset($user[0]->planes))
											foreach ($user[0]->planes as $pl){ ?>
												<button value="<?php echo $pl[0]->idMv;?>" class="plbtn btn btn-success btn-xs" style="margin-top: 15px !important;"><?php echo $pl[0]->mv;?><span class="fa fa-chevron-down"></span></button>

											<?php } ?>
										</div>
										<small id="msj_tagpl"></small>
									</div>
								</div>

								<div class="form-group" id="grupoKr" style="display: <?php echo ($user[0]->idRol==4) ? "block" : "none"; ?>">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Key Result<span
											class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<select class="form-control has-feedback-left" id="kr" name="kr">
											<option value=0>Selecciona que Key Result puede actualizar el capturista</option>
											<?php foreach ($planes as $plan){?>
												<optgroup label="<?php echo $plan->mv?>">
													<?php foreach ($plan->objetivos as $objetivos){?>
													<optgroup label="<?php echo $objetivos->objetivo?>">
														<?php foreach ($objetivos->kr as $kr){?>
															<option value="<?php echo $kr->idKeyResult;?>"><?php echo $kr->descripcion;?></option>
														<?php } ?>
													</optgroup>
													<?php } ?>
												</optgroup>
											<?php } ?>
										</select>
										<span class="fa fa-sort form-control-feedback left"
											  aria-hidden="true"></span>
										<small id="msj_kr"></small>
									</div>
								</div>

								<div class="form-group" id="grupoTag" style="display: <?php echo ($user[0]->idRol==4) ? "block" : "none"; ?>">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"><span
											class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<div class="col-md-12" id="tagkr">

											<?php
											if (isset($user[0]->kr))
											foreach ($user[0]->kr as $kr){ ?>
												<button value="<?php echo $kr[0]->idKeyResult;?>" class="plbtn btn btn-success btn-xs" style="margin-top: 15px !important;"><?php echo $kr[0]->descripcion;?><span class="fa fa-chevron-down"></span></button>

											<?php } ?>
										</div>
										<small id="msj_tagkr"></small>
									</div>
								</div>


								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombre<span
											class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input type="text" id="nombre" value="<?php echo $user[0]->nombre;?>"
											   class="form-control col-md-7 col-xs-12 has-feedback-left">
										<span class="fa fa-edit form-control-feedback left"
											  aria-hidden="true"></span>
										<small id="msj_nombre"></small>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Aepllido Paterno<span
											class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input type="text" id="apellidoP" value="<?php echo $user[0]->apellidoP;?>"
											   class="form-control col-md-7 col-xs-12 has-feedback-left">
										<span class="fa fa-edit form-control-feedback left"
											  aria-hidden="true"></span>
										<small id="msj_apellidoP"></small>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Apellido Materno<span
											class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input type="text" id="apellidoM" value="<?php echo $user[0]->apellidoM;?>"
											   class="form-control col-md-7 col-xs-12 has-feedback-left">
										<span class="fa fa-edit form-control-feedback left"
											  aria-hidden="true"></span>
										<small id="msj_apellidoM"></small>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Telefono<span
											class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input type="text" id="telefono" value="<?php echo $user[0]->telefono;?>"
											   class="form-control col-md-7 col-xs-12 has-feedback-left">
										<span class="fa fa-phone form-control-feedback left"
											  aria-hidden="true"></span>
										<small id="msj_telefono"></small>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Celular<span
											class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input type="text" id="celular" value="<?php echo $user[0]->celular;?>"
											   class="form-control col-md-7 col-xs-12 has-feedback-left">
										<span class="fa fa-mobile-phone	 form-control-feedback left"
											  aria-hidden="true"></span>
										<small id="msj_celular"></small>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Puesto<span
											class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input type="text" id="puesto" value="<?php echo $user[0]->puesto;?>"
											   class="form-control col-md-7 col-xs-12 has-feedback-left">
										<span class="fa fa-bank	 form-control-feedback left"
											  aria-hidden="true"></span>
										<small id="msj_puesto"></small>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Correo<span
											class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input type="email" id="correo" value="<?php echo $user[0]->correo;?>"
											   class="form-control col-md-7 col-xs-12 has-feedback-left">
										<span class="fa fa-envelope form-control-feedback left"
											  aria-hidden="true"></span>
										<small id="msj_correo"></small>
									</div>
								</div>


								<div class="form-group">
									<div class="col-md-6 text-right">
										<button class="btn btn-success" id="btnGuardar">Guardar</button>
									</div>
									<div class="col-md-6 text-left">
										<button class="btn btn-danger btnFinalizar">Regresar</button>
									</div>

								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>



		<div class="modal fade" id="myModal" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body text-center">
						<p>Key Result agregado correctamente.</p>
						<p>¿Deseas agregar otro?</p>
					</div>
					<div class="row">
						<div class="col-md-6 text-right">
							<button data-dismiss="modal" class="btn btn-primary" id="btnNkr">Nueva Kr</button>
						</div>
						<div class="col-md-6 text-left">
							<button class="btn btn-danger btnFinalizar">Finalizar</button>
						</div>
					</div>
				</div>

			</div>
		</div>

		<?php require_once 'complementos/footer.php' ?>
