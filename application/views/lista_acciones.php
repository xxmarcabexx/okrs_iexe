<?php

require_once 'complementos/head.php';
header('Access-Control-Allow-Origin: *');

?>
<script src="<?php echo base_url(); ?>assets/build/js/lista_acciones.js" charset="UTF-8"></script>
<link href="<?php echo base_url(); ?>assets/build/css/lista_usuarios.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css"


</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <?php require_once 'complementos/menu.php' ?>

        <!-- top navigation -->
        <?php require_once 'complementos/topnavigation.php' ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Modulo Acciones</h3>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2><i class="fa fa-align-left"></i> Lista de Acciones</h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-12">
                                <table class="table table-striped table-condensed resp_table demo">
                                    <thead>
                                    <tr class="text-center">
                                        <th class="azul text-center"><i class="fa fa-flag"></i> KR Interna</th>
                                        <th class="text-center"><i class="fa fa-user"></i> Descripción</th>
                                        <th class="verde text-center"><i class="fa fa-dot-circle-o"></i> Objetivo</th>

                                        <th class="chicle text-center"><i class="fa fa-user"></i> Descripción del Objetivo</th>
                                        <th class="azul">Progreso</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($acciones as $acc) { ?>
                                        <tr class="text-center">
                                            <td><?php echo $acc->accion;?></td>
                                            <td><?php echo $acc->descripcionKr;?></td>
                                            <td><?php echo $acc->objetivo;?></td>
                                            <td><?php echo $acc->descripcion;?></td>
                                            <td>
                                                <div class="project_progress">
                                                    <small
                                                            id="small<?php echo $acc->idAcciones; ?>"><?php echo number_format($acc->avance, 2); ?>
                                                        %
                                                    </small>
                                                    <div class="progress progress_sm">
                                                        <div class="progress-bar bg-green"
                                                             id="barraProgreso<?php echo $acc->idAcciones; ?>"
                                                             role="progressbar"
                                                             data-transitiongoal="<?php echo $acc->idAcciones; ?>"
                                                             aria-valuenow="56"
                                                             style="width: 57%;"></div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td><button class="btn btn-primary btn-xs updateAvance" value="<?php echo $acc->idAcciones;?>"><i class="fa fa-edit"></i> Actualizar</button></td>
                                            <td><button class="btn btn-danger btn-xs eliminar" value="<?php echo $acc->idAcciones;?>"><i class="fa fa-trash-o"></i> Eliminar</button></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- end of accordion -->
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                    <div class="form-group">
                        <label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required"
                                                                                           id="actual"></span></label>
                        <label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required"
                                                                                           id="tipometrica"></span></label>
                        <label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required"
                                                                                           id="rango"></span></label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Avance</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" id="avance" class="form-control col-md-7 col-xs-12" min="0" step="0.5">
                            <small id="msj_avance"></small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Descripción del
                            avance<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea class="form-control col-md-7 col-xs-12" id="descripcionavance"></textarea>
                            <small id="msj_descripcionavance"></small>
                        </div>
                    </div>
                </div>

                <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                    <div class="col-md-6 text-right">
                        <button class="btn btn-primary" id="btnAceptar">Aceptar</button>
                    </div>
                    <div class="col-md-6 text-left">
                        <button class="btn btn-danger btnFinalizar" class="close" data-dismiss="modal"
                                aria-label="Close" id="btnFinalizar">Cerrar
                        </button>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="aprobadoCancel" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                    <div class="form-group">
                        <label>Existe un avance aun no validado para esta Key Result</label>
                    </div>
                </div>

                <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                    <div class="col-md-6 col-md-offset-3 text-center">
                        <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Aceptar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modalValida" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                    <div class="form-group">
                        <label id="mensajeValida"></label>
                    </div>
                    <div class="form-group">
                        <label id="descripcionValida"></label>
                    </div>
                    <div class="form-group">
                        <label id="avanceValida"></label>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                    <div class="col-md-6 text-right">
                        <button class="btn btn-primary" id="btnAceptarValida">Aceptar</button>
                    </div>
                    <div class="col-md-6 text-left">
                        <button class="btn btn-danger btnFinalizar" class="close" data-dismiss="modal"
                                aria-label="Close">Cerrar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php require_once 'complementos/footer.php' ?>
