<?php

require_once 'complementos/head.php';
header('Access-Control-Allow-Origin: *');
date_default_timezone_set("America/Mexico_City");

?>

<script src="<?php echo base_url(); ?>assets/build/js/lista_objetivos.js"></script>

<link href="<?php echo base_url(); ?>assets/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/build/css/lista_objetivos.css" rel="stylesheet">

</head>
<!--<p class="network-status info-box success">You're online! 😄</p>-->
<body class="nav-md">
<input type="text"value="<?php echo $this->session->userdata('tipo');?>" style="display: none;" id="tipoUsuario">
<div class="container body">
	<div class="main_container">
		<?php require_once 'complementos/menu.php' ?>

		<!-- top navigation -->
		<?php require_once 'complementos/topnavigation.php' ?>
		<!-- /top navigation -->

		<!-- page content -->
		<div class="right_col" role="main">
			<div class="">
				<div class="page-title">
					<div class="title_left">
						<img src="<?php echo base_url(); ?>assets/build/images/mo.png" class="img-responsive">
						<!--<h3>Módulo Objetivos</h3>-->
					</div>
				</div>
				<div class="clearfix"></div>

				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<div class="clearfix"></div>
							</div>

							<?php
                            if($objetivos == null){
                                echo "<h4>Sin asignación.</h4>";
                            }
                            else
                            foreach ($objetivos as $obj){
                                ?>
							<div class="x_content listaObjetivos<?php echo $obj->idObjetivo; ?>">
								<!-- start accordion -->
								<div class="col-md-10">
									<div class="tituloObjetivos row">
										<div class="row alertAvance">
											<div class="col-md-12">
												<?php if (isset($obj->totalActualizar) && $obj->totalActualizar > 0) { ?>
													<sup
														style="background: #ED2B00 ; font-size: 12px; border-radius: 50px !important; color: white; padding: 4px 7px;">
														<?php echo $obj->totalActualizar; ?>
													</sup>
												<?php } ?>
											</div>
										</div>
										<div class="col-md-12 col-sm-12 col-xs-12 centrado">
											<div class="col-md-6 ico" data-toggle="collapse"
												 href="#<?php echo $obj->idObjetivo; ?>">
												<h2><i class="fa fa-arrow-circle-down iconito" style="font-size: 25px;"
													   aria-hidden="true"></i>
													<?php echo $obj->objetivo ?>
												</h2>
											</div>
											<?php
											if ($this->session->userdata('tipo') != 'capturista') {
												if ($this->session->userdata('tipo') == 'admin') { ?>
													<div class="col-md-3 text-right" style="margin-top: 5px;">
														<button class="btn btn-primary btn-xs editobjt"
																onclick="editarObjetivo('<?php echo $obj->idObjetivo; ?>')">
															<i class="fa fa-edit" aria-hidden="true"></i> Editar
															objetivo
														</button>
													</div>
													<div class="col-md-3 text-right" style="margin-top: 5px;">
														<button class="btn btn-warning btn-xs addkr"
																onclick="agregarKey('<?php echo $obj->idObjetivo; ?>')">
															<i class="fa fa-plus" aria-hidden="true"></i>
                                                            <?php if($obj->anual == 1){ ?>
                                                                OKR Bimestral
                                                            <?php }else{ ?>
                                                                Agregar Key Result
                                                            <?php } ?>
                                                        </button>
													</div>
												<?php } elseif ($this->session->userdata('tipo') == 'superadmin') { ?>
													<div class="col-md-2 text-right" style="margin-top: 5px;">
														<button class="btn btn-warning btn-xs addkr"
																onclick="agregarKey('<?php echo $obj->idObjetivo; ?>')">
															<i class="fa fa-plus" aria-hidden="true"></i>
                                                            <?php if($obj->anual == 1){ ?>
                                                                OKR Bimestral
                                                            <?php }else{ ?>
                                                                Agregar Key Result
                                                            <?php } ?>
														</button>
													</div>
													<div class="col-md-2 text-right" style="margin-top: 5px;">
														<button class="btn btn-primary btn-xs editobjt"
																onclick="editarObjetivo('<?php echo $obj->idObjetivo; ?>')">
															<i class="fa fa-edit" aria-hidden="true"></i> Editar
															Objetivo
														</button>
													</div>
													<div class="col-md-2 text-right" style="margin-top: 5px;">
														<button class="btn btn-danger btn-xs eliminaObjetivo"
																value='<?php echo $obj->idObjetivo; ?>'>
															<i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar
															Objetivo
														</button>
													</div>
												<?php }
											} ?>
										</div>
									</div>
								</div>
								<div class="col-md-2 text-center">
									<h6>Progreso OKR</h6>
									<div id="gauge<?php echo $obj->idObjetivo; ?>" class="grafi"></div>
								</div>
								<div class="col-md-12">
									<div id="<?php echo $obj->idObjetivo; ?>" class="accordion-body collapse">
										<div class="accordion-inner">
											<?php if ($this->session->userdata('tipo') != 'capturista'){ ?>
											<div class="col-md-8">
												<?php }else{ ?>
												<div class="col-md-12">
													<?php } ?>
													<table class="table table-striped table-condensed resp_table demo">
														<thead>
														<tr>
															<th class="text-left th_planes" id="th_indicador"><i
																	class="fa fa-caret-square-o-right"
																	aria-hidden="true"> </i> Key Result
															</th>
															<!--th class="text-left th_planes" id="th_indicador">Metrica</th>-->
															<th class="text-left th_planes" id="th_avance"><i
																	class="fa fa-line-chart" aria-hidden="true"></i>
																Avance
															</th>
															<th class="text-left th_planes" id="th_progreso"><i
																	class="fa fa-gear" aria-hidden="true"></i> Progreso
															</th>
															<th class="text-left th_planes" id="th_vi"><i
																	class="fa fa-percent" aria-hidden="true"></i> Valor
																Inicial
															</th>
															<th class="text-left th_planes" id="th_vf"><i
																	class="fa fa-percent" aria-hidden="true"></i> Valor
																Final
															</th>
															<th></th>
															<?php if ($this->session->userdata('tipo') != 'capturista') { ?>
																<th></th>
															<?php } ?>
															<?php if ($this->session->userdata('tipo') == 'superadmin') { ?>
																<th></th>
															<?php } ?>
														</tr>
														</thead>
														<tbody>
														<?php
														//if (count($obj) > 0)
														foreach ($obj->kr as $kr) {
															?>
															<tr class="listaKr<?php echo $kr->idKeyResult; ?>">
																<td id="descripcion<?php echo $kr->idKeyResult; ?>" data-toggle="collapse" href="#kri<?php echo $kr->idKeyResult; ?>">
                                                                    <?php if(isset($kr->acciones)){ ?>
                                                                        <i class="fa fa-arrow-circle-down iconito" style="font-size: 15px;" aria-hidden="true"></i>
                                                                    <?php } ?>
                                                                    <?php echo $kr->descripcion ?>
                                                                </td>
																<td id="avance<?php echo $kr->idKeyResult;?>"><?php echo $kr->avance; ?></td>
																<td>
																	<div class="project_progress">
																		<small
																			id="small<?php echo $kr->idKeyResult; ?>"><?php echo number_format($kr->avancePorcentaje, 2); ?>
																			%
																		</small>

																		<?php if (count($kr->bitacora) > 0 AND $kr->bitacora[0]->aprobado == 0) { ?>
																			<small style="color: red;"
																				   id="marcadorAvance<?php echo $kr->idKeyResult; ?>">
																				( <?php echo $kr->avancePorcentaje; ?>%
																				sin autorizar )
																			</small>
																		<?php } else { ?>
																			<small
																				id="marcadorAvance<?php echo $kr->idKeyResult; ?>"></small>
																		<?php } ?>


																		<div class="progress progress_sm">
																			<div class="progress-bar bg-green"
																				 id="barraProgreso<?php echo $kr->idKeyResult; ?>"
																				 role="progressbar"
																				 data-transitiongoal="<?php echo $kr->avancePorcentaje; ?>"
																				 aria-valuenow="56"
																				 style="width: 57%;"></div>
																		</div>
																	</div>
																</td>
																<td id="keyresultinicio<?php echo $kr->idKeyResult; ?>"><?php echo $kr->medicioncomienzo; ?></td>
																<td id="keyresultfinal<?php echo $kr->idKeyResult; ?>"><?php echo $kr->medicionfinal; ?></td>
																<td>
																	<?php if ($kr->avance >= $kr->medicionfinal) { ?>
																		<p>Completado al 100%</p>
																	<?php } else { ?>
                                                                    <?php if($obj->anual != 1){ ?>
																		<button
																			id="updateAvance<?php echo $kr->idKeyResult; ?>"
																			class="btn btn-info btn-xs updateAvance"
																			value="<?php echo $kr->idKeyResult; ?>"><i
																				class="fa fa-pencil"></i> Actualizar
																		</button>
                                                                        <?php } ?>
																	<?php } ?>
																</td>
                                                                <td>
                                                                    <button
                                                                            class="btn btn-danger btn-xs bitacoraLista"
                                                                            id="bitacora<?php echo $kr->idKeyResult; ?>"
                                                                            value="<?php echo $kr->idKeyResult; ?>">
                                                                        <i class="fa fa-pencil"></i> Bitácora
                                                                    </button>
                                                                </td>
																<?php if ($this->session->userdata('tipo') != 'capturista') { ?>
																	<td>
																		<?php
																		if (count($kr->bitacora) > 0) {
																			if ($kr->bitacora[0]->aprobado == 2) {
																				echo
																				"<button value='$kr->idKeyResult' class='btn btn-warning btn-xs avanceCancelado' id='avanceCancelado". $kr->idKeyResult . "'>
																						<i class='fa fa-pencil'></i> Avance Cancelado
																					</button>";
																			} elseif ($kr->bitacora[0]->aprobado == 0) {
																				echo
																					"<button class='btn btn-danger btn-xs autorizaAvance' id='autorizaAvance" . $kr->idKeyResult . "' value=" . $kr->bitacora[0]->idBitacora . ">
																						<i class='fa fa-pencil'></i> Autorizar
																					</button>";
																			}

																		} ?>
																	</td>
                                                                <?php }elseif($this->session->userdata('tipo') == 'capturista'){ ?>
                                                                <td>
                                                                    <?php
                                                                    if (count($kr->bitacora) > 0) {
                                                                        if ($kr->bitacora[0]->aprobado == 2) {
                                                                            echo
                                                                                "<button value='$kr->idKeyResult' class='btn btn-warning btn-xs avanceCancelado' id='avanceCancelado". $kr->idKeyResult . "'>
																						<i class='fa fa-pencil'></i> Avance Cancelado
																					</button>";
                                                                        }
                                                                    } ?>
                                                                </td>
                                                                <?php } ?>
																<?php if ($this->session->userdata('tipo') == 'superadmin') { ?>
																	<td>
																		<button class="btn btn-danger btn-xs eliminaKr"
																				value='<?php echo $kr->idKeyResult; ?>'>
																			<i class="fa fa-trash-o"
																			   aria-hidden="true"></i> Eliminar
																		</button>
																	</td>
                                                                    <td>
                                                                        <?php if($obj->anual == 1){ ?>
                                                                            <button class="btn azul btn-xs addkri" style="background: orangered; color: white;"
                                                                                    value='<?php echo $kr->idKeyResult; ?>'>
                                                                                <i class="fa fa-plus-circle"
                                                                                   aria-hidden="true"></i> OKR Interno
                                                                            </button>
                                                                        <?php } ?>

                                                                    </td>
                                                                <?php }elseif ($this->session->userdata('tipo') == 'admin') { ?>
                                                                    <td>
                                                                        <button class="btn btn-warning btn-xs editarKr"
                                                                                value='<?php echo $kr->idKeyResult; ?>'>
                                                                            <i class="fa fa-trash-o"
                                                                               aria-hidden="true"></i> Editar Kr
                                                                        </button>
                                                                    </td>
                                                                <?php } ?>
															</tr>
                                                            <?php if(isset($kr->acciones)){ ?>
                                                                <tbody id="kri<?php echo $kr->idKeyResult; ?>" class="accordion-body collapse">
                                                                    <tr style="background: gainsboro;">
                                                                        <td colspan="10"><h6><strong>OKRS INTERNO</strong></h6></td>
                                                                    </tr>
                                                                    <?php foreach ($kr->acciones as $acciones){ ?>
                                                                    <tr id="filaAccion<?php echo $acciones->idAcciones; ?>" >
                                                                        <td><?php echo $acciones->accion;?></td>
                                                                        <td id="avanceAcciones<?php echo $acciones->idAcciones; ?>"><?php echo $acciones->avance;?></td>
                                                                        <td>
                                                                            <div class="project_progress">
                                                                                <small id="smallAccion<?php echo $acciones->idAcciones;?>"><?php echo $acciones->avance;?> %</small>
                                                                                <small id="marcadorAvanceAcciones<?php echo $acciones->idAcciones; ?>"></small>
                                                                            <div class="progress progress_sm">
                                                                                <div class="progress-bar bg-green"
                                                                                     id="barraProgresoAcciones<?php echo $acciones->idAcciones; ?>"
                                                                                     role="progressbar"
                                                                                     data-transitiongoal="<?php echo $acciones->avance; ?>"
                                                                                     aria-valuenow="56"
                                                                                     style="width: 57%;"></div>
                                                                            </div>
                                                                            </div>
                                                                        </td>
                                                                        <td>0</td>
                                                                        <td>100</td>
                                                                        <td colspan="7">
                                                                            <?php if ($acciones->avance >= 100) { ?>
                                                                                <p>Completado al 100%</p>
                                                                            <?php } else { ?>
                                                                                <button
                                                                                        id="updateAvanceAcciones<?php echo $acciones->idAcciones; ?>"
                                                                                        class="btn btn-info btn-xs updateAvanceAcciones"
                                                                                        value="<?php echo $acciones->idAcciones; ?>"><i
                                                                                            class="fa fa-pencil"></i> Actualizar
                                                                                </button>
                                                                            <?php } ?>


                                                                            <?php if ($this->session->userdata('tipo') == 'superadmin') { ?>

                                                                                <button class="btn btn-danger btn-xs eliminaAccion" value='<?php echo $acciones->idAcciones; ?>'>
                                                                                    <i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar
                                                                                </button>
                                                                            <?php }?>

                                                                            <button
                                                                                    class="btn btn-danger btn-xs bitacoraListaAcciones"
                                                                                    id="bitacoraAcciones<?php echo $acciones->idAcciones; ?>"
                                                                                    value="<?php echo $acciones->idAcciones; ?>">
                                                                                <i class="fa fa-pencil"></i> Bitácora
                                                                            </button>
                                                                        </td>
                                                                    </tr>
                                                            <?php } ?>
                                                        <tr style="background: gainsboro;" id="bordeBajo<?php echo $kr->idKeyResult; ?>">
                                                            <td colspan="10"></td>
                                                        </tr>
                                                        </tbody>


                                                        <?php } ?>
                                                        <?php } ?>

														</tbody>
													</table>
												</div>
												<?php if ($this->session->userdata('tipo') != 'capturista') { ?>
													<div class="col-md-4">
														<div class="col-md-12">
															<div class="panel panel-primary">
																<div class="panel-heading" style="height: 40px;">
                                                                    <?php if($this->session->userdata('idRol')==2){?>
                                                                        <div class="col-md-7 text-right">
                                                                            Buzón <i class="fa fa-envelope" aria-hidden="true"></i>
                                                                        </div>

                                                                        <div style="cursor: pointer;" class="col-md-5 text-right vaciarChat" name="<?php echo $obj->idObjetivo;?>">
                                                                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                                                                            Vaciar Buzón
                                                                        </div>
                                                                    <?php  }else{ ?>
                                                                        <div class="col-md-12 text-center">
                                                                            Buzón <i class="fa fa-envelope" aria-hidden="true"></i>
                                                                        </div>
                                                                    <?php } ?>
																</div>
																<div class="panel-body PlanChat"
																	 id="PlanChat<?php echo $obj->idObjetivo; ?>">
																	<ul id="chatPlan<?php echo $obj->idObjetivo; ?>">
																		<?php foreach ($obj->chat as $ch) { ?>
																			<li>
																				<div class="col-md-10 mensajeChatDer">
																					<label><?php echo $ch->nombre; ?></label>
																					<p><?php echo $ch->mensaje; ?></p>
																					<small
																						class="col-md-12 fechamen text-right"><?php echo $ch->fechahora; ?></small>
																				</div>
																			</li>
																		<?php } ?>
																	</ul>
																</div>
																<div class="panel-footer">
																	<div class="form-row align-items-center">
																		<div class="col-sm-8 my-1">
																			<input type="text" class="form-control"
																				   id="mensajeChat<?php echo $obj->idObjetivo; ?>"
																				   placeholder="Escribir mensaje">
																		</div>
																		<div class="col-auto my-1">
																			<button class="btn btn-primary enviarChat"
																					text="<?php echo $obj->idObjetivo; ?>">
																				Enviar
																			</button>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												<?php } ?>
											</div>
										</div>
										<!-- end of accordion -->
									</div>
								</div>
								<?php } ?>

							</div>
						</div>
					</div>
				</div>
			</div>


			<div class="modal fade" id="myModal" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title text-center" id="tituloKr"></h4>

						</div>

						<div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
							<div class="form-group">
								<label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required"
																								   id="actual"></span></label>
								<label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required"
																								   id="tipometrica"></span></label>
								<label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required"
																								   id="rango"></span></label>
							</div>
							<div class="form-group">
								<div class="col-md-offset-3 col-md-6 col-sm-6 col-xs-12">
									<input placeholder="Ingresar avance" type="number" min=0 step="0.5" id="avance"
										   class="form-control col-md-7 col-xs-12 has-feedback-left">
									<span class="fa fa-line-chart form-control-feedback left" aria-hidden="true"></span>
									<small id="msj_avance" style="color: red;"></small>
								</div>
							</div>
                            <!--<div class="form-group">
                                <div class="col-md-offset-3 col-md-6 col-sm-6 col-xs-12">
                                    <input id="avancet" readonly
                                           class="form-control col-md-7 col-xs-12 has-feedback-left">
                                    <span class="fa fa-plus form-control-feedback left" aria-hidden="true"></span>
                                </div>
                            </div>-->
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Descripción
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<textarea class="form-control col-md-7 col-xs-12" id="descripcionavance"></textarea>
									<small id="msj_descripcionavance" style="color: red;"></small>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12 text-center">
								<button class="btn btn-success btn-xs agregaAnexo"><i class="fa fa-paperclip"></i>
									Adjuntar evidencia
								</button>
							</div>
							<div class="col-md-12 text-center">
								<small id="mensajeAexos" style="color: red;"></small>
							</div>
							<div class="col-md-10 col-md-offset-1">
								<div class="project_progress" id="barraCarga" style="display: none;">
									<small id="porcentajeProgreso">
									</small>
									<div class="progress progress_sm">
										<small id="porcentajeProgreso" style="color: red;"></small>
										<div class="progress-bar bg-green" id="barraProgresoCarga" role="progressbar"
											 data-transitiongoal="20" aria-valuenow="56" style="width: 57%;"></div>
									</div>
								</div>
							</div>
							<div class="col-md-10 col-md-offset-1">
								<div class="row" id="formAnexo"></div>
							</div>
						</div>

						<div class="row" style="margin-bottom: 25px; margin-top: 15px;">
							<div class="col-md-6 text-right">
								<button class="btn btn-primary" id="btnAceptar">Aceptar</button>
							</div>
							<div class="col-md-6 text-left">
								<button class="btn btn-danger" class="close" data-dismiss="modal"
										aria-label="Close" id="btnFinalizar">Cerrar
								</button>
							</div>
						</div>
					</div>

				</div>
			</div>


			<div class="modal fade" id="aprobadoCancel" role="dialog">
				<div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
						</div>

						<div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
							<div class="form-group">
								<label>Existe un avance no autorizado para esta Key Result</label>
							</div>
						</div>

						<div class="row" style="margin-bottom: 25px; margin-top: 15px;">
							<div class="col-md-6 col-md-offset-3 text-center">
								<button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Aceptar</button>
							</div>
						</div>
					</div>
				</div>
			</div>


			<div class="modal fade" id="noAutorizar" role="dialog">
				<div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title text-center">No autorizar el avance de: <label id="avanceDe"></label></h4>
						</div>

						<div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
							<div class="form-group">
								<label id="motivoCancel"></label>
							</div>
						</div>
						<div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
							<div class="form-group">
								<label>Motivo: </label>
							</div>
						</div>
						<div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
							<div class="form-group">
								<textarea id="motivo"></textarea>
							</div>
						</div>

						<div class="row" style="margin-bottom: 25px; margin-top: 15px;">
							<div class="col-md-6 text-center">
								<button class="btn btn-danger" data-dismiss="modal" id="aceptarCancelacion"
										aria-label="Close">Aceptar
								</button>
							</div>
							<div class="col-md-6 text-center">
								<button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Cerrar</button>
							</div>
						</div>
					</div>
				</div>
			</div>


			<div class="modal fade" id="modalValida" role="dialog">
				<div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title text-center" id="tituloavance"></h4>

                        </div>

						<div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
							<div class="form-group">
								<label id="mensajeValida"></label>
							</div>
							<div class="form-group">
								<label id="avanceValida"></label>
							</div>
							<div class="form-group">
								<label id="descripcionValida"></label>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-md-offset-3 text-center">
								<table class="table table-striped table-bordered" id="tablaAnexos">
									<thead>
									<td>Nombre de la evidencia</td>
									</thead>
								</table>
							</div>
						</div>
						<div class="row" style="margin-bottom: 25px; margin-top: 15px;">
							<div class="col-md-4 text-right">
								<button class="btn btn-primary" id="btnAceptarValida">Autorizar</button>
							</div>
							<div class="col-md-4 text-center">
								<button class="btn btn-danger" id="btnNoAutorizar" class="close" data-dismiss="modal"
										aria-label="Close">No autorizar
								</button>
							</div>
							<div class="col-md-4 text-left">
								<button class="btn btn-danger" class="close" data-dismiss="modal"
										aria-label="Close">Cerrar
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="modal fade" id="modalClave" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                            <div class="form-group">
                                <label id="leyendaEliminar">Esta apunto de eliminar un objetivo</label><br>
                                <small style="color: #2B3D53">Ingrese su contraseña para validar la eliminación</small>
                            </div>
                            <div class="form-group">
                                <input type="password" id="contraseña">
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                            <div class="col-md-6 text-right">
                                <button class="btn btn-primary" id="validaClave">Aceptar</button>
                            </div>
                            <div class="col-md-6 text-left">
                                <button class="btn btn-danger" class="close" data-dismiss="modal"
                                        aria-label="Close" id="cerraClave">Cerrar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modalClaveEditarKr" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                            <div class="form-group">
                                <label id="leyendaEliminar">Esta apunto de editar un Key Result</label><br>
                                <small style="color: #2B3D53">Ingrese su contraseña para validar la eliminación</small>
                            </div>
                            <div class="form-group">
                                <input type="password" id="contraseñaEdit">
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                            <div class="col-md-6 text-right">
                                <button class="btn btn-primary" id="validaClaveEdit">Aceptar</button>
                            </div>
                            <div class="col-md-6 text-left">
                                <button class="btn btn-danger" class="close" data-dismiss="modal"
                                        aria-label="Close" id="cerraClaveEdit">Cerrar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<input id="usuarioLogueado" value="<?php echo $this->session->userdata('idUser'); ?>"
				   style="display: none;">
            <div class="modal fade" id="myModalDic" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
						</div>

						<div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
							<div class="form-group">
								<label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required"
																								   id="tipometricaDic"></span></label>
							</div>
							<div class="col-md-12">
								<div class="col-md-12">
									<label>¿Desea finalizar el Key Result con el 100%? </label>
								</div>
								<br>
								<div class="radio col-md-6 text-right">
									<input type="radio" name="dico" value="Si">Si
								</div>
								<div class="radio col-md-6 text-left">
									<input type="radio" name="dico" value="No">No
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Avance:
								</label>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<textarea class="form-control col-md-7 col-xs-12"
											  id="descripcionavanceDic"></textarea>
									<small id="msj_descripcionavanceDic"></small>
								</div>
							</div>
						</div>

						<div class="row" style="margin-bottom: 25px; margin-top: 15px;">
							<div class="col-md-6 text-right">
								<button class="btn btn-primary" id="btnAceptarDic">Aceptar</button>
							</div>
							<div class="col-md-6 text-left">
								<button class="btn btn-danger btnFinalizar" class="close" data-dismiss="modal"
										aria-label="Close" id="btnFinalizarDic">Cerrar
								</button>
							</div>
						</div>
					</div>

				</div>
			</div>


			<div class="modal fade" id="modalCancelado" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title text-center" id="tituloCancelado"></h4>

						</div>
						<div class="row">
							<div class="col-md-6 col-md-offset-3 text-center">
								<h5 id="usuarioCancelado"></h5>
							</div>
							<div class="col-md-6 col-md-offset-3 text-center">
								<h5 id="avanceCancelado"></h5>
							</div>
							<div class="col-md-6 col-md-offset-3 text-center">
								<h5 id="motivoCancelado"></h5>
							</div>
						</div>

						<div class="row" style="margin-bottom: 25px; margin-top: 15px;">
							<div class="col-md-6 text-right">
								<button class="btn btn-primary" id="btnAceptarCancelado">Aceptar</button>
							</div>
							<div class="col-md-6 text-left">
								<button class="btn btn-danger btnFinalizar" class="close" data-dismiss="modal"
										aria-label="Close" id="btnFinalizar">Cerrar
								</button>
							</div>
						</div>
					</div>

				</div>
			</div>


            <div class="modal fade" id="modalAddkr" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title text-center" id="tituloCancelado">OKR interno</h4>

                        </div>
                            <div class="x_panel">
                                <div class="form-horizontal form-label-left" id="step-1">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">OKR
                                            <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea class="form-control col-md-7 col-xs-12 has-feedback-left"
                                                  id="accion"></textarea>
                                            <small id="msj_accion"></small>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="col-md-6 text-right">
                                        <button class="btn btn-success btn-sm" id="guardarAccion"><i class="fa fa-plus"></i> Guardar</button>
                                    </div>
                                    <div class="col-md-6 text-left">
                                        <button class="btn btn-danger btn-sm" id="cerrarAddkrInterno"><i class="fa fa-arrow-circle-left"></i> Cerrar</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                </div>
            </div>




            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title text-center" id="tituloKr"></h4>

                        </div>

                        <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                            <div class="form-group">
                                <label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required"
                                                                                                   id="actual"></span></label>
                                <label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required"
                                                                                                   id="tipometrica"></span></label>
                                <label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required"
                                                                                                   id="rango"></span></label>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-3 col-md-6 col-sm-6 col-xs-12">
                                    <input placeholder="Ingresar avance" type="number" min=0 step="0.5" id="avance"
                                           class="form-control col-md-7 col-xs-12 has-feedback-left">
                                    <span class="fa fa-line-chart form-control-feedback left" aria-hidden="true"></span>
                                    <small id="msj_avance" style="color: red;"></small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Descripción
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea class="form-control col-md-7 col-xs-12" id="descripcionavance"></textarea>
                                    <small id="msj_descripcionavance" style="color: red;"></small>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button class="btn btn-success btn-xs agregaAnexo"><i class="fa fa-paperclip"></i>
                                    Adjuntar evidencia
                                </button>
                            </div>
                            <div class="col-md-12 text-center">
                                <small id="mensajeAexos" style="color: red;"></small>
                            </div>
                            <div class="col-md-10 col-md-offset-1">
                                <div class="project_progress" id="barraCarga" style="display: none;">
                                    <small id="porcentajeProgreso">
                                    </small>
                                    <div class="progress progress_sm">
                                        <small id="porcentajeProgreso" style="color: red;"></small>
                                        <div class="progress-bar bg-green" id="barraProgresoCarga" role="progressbar"
                                             data-transitiongoal="20" aria-valuenow="56" style="width: 57%;"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-10 col-md-offset-1">
                                <div class="row" id="formAnexo"></div>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                            <div class="col-md-6 text-right">
                                <button class="btn btn-primary" id="btnAceptar">Aceptar</button>
                            </div>
                            <div class="col-md-6 text-left">
                                <button class="btn btn-danger" class="close" data-dismiss="modal"
                                        aria-label="Close" id="btnFinalizar">Cerrar
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


            <input id="fechaHoy" value="<?php echo date('Y-m-d H:i'); ?>" style="display: none;">
			<input id="nombreLogueado" value="<?php echo $this->session->userdata('usuario'); ?>"
				   style="display: none;">


            <div class="modal fade" id="myModalAccion" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                            <div class="form-group">
                                <label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required" id="actualAccion"></span></label>
                                <label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required" id="rangoAccion"></span></label>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Avance</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="number" id="avanceAccion" class="form-control col-md-7 col-xs-12" min="0" step="0.5">
                                    <small id="msj_avanceAccion"></small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Descripción del
                                    avance<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea class="form-control col-md-7 col-xs-12" id="descripcionavanceAccion"></textarea>
                                    <small id="msj_descripcionavanceAccion"></small>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                            <div class="col-md-6 text-right">
                                <button class="btn btn-primary" id="btnAceptarAccion">Aceptar</button>
                            </div>
                            <div class="col-md-6 text-left">
                                <button class="btn btn-danger btnFinalizar" class="close" data-dismiss="modal"
                                        aria-label="Close" id="btnFinalizarAccion">Cerrar
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>



            <div class="modal fade" id="modalClaveAccion" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                            <div class="form-group">
                                <label id="leyendaEliminar">Esta apunto de eliminar un OKR interno</label><br>
                                <small style="color: #2B3D53">Ingrese su contraseña para validar la eliminación</small>
                            </div>
                            <div class="form-group">
                                <input type="password" id="claveAccion">
                                <br>
                                <small id="msj_claveAccion"></small>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                            <div class="col-md-6 text-right">
                                <button class="btn btn-primary" id="validaClaveAccion">Aceptar</button>
                            </div>
                            <div class="col-md-6 text-left">
                                <button class="btn btn-danger" class="close" data-dismiss="modal"
                                        aria-label="Close" id="cerraClaveAccion">Cerrar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="modalBitacora" role="dialog">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title text-center" id="indicadorT">Bitácora del indicador: <strong id="tIndicador"></strong></h4>
                        </div>

                        <div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
                            <div class="row">
                                <div class="col-md-10 col-md-offset-1 text-center">
                                    <table class="table table-striped table-bordered" id="tablaBitacora">
                                        <thead style="font-size: 12px;">
                                        <td>Fecha</td>
                                        <td>Descripción</td>
                                        <td>Último avance</td>
                                        <td>Avance</td>
                                        <td>Estatus</td>
                                        <!--<td>Usuario/autorizo</td>-->
                                        <td>Usuario</td>
                                        <td>Motivo</td>
                                        <!--<td>Usuario/no autorizo</td>-->
                                        </thead>
                                        <tbody style="font-size: 10px;">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="margin-bottom: 25px; margin-top: 15px;">
                            <div class="col-md-6 col-md-offset-3 text-center">
                                <button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Aceptar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <?php require_once 'complementos/footer.php' ?>


			<script>
				$(document).ready(function () {

					<?php
                    if($objetivos != null){
                    foreach ($objetivos as $obj){ ?>

					g = new JustGage({
						id: 'gauge<?php echo $obj->idObjetivo;?>',
						value: <?php echo $obj->avancePorcentaje;?>,
						min: 0,
						max: 100,
						titlePosition: "below",
						valueFontColor: "#3f4c6b",
						pointer: true,
						pointerOptions: {
							toplength: -15,
							bottomlength: 10,
							bottomwidth: 12,
							color: '#8e8e93',
							stroke: '#ffffff',
							stroke_width: 3,
							stroke_linecap: 'round'
						},
						relativeGaugeSize: true,

					});

					<?php } }?>

				});


			</script>
