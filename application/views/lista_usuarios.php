<?php

require_once 'complementos/head.php';
header('Access-Control-Allow-Origin: *');

?>
<script src="<?php echo base_url(); ?>assets/build/js/lista_usuarios.js" charset="UTF-8"></script>
<link href="<?php echo base_url(); ?>assets/build/css/lista_usuarios.css" rel="stylesheet">

</head>

<body class="nav-md">
<div class="container body">
	<div class="main_container">
		<?php require_once 'complementos/menu.php' ?>

		<!-- top navigation -->
		<?php require_once 'complementos/topnavigation.php' ?>
		<!-- /top navigation -->

		<!-- page content -->
		<div class="right_col" role="main">
			<div class="">
				<div class="page-title">
					<div class="title_left">
						<h3>Modulo Usuarios</h3>
					</div>
				</div>
				<div class="clearfix"></div>

				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<h2><i class="fa fa-align-left"></i> Lista de Usuarios</h2>
								<div class="clearfix"></div>
							</div>
							<div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
								<table id="table-usuarios" class="table table-striped table-condensed resp_table demo">
									<thead>
									<tr>
                                        <th class="azul"><i class="fa fa-flag"></i> Tipo</th>
                                        <th class=""><i class="fa fa-user"></i> Usuario</th>
										<th class="verde"><i class="fa fa-dot-circle-o"></i> Contraseña</th>

										<th class="chicle"><i class="fa fa-user"></i> Nombre</th>
										<th class="azulf"><i class="fa fa-envelope-o"></i> Correo</th>
										<th></th>
										<th></th>
									</tr>
									</thead>
									<tbody>
									<?php
									foreach ($usuarios as $user) { ?>
										<tr>
                                            <td><?php echo $user->nombrePuesto;?></td>
                                            <td><?php echo $user->user;?></td>
											<td><?php echo $user->clave;?></td>
											<td><?php echo $user->nombre." ".$user->apellidoP." ".$user->apellidoM;?></td>
											<td><?php echo $user->correo;?></td>
											<td><button class="btn btn-primary btn-xs editar" value="<?php echo $user->user;?>"><i class="fa fa-edit"></i> Editar</button></td>
											<td><button class="btn btn-danger btn-xs eliminar" value="<?php echo $user->user;?>"><i class="fa fa-trash-o"></i> Eliminar</button></td>
										</tr>
									<?php } ?>
									</tbody>
								</table>
							</div>
							<!-- end of accordion -->
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>

				<div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
					<div class="form-group">
						<label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required"
																						   id="actual"></span></label>
						<label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required"
																						   id="tipometrica"></span></label>
						<label class="col-md-12 col-sm-12 col-xs-12" for="last-name"><span class="required"
																						   id="rango"></span></label>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Avance</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="number" id="avance" class="form-control col-md-7 col-xs-12" min="0" step="0.5">
							<small id="msj_avance"></small>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Descripción del
							avance<span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<textarea class="form-control col-md-7 col-xs-12" id="descripcionavance"></textarea>
							<small id="msj_descripcionavance"></small>
						</div>
					</div>
				</div>

				<div class="row" style="margin-bottom: 25px; margin-top: 15px;">
					<div class="col-md-6 text-right">
						<button class="btn btn-primary" id="btnAceptar">Aceptar</button>
					</div>
					<div class="col-md-6 text-left">
						<button class="btn btn-danger btnFinalizar" class="close" data-dismiss="modal"
								aria-label="Close" id="btnFinalizar">Cerrar
						</button>
					</div>
				</div>
			</div>

		</div>
	</div>


	<div class="modal fade" id="aprobadoCancel" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>

				<div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
					<div class="form-group">
						<label>Existe un avance aun no validado para esta Key Result</label>
					</div>
				</div>

				<div class="row" style="margin-bottom: 25px; margin-top: 15px;">
					<div class="col-md-6 col-md-offset-3 text-center">
						<button class="btn btn-danger" data-dismiss="modal" aria-label="Close">Aceptar</button>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="modalValida" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>

				<div class="form-horizontal form-label-left text-center" style="margin-top: 20px;">
					<div class="form-group">
						<label id="mensajeValida"></label>
					</div>
					<div class="form-group">
						<label id="descripcionValida"></label>
					</div>
					<div class="form-group">
						<label id="avanceValida"></label>
					</div>
				</div>
				<div class="row" style="margin-bottom: 25px; margin-top: 15px;">
					<div class="col-md-6 text-right">
						<button class="btn btn-primary" id="btnAceptarValida">Aceptar</button>
					</div>
					<div class="col-md-6 text-left">
						<button class="btn btn-danger btnFinalizar" class="close" data-dismiss="modal"
								aria-label="Close">Cerrar
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>

    <script src="<?php echo base_url(); ?>assets/js/basictable/jquery.basictable.min.js" charset="UTF-8"></script>
    <link href="<?php echo base_url(); ?>assets/js/basictable/basictable.css" rel="stylesheet">

    <script>
        $('#table-usuarios').basictable();
    </script>

	<?php require_once 'complementos/footer.php' ?>
