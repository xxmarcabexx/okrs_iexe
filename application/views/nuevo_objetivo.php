
<?php require_once 'complementos/head.php' ?>

<script src="assets/build/js/alta_objetivos.js"></script>
<link href="assets/build/css/nuevo_objetivo.css" rel="stylesheet">

<link href="assets/build/datepicker/css/datepicker.min.css" rel="stylesheet" type="text/css">
<script src="assets/build/datepicker/js/datepicker.min.js"></script>
<!-- Include English language -->
<script src="assets/build/datepicker/js/i18n/datepicker.es.js"></script>

<script>
	/*$(document).ready(function () {
		$('#my-element').datepicker([options])
// Access instance of plugin
		$('#my-element').data('datepicker')
	});*/
</script>


</head>

<body class="nav-md">
<div id="fondoLoader" style="background-color: rgba(0,0,0,0.2); display: none;
    margin: 0 auto;
    width: 100%;
    height: 100%;z-index: 1; position: fixed;">
    <img src="<?php echo base_url();?>assets/build/images/500.gif" style="margin-left: auto; margin-right: auto; display: block; margin-top: 20%;">
</div>
<div class="container body">
	<div class="main_container">
		<?php require_once 'complementos/menu.php' ?>

		<!-- top navigation -->
		<?php require_once 'complementos/topnavigation.php' ?>
		<!-- /top navigation -->

		<!-- page content -->
		<div class="right_col" role="main">
			<div class="">
				<div class="page-title">
					<div class="title_left">
						<img src="<?php echo base_url(); ?>assets/build/images/mo.png" class="img-responsive">
						<!--<h3>Modulo Objetivos</h3>-->
					</div>
				</div>
				<div class="clearfix"></div>

				<div class="row">

					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<h4>Creacion de Objetivos
									<small></small>
								</h4>
								<div class="clearfix"></div>
							</div>
							<div class="x_content">
								<!-- Smart Wizard -->
								<!--<p>Descripcion breve del modulo.</p>-->
								<div class="form-bootstrapWizard">
									<ul class="bootstrapWizard form-wizard">
										<li class="active" data-target="#step1">
											<a href="#tab1" data-toggle="tab" class="active"> <span
													class="step">1</span> <span class="title">Alta Objetivo</span> </a>
										</li>
										<li data-target="#step2" class="">
											<a href="#tab2" data-toggle="tab"> <span class="step">2</span> <span
													class="title">Alta Key Result</span> </a>
										</li>
									</ul>
									<div class="clearfix"></div>
								</div>

								<div id="wizard" class="form_wizard wizard_horizontal">
									<div id="step-1">

										<div class="form-horizontal form-label-left">
											<div class="form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12"
													   for="first-name">Plan<span class="required">*</span>
												</label>
												<div class="col-md-6 col-sm-6 col-xs-12">
													<select class="form-control has-feedback-left" id="mv">
														<option value=0>Selecciona un plan</option>
														<?php foreach ($dataMv as $mv) { ?>
															<option
																value=<?php echo $mv->idMv; ?>><?php echo $mv->mv; ?></option>
														<?php } ?>
													</select>
													<span class="fa fa-bank form-control-feedback left"
														  aria-hidden="true"></span>
													<small id="msj_mv"></small>
												</div>
											</div>

                                            <div class="form-group" id="grupoCheck" style="display: none;">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                                    Objetivo Anual
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="checkbox" id="anual">
                                                </div>
                                            </div>




											<div class="form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12"
													   for="first-name">Objetivo<span class="required">*</span>
												</label>
												<div class="col-md-6 col-sm-6 col-xs-12">
													<input type="text" id="objetivo"
														   class="form-control col-md-7 col-xs-12 has-feedback-left">
													<span class="fa fa-dot-circle-o form-control-feedback left"
														  aria-hidden="true"></span>
													<small id="msj_objetivo"></small>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12"
													   for="last-name">Descripción del objetivo <span
														class="required">*</span>
												</label>
												<div class="col-md-6 col-sm-6 col-xs-12">
													<textarea class="form-control col-md-7 col-xs-12 has-feedback-left"
															  id="descripcion"></textarea>
													<small id="msj_descripcion"></small>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12"
													   for="first-name">Fecha de Inicio del Objetivo<span
														class="required">*</span>
												</label>
												<div class="col-md-6 col-sm-6 col-xs-12">
													<input type="text" id="inicio" data-position='top left' data-language='es'
														   class="datepicker-here form-control col-md-7 col-xs-12 has-feedback-left">
													<span class="fa fa-calendar form-control-feedback left"
														  aria-hidden="true"></span>
													<small id="msj_inicio"></small>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12"
													   for="first-name">Fecha Final del Objetivo<span
														class="required">*</span>
												</label>
												<div class="col-md-6 col-sm-6 col-xs-12">
													<input type="text" id="final" data-position='top left' data-language='es'
														   class="datepicker-here form-control col-md-7 col-xs-12 has-feedback-left">
													<span class="fa fa-calendar form-control-feedback left"
														  aria-hidden="true"></span>
													<small id="msj_final"></small>
												</div>
											</div>

										</div>
									</div>
									<div id="step-2">
										<div class="form-horizontal form-label-left">
											<div class="form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12"
													   for="last-name">Key Result <span class="required">*</span>
												</label>
												<div class="col-md-6 col-sm-6 col-xs-12">
													<textarea class="form-control col-md-7 col-xs-12"
															  id="descripcionkr"></textarea>
													<small id="msj_descripcionkr"></small>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12"
													   for="first-name">Seleccionar Métrica<span
														class="required">*</span>
												</label>
												<div class="col-md-6 col-sm-6 col-xs-12" id="selectMetrica">
													<div class="radio">
														<label>
															<input type="radio" value="Porcentaje" id="metrica1"
																   name="optionsRadios">Porcentaje
														</label>
													</div>
													<div class="radio">
														<label>
															<input type="radio" value="Numero" id="metrica2"
																   name="optionsRadios">Numérico
														</label>
													</div>
													<div class="radio">
														<label>
															<input type="radio" value="Financiero" id="metrica3"
																   name="optionsRadios">Financiero
														</label>
													</div>
													<div class="radio">
														<label>
															<input type="radio" value="Dicotomica" id="metrica4"
																   name="optionsRadios">Dicotomica
														</label>
													</div>
													<small id="msj_metrica"></small>
												</div>
											</div>

											<div class="form-group cfo">
												<label class="control-label col-md-3 col-sm-3 col-xs-12"
													   for="first-name">Comienzo<span class="required">*</span>
												</label>
												<div class="col-md-6 col-sm-6 col-xs-12">
													<input type="number" min=0 step="0.5" id="medicioncomienzo"
														   class="form-control col-md-7 col-xs-12 has-feedback-left">
													<span class="fa fa-flag-o form-control-feedback left"
														  aria-hidden="true"></span>
													<small id="msj_comienzo"></small>
												</div>
											</div>
											<div class="form-group cfo">
												<label class="control-label col-md-3 col-sm-3 col-xs-12"
													   for="first-name">Final<span class="required">*</span>
												</label>
												<div class="col-md-6 col-sm-6 col-xs-12">
													<input type="number" id="medicionfinal" step="0.5"
														   class="form-control col-md-7 col-xs-12 has-feedback-left">
													<span class="fa fa-flag-checkered form-control-feedback left"
														  aria-hidden="true"></span>
													<small id="msj_tope"></small>
												</div>
											</div>
											<!--<div class="form-group cfo">
												<label class="control-label col-md-3 col-sm-3 col-xs-12"
													   for="first-name">Orden<span class="required">*</span>
												</label>
												<div class="col-md-6 col-sm-6 col-xs-12">
													<select class="form-control has-feedback-left" id="orden">
														<option value=0>Selecciona el orden de la métrica</option>
														<option value="Ascendente">Ascendente</option>
														<option value="Descendente">Descendente</option>
													</select>
													<span class="fa fa-bar-chart form-control-feedback left"
														  aria-hidden="true"></span>
													<small id="msj_orden"></small>
												</div>
											</div>-->

										</div>

									</div>
								</div>
								<!-- End SmartWizard Content -->
								<div class="row" id="seccionBotones">
									<div class="col-md-1 text-left" id="btnAtras">
										<button class="btn btn-success">Atras</button>
									</div>
									<div class="col-md-2 text-left" id="btnSiguiente">
										<button class="btn btn-primary">Siguiente</button>
									</div>
									<div class="col-md-2 text-left" id="btnGuardar">
										<button class="btn btn-primary">Guardar</button>
									</div>
									<div class="col-md-8 text-right" id="btnFinalizar">
										<button class="btn btn-danger pull-right btnFinalizar">Finalizar</button>
									</div>
								</div>
								<!-- End SmartWizard Content -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


		<!-- Modal -->
		<div class="modal fade" id="myModal" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body text-center">
						<p>Key Result agregado correctamente.</p>
						<p>¿Deseas agregar otro?</p>
					</div>
					<div class="row">
						<div class="col-md-6 text-right">
							<button data-dismiss="modal" class="btn btn-primary" id="btnNkr">Nueva Kr</button>
						</div>
						<div class="col-md-6 text-left">
							<button class="btn btn-danger btnFinalizar">Finalizar</button>
						</div>
					</div>
				</div>

			</div>
		</div>
		<!-- /page content -->
		<?php require_once 'complementos/footer.php' ?>
