<?php require_once 'complementos/head.php' ?>

<script src="<?php echo base_url();?>assets/build/js/agrega_usuario.js"></script>
<link href="<?php echo base_url();?>assets/build/css/agrega_usuario.css" rel="stylesheet">

</head>

<body class="nav-md">
<div id="fondoLoader" style="background-color: rgba(0,0,0,0.2); display: none;
    margin: 0 auto;
    width: 100%;
    height: 100%;z-index: 1; position: fixed;">
    <img src="<?php echo base_url();?>assets/build/images/500.gif" style="margin-left: auto; margin-right: auto; display: block; margin-top: 20%;">
</div>
<div class="container body">
	<div class="main_container">
		<?php require_once 'complementos/menu.php' ?>

		<!-- top navigation -->
		<?php require_once 'complementos/topnavigation.php' ?>
		<!-- /top navigation -->

		<!-- page content -->
		<div class="right_col" role="main">
			<div class="">
				<div class="page-title">
					<div class="title_left">
						<h3>Módulo Usuarios</h3>
					</div>
				</div>
				<div class="clearfix"></div>

				<div class="row">

					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
							<div class="x_title">
								<h2>Alta de usuario</h2>
								<div class="clearfix"></div>
							</div>

							<div class="form-horizontal form-label-left" id="step-1">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tipo de usuario<span
                                                class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control has-feedback-left" id="tipo">
                                            <option value=0>Selecciona el tipo de usuario</option>
                                            <option value=1>Superadmin</option>
                                            <option value=2>Gobernador</option>
                                            <option value=3>Lider</option>
                                            <option value=4>Enlace</option>
                                        </select>
                                        <span class="fa fa-sort form-control-feedback left"
                                              aria-hidden="true"></span>
                                        <small id="msj_tipo"></small>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                           for="first-name">Ver acuerdos:<span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12"  style="margin-top: 5px; margin-left: 10px;">
                                        <input type="checkbox" id="acuerdosV">
                                    </div>
                                </div>

								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12"
										   for="first-name">Usuario<span class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input type="text" id="usuario"
											   class="form-control col-md-7 col-xs-12 has-feedback-left">
										<span class="fa fa-user form-control-feedback left"
											  aria-hidden="true"></span>
										<small id="msj_usuario"></small>
									</div>
								</div>




								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Contraseña<span
											class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input type="password" id="clave"
											   class="form-control col-md-7 col-xs-12 has-feedback-left">
										<span class="fa fa-lock form-control-feedback left"
											  aria-hidden="true"></span>
										<small id="msj_clave"></small>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Validar contraseña<span
											class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input type="password" id="clave_valida"
											   class="form-control col-md-7 col-xs-12 has-feedback-left">
										<span class="fa fa-lock form-control-feedback left"
											  aria-hidden="true"></span>
										<small id="msj_clave_valida"></small>
									</div>
								</div>

								<div class="form-group" id="grupoPlanes" style="display: none;">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Planes<span
											class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<select class="form-control has-feedback-left" id="pl" name="pl">
											<option value=0>Selecciona que planes puede actualizar el capturista</option>
											<?php foreach ($planes as $plan){?>
												<option value="<?php echo $plan->idMv;?>"><?php echo $plan->mv?></option>
											<?php } ?>
										</select>
										<span class="fa fa-sort form-control-feedback left"
											  aria-hidden="true"></span>
										<small id="msj_kr"></small>
									</div>
								</div>
								<div class="form-group" id="grupoTagPl" style="display: none;">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"><span
											class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<div class="col-md-12" id="tagpl"></div>
										<small id="msj_tagpl"></small>
									</div>
								</div>

								<div class="form-group" id="grupoKr" style="display: none;">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Key Result<span
											class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<select class="form-control has-feedback-left" id="kr" name="kr">
											<option value=0>Selecciona que Key Result puede actualizar el capturista</option>
											<?php foreach ($planes as $plan){?>
												<optgroup label="<?php echo $plan->mv?>">
													<?php foreach ($plan->objetivos as $objetivos){?>
													<optgroup label="<?php echo $objetivos->objetivo?>">
														<?php foreach ($objetivos->kr as $kr){?>
															<option value="<?php echo $kr->idKeyResult;?>"><?php echo $kr->descripcion;?></option>
														<?php } ?>
													</optgroup>
													<?php } ?>
												</optgroup>
											<?php } ?>
										</select>
										<span class="fa fa-sort form-control-feedback left"
											  aria-hidden="true"></span>
										<small id="msj_kr"></small>
									</div>
								</div>

								<div class="form-group" id="grupoTag" style="display: none;">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"><span
											class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<div class="col-md-12" id="tagkr"></div>
										<small id="msj_tagkr"></small>
									</div>
								</div>


								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombre<span
											class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input type="text" id="nombre"
											   class="form-control col-md-7 col-xs-12 has-feedback-left">
										<span class="fa fa-edit form-control-feedback left"
											  aria-hidden="true"></span>
										<small id="msj_nombre"></small>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Aepllido Paterno<span
											class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input type="text" id="apellidoP"
											   class="form-control col-md-7 col-xs-12 has-feedback-left">
										<span class="fa fa-edit form-control-feedback left"
											  aria-hidden="true"></span>
										<small id="msj_apellidoP"></small>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Apellido Materno<span
											class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input type="text" id="apellidoM"
											   class="form-control col-md-7 col-xs-12 has-feedback-left">
										<span class="fa fa-edit form-control-feedback left"
											  aria-hidden="true"></span>
										<small id="msj_apellidoM"></small>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Telefono<span
											class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input type="text" id="telefono"
											   class="form-control col-md-7 col-xs-12 has-feedback-left">
										<span class="fa fa-phone form-control-feedback left"
											  aria-hidden="true"></span>
										<small id="msj_telefono"></small>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Celular<span
											class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input type="text" id="celular"
											   class="form-control col-md-7 col-xs-12 has-feedback-left">
										<span class="fa fa-mobile-phone	 form-control-feedback left"
											  aria-hidden="true"></span>
										<small id="msj_celular"></small>
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Puesto<span
											class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input type="text" id="puesto"
											   class="form-control col-md-7 col-xs-12 has-feedback-left">
										<span class="fa fa-bank	 form-control-feedback left"
											  aria-hidden="true"></span>
										<small id="msj_puesto"></small>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Correo<span
											class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input type="email" id="correo"
											   class="form-control col-md-7 col-xs-12 has-feedback-left">
										<span class="fa fa-envelope form-control-feedback left"
											  aria-hidden="true"></span>
										<small id="msj_correo"></small>
									</div>
								</div>


								<div class="form-group">
									<div class="col-md-6 text-right">
										<button class="btn btn-success" id="btnGuardar">Guardar</button>
									</div>
									<div class="col-md-6 text-left">
										<button class="btn btn-danger btnFinalizar">Regresar</button>
									</div>

								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>



		<div class="modal fade" id="myModal" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body text-center">
						<p>Key Result agregado correctamente.</p>
						<p>¿Deseas agregar otro?</p>
					</div>
					<div class="row">
						<div class="col-md-6 text-right">
							<button data-dismiss="modal" class="btn btn-primary" id="btnNkr">Nueva Kr</button>
						</div>
						<div class="col-md-6 text-left">
							<button class="btn btn-danger btnFinalizar">Finalizar</button>
						</div>
					</div>
				</div>

			</div>
		</div>

		<?php require_once 'complementos/footer.php' ?>
