$(document).ready(function () {

	$("#proyecto").focus(function () {
		$("#msj_proyecto").html('');
		$("#proyecto").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
	});

	$("#status").focus(function () {
		$("#msj_status").html('');
		$("#status").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
	});

	$("#finicio").focus(function () {
		$("#msj_finicio").html('');
		$("#finicio").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
	});

	$("#ffin").focus(function () {
		$("#msj_ffin").html('');
		$("#ffin").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
	});

	$("#lider").focus(function () {
		$("#msj_lider").html('');
		$("#lider").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
	});

	$("#inversion").focus(function () {
		$("#msj_inversion").html('');
		$("#inversion").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
	});

	$("#finalidad").focus(function () {
		$("#msj_finalidad").html('');
		$("#finalidad").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
	});

	$("#poblacionP").focus(function () {
		$("#msj_poblacionP").html('');
		$("#poblacionP").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
	});

	$("#proposito").focus(function () {
		$("#msj_proposito").html('');
		$("#proposito").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
	});

	$("#poblacionO").focus(function () {
		$("#msj_poblacionO").html('');
		$("#poblacionO").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
	});

	$("#presupuesto2018").focus(function () {
		$("#msj_presupuesto2018").html('');
		$("#presupuesto2018").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
	});

	$("#presupuesto2019").focus(function () {
		$("#msj_presupuesto2019").html('');
		$("#presupuesto2019").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
	});

	$("#indicadorEstrategico").focus(function () {
		$("#msj_indicadorEstrategico").html('');
		$("#indicadorEstrategico").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
	});

	$("#inversion2018").focus(function () {
		$("#msj_inversion2018").html('');
		$("#inversion2018").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
	});

	$("#inversion2019").focus(function () {
		$("#msj_inversion2019").html('');
		$("#inversion2019").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
	});

	$("#metaIndicador").focus(function () {
		$("#msj_metaIndicador").html('');
		$("#metaIndicador").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
	});


	$("#afinicio").focus(function () {
		$("#msj_afinicio").html('');
		$("#afinicio").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
	});
	$("#affinal").focus(function () {
		$("#msj_affinal").html('');
		$("#affinal").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
	});
	$("#afisinicio").focus(function () {
		$("#msj_afisinicio").html('');
		$("#afisinicio").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
	});
	$("#afisifinal").focus(function () {
		$("#msj_afisifinal").html('');
		$("#afisifinal").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
	});
	$("#giinicio").focus(function () {
		$("#msj_giinicio").html('');
		$("#giinicio").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
	});
	$("#gifinal").focus(function () {
		$("#msj_gifinal").html('');
		$("#gifinal").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
	});
	$("#efinicio").focus(function () {
		$("#msj_efinicio").html('');
		$("#efinicio").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
	});
	$("#effinal").focus(function () {
		$("#msj_effinal").html('');
		$("#effinal").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
	});
	$("#eainicio").focus(function () {
		$("#msj_eainicio").html('');
		$("#eainicio").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
	});
	$("#eafinal").focus(function () {
		$("#msj_eafinal").html('');
		$("#eafinal").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
	});
	$("#ejinicio").focus(function () {
		$("#msj_ejinicio").html('');
		$("#ejinicio").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
	});
	$("#ejfinal").focus(function () {
		$("#msj_ejfinal").html('');
		$("#ejfinal").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
	});
	$("#rpinicio").focus(function () {
		$("#msj_rpinicio").html('');
		$("#rpinicio").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
	});
	$("#rpfinal").focus(function () {
		$("#msj_rpfinal").html('');
		$("#rpfinal").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
	});
	$("#scincio").focus(function () {
		$("#msj_scincio").html('');
		$("#scincio").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
	});
	$("#scfinal").focus(function () {
		$("#msj_scfinal").html('');
		$("#scfinal").css({"border-radius": "50px", "border-color": "#5bc7cb", "color": "#5bc7cb"});
	});


	$("#btnGuardar").click(function () {

		var finalizar = 1;


		if ($("#proyecto").val() == '') {
			$("#msj_proyecto").html("Debe ingresar el nombre del proyecto");
			$("#proyecto").css({"border": "1px solid red", "border-radius": "50px"});
			finalizar = 0;
		}

		if ($("#status").val() == '') {
			$("#msj_status").html("Debe ingresar el status del proyecto");
			$("#status").css({"border": "1px solid red", "border-radius": "50px"});
			finalizar = 0;
		}

		if ($("#finicio").val() == '') {
			$("#msj_finicio").html("Debe ingresar la fecha inicial del proyecto");
			$("#finicio").css({"border": "1px solid red", "border-radius": "50px"});
			finalizar = 0;
		}

		if ($("#ffin").val() == '') {
			$("#msj_ffin").html("Debe ingresar la fecha final del proyecto");
			$("#ffin").css({"border": "1px solid red", "border-radius": "50px"});
			finalizar = 0;
		}

		if ($("#lider").val() == '') {
			$("#msj_lider").html("Debe ingresar el lider del proyecto");
			$("#lider").css({"border": "1px solid red", "border-radius": "50px"});
			finalizar = 0;
		}

		if ($("#inversion").val() == '') {
			$("#msj_inversion").html("Debe ingresar la inversion del proyecto");
			$("#inversion").css({"border": "1px solid red", "border-radius": "50px"});
			finalizar = 0;
		}

		if ($("#finalidad").val() == '') {
			$("#msj_finalidad").html("Debe ingresar la finalidad del proyecto");
			$("#finalidad").css({"border": "1px solid red", "border-radius": "50px"});
			finalizar = 0;
		}

		if ($("#poblacionP").val() == '') {
			$("#msj_poblacionP").html("Debe ingresar la poblacion potencial del proyecto");
			$("#poblacionP").css({"border": "1px solid red", "border-radius": "50px"});
			finalizar = 0;
		}

		if ($("#proposito").val() == '') {
			$("#msj_proposito").html("Debe ingresar el proposito del proyecto");
			$("#proposito").css({"border": "1px solid red", "border-radius": "50px"});
			finalizar = 0;
		}

		if ($("#poblacionO").val() == '') {
			$("#msj_poblacionO").html("Debe ingresar la poblacion objetivo");
			$("#poblacionO").css({"border": "1px solid red", "border-radius": "50px"});
			finalizar = 0;
		}

		if ($("#presupuesto2018").val() == '') {
			$("#msj_presupuesto2018").html("Debe ingresar el presupusto 2018");
			$("#presupuesto2018").css({"border": "1px solid red", "border-radius": "50px"});
			finalizar = 0;
		}

		if ($("#presupuesto2019").val() == '') {
			$("#msj_presupuesto2019").html("Debe ingresar el presupusto 2019");
			$("#presupuesto2019").css({"border": "1px solid red", "border-radius": "50px"});
			finalizar = 0;
		}

		if ($("#indicadorEstrategico").val() == '') {
			$("#msj_indicadorestrategico").html("Debe ingresar el indicador estrategico");
			$("#indicadorEstrategico").css({"border": "1px solid red", "border-radius": "50px"});
			finalizar = 0;
		}

		if ($("#inversion2018").val() == '') {
			$("#msj_inversion2018").html("Debe ingresar la inversion del 2018");
			$("#inversion2018").css({"border": "1px solid red", "border-radius": "50px"});
			finalizar = 0;
		}

		if ($("#inversion2019").val() == '') {
			$("#msj_inversion2019").html("Debe ingresar la inversion de 2019");
			$("#inversion2019").css({"border": "1px solid red", "border-radius": "50px"});
			finalizar = 0;
		}

		if ($("#metaIndicador").val() == '') {
			$("#msj_metaIndicador").html("Debe ingresar la meta del indicar del proyecto");
			$("#metaIndicador").css({"border": "1px solid red", "border-radius": "50px"});
			finalizar = 0;
		}

		if ($("#afinicio").val() == '') {
			$("#msj_afinicio").html("Debe ingresar el valor inicial del avance financiero");
			$("#afinicio").css({"border": "1px solid red", "border-radius": "50px"});
			finalizar = 0;
		}

		if ($("#affinal").val() == '') {
			$("#msj_affinal").html("Debe ingresar el valor final del avance financiero");
			$("#affinal").css({"border": "1px solid red", "border-radius": "50px"});
			finalizar = 0;
		}

		if (parseInt($("#afinicio").val()) > parseInt($("#affinal").val())) {
			$("#msj_afinicio").html("El valor inicial debe ser menor al valor final");
			$("#afinicio").css({"border": "1px solid red", "border-radius": "50px"});
		}


		if ($("#afisinicio").val() == '') {
			$("#msj_afisinicio").html("Debe ingresar el valor inicial del avance fisico");
			$("#afisinicio").css({"border": "1px solid red", "border-radius": "50px"});
			finalizar = 0;
		}

		if ($("#afisifinal").val() == '') {
			$("#msj_afisifinal").html("Debe ingresar el valor final del avance fisico");
			$("#afisifinal").css({"border": "1px solid red", "border-radius": "50px"});
			finalizar = 0;
		}

		if (parseInt($("#afisinicio").val()) > parseInt($("#afisifinal").val())) {
			$("#msj_afisinicio").html("El valor inicial debe ser menor al valor final");
			$("#afisinicio").css({"border": "1px solid red", "border-radius": "50px"});
		}

		if ($("#giinicio").val() == '') {
			$("#msj_giinicio").html("Debe ingresar el valor inicial de la gestion de inversion");
			$("#giinicio").css({"border": "1px solid red", "border-radius": "50px"});
			finalizar = 0;
		}

		if ($("#gifinal").val() == '') {
			$("#msj_gifinal").html("Debe ingresar el valor final de la gestion de inversion");
			$("#gifinal").css({"border": "1px solid red", "border-radius": "50px"});
			finalizar = 0;
		}

		//console.log(parseInt($("#giinicio").val()) + ">" + parseInt( $("#gifinal").val() ));
		if (parseInt($("#giinicio").val()) > parseInt($("#gifinal").val())) {
			$("#msj_giinicio").html("El valor inicial debe ser menor al valor final");
			$("#giinicio").css({"border": "1px solid red", "border-radius": "50px"});
		}

		if ($("#efinicio").val() == '') {
			$("#msj_efinicio").html("Debe ingresar el valor inicial de la efectividad financiera");
			$("#efinicio").css({"border": "1px solid red", "border-radius": "50px"});
			finalizar = 0;
		}

		if ($("#effinal").val() == '') {
			$("#msj_effinal").html("Debe ingresar el valor final de la efectividad financiera");
			$("#effinal").css({"border": "1px solid red", "border-radius": "50px"});
			finalizar = 0;
		}


		if (parseInt($("#efinicio").val()) > parseInt($("#effinal").val())) {
			$("#msj_efinicio").html("El valor inicial debe ser menor al valor final");
			$("#efinicio").css({"border": "1px solid red", "border-radius": "50px"});
		}

		if ($("#eainicio").val() == '') {
			$("#msj_eainicio").html("Debe ingresar el valor inicial de la efecividad administrativa");
			$("#eainicio").css({"border": "1px solid red", "border-radius": "50px"});
			finalizar = 0;
		}

		if ($("#eafinal").val() == '') {
			$("#msj_eafinal").html("Debe ingresar el valor final de la efectividad administrativa");
			$("#eafinal").css({"border": "1px solid red", "border-radius": "50px"});
			finalizar = 0;
		}


		if (parseInt($("#eainicio").val()) > parseInt($("#eafinal").val())) {
			$("#msj_eainicio").html("El valor inicial debe ser menor al valor final");
			$("#eainicio").css({"border": "1px solid red", "border-radius": "50px"});
		}

		if ($("#ejinicio").val() == '') {
			$("#msj_ejinicio").html("Debe ingresar el valor inicial de la efectividad jurídica");
			$("#ejinicio").css({"border": "1px solid red", "border-radius": "50px"});
			finalizar = 0;
		}

		if ($("#ejfinal").val() == '') {
			$("#msj_ejfinal").html("Debe ingresar el valor final de la efectividad jurídica");
			$("#ejfinal").css({"border": "1px solid red", "border-radius": "50px"});
			finalizar = 0;
		}


		if (parseInt($("#ejinicio").val()) > parseInt($("#ejfinal").val())) {
			$("#msj_ejinicio").html("El valor inicial debe ser menor al valor final");
			$("#ejinicio").css({"border": "1px solid red", "border-radius": "50px"});
		}

		if ($("#rpinicio").val() == '') {
			$("#msj_rpinicio").html("Debe ingresar el valor inicial de la reprogramacion presupuestaria");
			$("#rpinicio").css({"border": "1px solid red", "border-radius": "50px"});
			finalizar = 0;
		}

		if ($("#rpfinal").val() == '') {
			$("#msj_rpfinal").html("Debe ingresar el valor final de la reprogramacion presupuestaria");
			$("#rpfinal").css({"border": "1px solid red", "border-radius": "50px"});
			finalizar = 0;
		}

		if (parseInt($("#rpinicio").val()) > parseInt($("#rpfinal").val())) {
			$("#msj_rpinicio").html("El valor inicial debe ser menor al valor final");
			$("#rpinicio").css({"border": "1px solid red", "border-radius": "50px"});
		}

		if ($("#scincio").val() == '') {
			$("#msj_scincio").html("Debe ingresar el valor inicial de la satisfaccion ciudadana");
			$("#scincio").css({"border": "1px solid red", "border-radius": "50px"});
			finalizar = 0;
		}

		if ($("#scfinal").val() == '') {
			$("#msj_scfinal").html("Debe ingresar el valor final de la satisfaccion ciudadana");
			$("#scfinal").css({"border": "1px solid red", "border-radius": "50px"});
			finalizar = 0;
		}


		if (parseInt($("#scincio").val()) > parseInt($("#scfinal").val())) {
			$("#msj_scincio").html("El valor inicial debe ser menor al valor final");
			$("#scincio").css({"border": "1px solid red", "border-radius": "50px"});
            finalizar = 0;
        }

		if (finalizar == 0) {
			return false;
		} else if (finalizar == 1) {
			$.ajax({
				url: 'PlanesController/insert',
				data: {
					mv: $("#proyecto").val(),
					status: $("#status").val(),
					lider: $("#lider").val(),
					inversionT: $("#inversion").val(),
					fin: $("#finalidad").val(),
					poblacionPotencial: $("#poblacionP").val(),
					proposito: $("#proposito").val(),
					poblacionObjetivo: $("#poblacionO").val(),
					presupuesto2018: $("#presupuesto2018").val(),
					presupuesto2019: $("#presupuesto2019").val(),
					indicadorEstrategico: $("#indicadorEstrategico").val(),
					inversion2018: $("#inversion2018").val(),
					inversion2019: $("#inversion2019").val(),
					metaIndicador: $("#metaIndicador").val(),
					finicial: $("#finicio").val(),
					ffinal: $("#ffin").val()
				},
				type: 'POST',
				success: function (response) {
					if (response != 0) {
						var idPlan = response;
						var indicadores = new Array();
						indicadores[0] = new Object();
						indicadores[1] = new Object();
						indicadores[2] = new Object();
						indicadores[3] = new Object();
						indicadores[4] = new Object();
						indicadores[5] = new Object();
						indicadores[6] = new Object();
						indicadores[7] = new Object();


						indicadores[0].nombreIndicador = 'Avance Financiero';
						indicadores[0].inicio = $("#afinicio").val();
						indicadores[0].final = $("#affinal").val();
						indicadores[0].avance = 0;
						indicadores[0].idPlan = idPlan;

						indicadores[1].nombreIndicador = 'Avance Físico';
						indicadores[1].inicio = $("#afisinicio").val();
						indicadores[1].final = $("#afisifinal").val();
						indicadores[1].avance = 0;
						indicadores[1].idPlan = idPlan;

						indicadores[2].nombreIndicador = 'Gestión de Inversión';
						indicadores[2].inicio = $("#giinicio").val();
						indicadores[2].final = $("#gifinal").val();
						indicadores[2].avance = 0;
						indicadores[2].idPlan = idPlan;

						indicadores[3].nombreIndicador = 'Efectividad Financiera';
						indicadores[3].inicio = $("#efinicio").val();
						indicadores[3].final = $("#effinal").val();
						indicadores[3].avance = 0;
						indicadores[3].idPlan = idPlan;

						indicadores[4].nombreIndicador = 'Efectividad Administrativa';
						indicadores[4].inicio = $("#eainicio").val();
						indicadores[4].final = $("#eafinal").val();
						indicadores[4].avance = 0;
						indicadores[4].idPlan = idPlan;

						indicadores[5].nombreIndicador = 'Efectividad Jurídica';
						indicadores[5].inicio = $("#ejinicio").val();
						indicadores[5].final = $("#ejfinal").val();
						indicadores[5].avance = 0;
						indicadores[5].idPlan = idPlan;

						indicadores[6].nombreIndicador = 'Resprogramación Presupuestaria';
						indicadores[6].inicio = $("#rpinicio").val();
						indicadores[6].final = $("#rpfinal").val();
						indicadores[6].avance = 0;
						indicadores[6].idPlan = idPlan;

						indicadores[7].nombreIndicador = 'Satisfacción Ciudadana';
						indicadores[7].inicio = $("#scincio").val();
						indicadores[7].final = $("#scfinal").val();
						indicadores[7].avance = 0;
						indicadores[7].idPlan = idPlan;

						var dataObjt = JSON.stringify(indicadores);
						$.ajax({
							url: 'IndicadoresController/insert',
							dataType: "json",
							data: {
								obj: dataObjt
							},
							type: 'POST',
							success: function (response) {
								if (response != 0) {
									alert("Los registros han sido guardados correctamente");
									window.location = 'lista_planes';
								} else if (response == 0) {
									return false;
								}
							},
                            xhr: function(){
                                var xhr = $.ajaxSettings.xhr() ;
                                xhr.onloadstart = function(e) {
                                    $("#fondoLoader").show();
                                };
                                xhr.upload.onloadend = function (e) {
                                    $("#fondoLoader").fadeOut(2000);
                                }
                                return xhr ;
                            }
						});
					} else if (response == 0) {
						alert("Ubo un error");
					}
				}
			});
		}
	});

});


/*proyecto
status
finicio
ffin
lider
inversion
finalidad
poblacionP
proposito
poblacionO
presupuesto2018
presupuesto2019
indicadorEstrategico
inversion2018
inversion2019
metaIndicador*/


/*
afinicio
affinal

afisinicio
afisifinal
giinicio
gifinal
efinicio
effinal
eainicio
eafinal
ejinicio
ejfinal
rpinicio
rpfinal
scincio
scfinal
*/
