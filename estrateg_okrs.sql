/*
 Navicat Premium Data Transfer

 Source Server         : Estrategicos Aguascalientes
 Source Server Type    : MySQL
 Source Server Version : 50561
 Source Host           : www.estrategicosags.com:3306
 Source Schema         : estrateg_okrs

 Target Server Type    : MySQL
 Target Server Version : 50561
 File Encoding         : 65001

 Date: 14/02/2019 09:42:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for acciones
-- ----------------------------
DROP TABLE IF EXISTS `acciones`;
CREATE TABLE `acciones`  (
  `idAcciones` int(11) NOT NULL AUTO_INCREMENT,
  `idKr` int(11) NOT NULL,
  `accion` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `status` bit(1) NOT NULL,
  PRIMARY KEY (`idAcciones`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 110 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of acciones
-- ----------------------------
INSERT INTO `acciones` VALUES (1, 62, 'Accion de prueba', 0.00, b'1');
INSERT INTO `acciones` VALUES (2, 74, 'Ok', 100.00, b'1');
INSERT INTO `acciones` VALUES (3, 74, 'Test', 100.00, b'1');
INSERT INTO `acciones` VALUES (4, 75, 'Acción nueva', 80.00, b'0');
INSERT INTO `acciones` VALUES (5, 76, 'Acción 2 nueva', 12.00, b'1');
INSERT INTO `acciones` VALUES (6, 76, 'Nueva accion Marco', 0.00, b'1');
INSERT INTO `acciones` VALUES (7, 74, 'Test', 100.00, b'1');
INSERT INTO `acciones` VALUES (8, 74, 'Test A', 100.00, b'1');
INSERT INTO `acciones` VALUES (9, 74, 'Test B', 10.00, b'1');
INSERT INTO `acciones` VALUES (10, 74, 'OKR interno', 10.00, b'1');
INSERT INTO `acciones` VALUES (11, 74, 'OKR interno', 0.00, b'1');
INSERT INTO `acciones` VALUES (12, 74, 'Test', 0.00, b'1');
INSERT INTO `acciones` VALUES (13, 74, 'marco', 0.00, b'1');
INSERT INTO `acciones` VALUES (14, 74, 'Prueba A1', 0.00, b'1');
INSERT INTO `acciones` VALUES (15, 74, 'Prueba A2', 0.00, b'1');
INSERT INTO `acciones` VALUES (16, 75, 'Accion Marco', 1000.00, b'1');
INSERT INTO `acciones` VALUES (17, 71, 'chido ', 0.00, b'1');
INSERT INTO `acciones` VALUES (18, 71, 'ssssssss', 0.00, b'1');
INSERT INTO `acciones` VALUES (19, 71, '', 0.00, b'1');
INSERT INTO `acciones` VALUES (20, 71, '', 0.00, b'1');
INSERT INTO `acciones` VALUES (21, 74, 'Test XXXX', 0.00, b'1');
INSERT INTO `acciones` VALUES (22, 74, 'Test XXXX', 0.00, b'1');
INSERT INTO `acciones` VALUES (23, 74, 'Test XXXX', 0.00, b'1');
INSERT INTO `acciones` VALUES (24, 75, 'Test', 100.00, b'1');
INSERT INTO `acciones` VALUES (25, 75, 'Test 1', 100.00, b'1');
INSERT INTO `acciones` VALUES (26, 75, 'Test 2', 100.00, b'1');
INSERT INTO `acciones` VALUES (27, 76, 'Test A', 0.00, b'1');
INSERT INTO `acciones` VALUES (28, 76, 'Test A', 0.00, b'1');
INSERT INTO `acciones` VALUES (29, 76, 'Test A', 0.00, b'1');
INSERT INTO `acciones` VALUES (30, 76, 'A', 0.00, b'1');
INSERT INTO `acciones` VALUES (31, 76, 'A', 0.00, b'1');
INSERT INTO `acciones` VALUES (32, 76, 'A', 0.00, b'1');
INSERT INTO `acciones` VALUES (33, 76, 'A', 0.00, b'1');
INSERT INTO `acciones` VALUES (34, 76, 'B', 0.00, b'1');
INSERT INTO `acciones` VALUES (35, 76, 'B', 0.00, b'1');
INSERT INTO `acciones` VALUES (36, 71, 'Nueva', 0.00, b'1');
INSERT INTO `acciones` VALUES (37, 76, 'marco', 0.00, b'1');
INSERT INTO `acciones` VALUES (38, 72, 'ok', 0.00, b'1');
INSERT INTO `acciones` VALUES (39, 72, 'ok', 0.00, b'1');
INSERT INTO `acciones` VALUES (40, 78, 'Contratación de un proveedor para la evaluación diagnóstica de profesores y alumnos', 45.00, b'1');
INSERT INTO `acciones` VALUES (41, 78, 'Elaboración de documento de diagnóstico', 10.00, b'1');
INSERT INTO `acciones` VALUES (42, 79, 'Búsqueda de opciones de cursos de fortalecimiento del idioma', 0.00, b'1');
INSERT INTO `acciones` VALUES (43, 79, 'Búsqueda de opciones  de cursos de preparación y certificación internacional para el fortalecimiento de las cuatro habilidades del idioma', 0.00, b'1');
INSERT INTO `acciones` VALUES (44, 79, 'Búsqueda de opciones de cursos y talleres de planeación y evaluación del idioma inglés', 0.00, b'1');
INSERT INTO `acciones` VALUES (45, 79, 'Búsqueda de opciones  cursos y talleres de mejora continua en la enseñanza del inglés', 0.00, b'1');
INSERT INTO `acciones` VALUES (46, 80, 'Selección y compra de material de apoyo educativo para el óptimo funcionamiento de la enseñanza del inglés.', 0.00, b'1');
INSERT INTO `acciones` VALUES (47, 80, 'Selección y compra de equipo de cómputo, proyectores y material tecnológico para escuelas e implementen la enseñanza del inglés como segunda lengua.', 0.00, b'1');
INSERT INTO `acciones` VALUES (48, 81, 'Búsqueda de opciones para la adquisición de vales de gasolina para los Asesores Técnico Pedagógicos.\n', 0.00, b'1');
INSERT INTO `acciones` VALUES (49, 81, 'Adquisición de vales de gasolina para la oportuna asesoría, acompañamiento y seguimiento a los maestros participantes del programa bilingüe.', 0.00, b'1');
INSERT INTO `acciones` VALUES (50, 81, 'Adquisición de vales de gasolina para la oportuna asesoría, acompañamiento y seguimiento a los maestros participantes del programa bilingüe.', 0.00, b'1');
INSERT INTO `acciones` VALUES (51, 82, 'Apertura de una sede en modalidad semiescolarizada en la zona Oriente de la ciudad\n', 0.00, b'1');
INSERT INTO `acciones` VALUES (52, 82, 'Establecer un centro  de asesoría en tres municipios del Estado.\n', 0.00, b'1');
INSERT INTO `acciones` VALUES (53, 82, 'Establecer el servicio educativo cinco nuevos Centros CRECER  en el municipio de Aguascalientes y mantener los existentes', 0.00, b'1');
INSERT INTO `acciones` VALUES (54, 82, 'Recuperación del 20% de alumnos que causarán baja en el sistema escolarizado\n', 0.00, b'1');
INSERT INTO `acciones` VALUES (55, 83, 'Certificación de 18 evaluadores institucionales de la ECE del IEA.\n', 0.00, b'1');
INSERT INTO `acciones` VALUES (56, 83, 'Publicación en 1 página web de la información relativa a la certificación de competencias docentes. \n', 0.00, b'1');
INSERT INTO `acciones` VALUES (57, 83, 'Apoyar al los subsistemas participantes en el 100% de las reuniones de promoción del proyecto.\n', 0.00, b'1');
INSERT INTO `acciones` VALUES (58, 83, 'Realizar 1 evento de entrega de certificados de competencia laboral.\n', 0.00, b'1');
INSERT INTO `acciones` VALUES (59, 84, 'Reunión de aprobación y publicación de la Convocatoria para el Ingreso a la Educación  Media ', 0.00, b'1');
INSERT INTO `acciones` VALUES (60, 84, 'Reuniones de difusión (EXPO Y SISTEMA DE INGRESO) en el 100% de URSE’s del Municipio Capital', 0.00, b'1');
INSERT INTO `acciones` VALUES (61, 84, 'Desarrollo de la Expo Oferta Educativa 2019', 0.00, b'1');
INSERT INTO `acciones` VALUES (62, 84, 'Plataforma web/virtual para el ingreso ', 0.00, b'1');
INSERT INTO `acciones` VALUES (63, 84, 'Plataforma web/virtual para el ingreso ', 0.00, b'0');
INSERT INTO `acciones` VALUES (64, 85, 'Reunión de trabajo con los planteles que cuentan con menor matrícula\n', 0.00, b'1');
INSERT INTO `acciones` VALUES (65, 85, 'Reunión de análisis de los estudios de factibilidad con la Subdirección de estadística del IEA.\n', 0.00, b'1');
INSERT INTO `acciones` VALUES (66, 85, 'Propuesta de creación, absorción y reubicación de planteles con base en estudio realizado.\n', 0.00, b'1');
INSERT INTO `acciones` VALUES (67, 85, 'Presentación de propuesta oficialización de creación, absorción y/o reubicación de planteles.\n', 0.00, b'1');
INSERT INTO `acciones` VALUES (68, 86, 'Taller de capacitación para ingresar en el Padrón de Escuelas de Buena Calidad de la EMS                       \n', 0.00, b'1');
INSERT INTO `acciones` VALUES (69, 86, 'Presentación de punto de acuerdo en las juntas directivas de los 2 OPDs para el ingreso al Padrón de Escuelas de Buena Calidad de la EMS.\n', 0.00, b'1');
INSERT INTO `acciones` VALUES (70, 86, 'Reuniones de seguimiento y acompañamiento \n', 0.00, b'1');
INSERT INTO `acciones` VALUES (71, 88, 'Aplicación y seguimiento de la encuesta con directores de EMS', 0.00, b'1');
INSERT INTO `acciones` VALUES (72, 88, 'Reporte de encuesta de diagnóstico', 0.00, b'1');
INSERT INTO `acciones` VALUES (73, 89, 'Análisis del problema /primera sección del documento ejecutivo', 0.00, b'1');
INSERT INTO `acciones` VALUES (74, 89, 'Propuesta de solución /segunda sección del documento ejecutivo', 0.00, b'1');
INSERT INTO `acciones` VALUES (75, 89, 'Factibilidad de la propuesta /tercera sección del documento ejecutivo', 0.00, b'1');
INSERT INTO `acciones` VALUES (76, 90, 'Diseño de propuesta de inversión', 0.00, b'1');
INSERT INTO `acciones` VALUES (77, 90, 'Definición de la estrategia de recursos: internos y externos, así como su planificación', 0.00, b'1');
INSERT INTO `acciones` VALUES (78, 91, 'Procesos de adquisición de bienes y/o servicios para la incorporación de infraestructura de comunicación y conectividad en 25 planteles de bachillerato estatales  \n', 0.00, b'1');
INSERT INTO `acciones` VALUES (79, 91, 'Uso de la Unidad de Espacio Común por parte de los Subsistemas Públicos/Planteles de Bachillerato', 0.00, b'1');
INSERT INTO `acciones` VALUES (80, 91, 'Capacitación, actualización y/o certificación de la comunidad docente de planteles estatales  en el uso de TIC’s\n', 0.00, b'1');
INSERT INTO `acciones` VALUES (81, 91, 'Incorporación de una Plataforma de Gestión Educativa: Moodle, Edmodo, Schoology, etc.', 0.00, b'1');
INSERT INTO `acciones` VALUES (82, 92, 'Unificar conceptualmente el modelo que cumpla con los elementos básicos para poder considerarse como Modelo de Formación Dual', 0.00, b'1');
INSERT INTO `acciones` VALUES (83, 92, 'Incorporación de IES e IEMS  al modelo de Formación Dual', 0.00, b'1');
INSERT INTO `acciones` VALUES (84, 93, 'Logística de Invitación de IES a Empresarios', 0.00, b'1');
INSERT INTO `acciones` VALUES (85, 93, 'Presentación con empresarios\n', 0.00, b'1');
INSERT INTO `acciones` VALUES (86, 94, 'Promover la formalización de Convenios con Cámaras y empresa', 0.00, b'1');
INSERT INTO `acciones` VALUES (87, 94, 'Seguimiento al desarrollo de convenios', 0.00, b'1');
INSERT INTO `acciones` VALUES (88, 95, 'Promover la integración de IES , Empresas, SEDEC e IEA al consejo', 0.00, b'1');
INSERT INTO `acciones` VALUES (89, 95, 'Desarrollo de Visión y objetivos y líneas acción del Consejo Consultivo', 0.00, b'1');
INSERT INTO `acciones` VALUES (90, 96, 'Metodología de Seguimiento mensual a la reingeniería de personal de salud', 0.00, b'1');
INSERT INTO `acciones` VALUES (91, 96, 'Optimización de contratación de personal de salud sujeta a políticas federales y estatales ', 0.00, b'1');
INSERT INTO `acciones` VALUES (92, 96, 'Seguimiento de indicadores de productividad y calidad de la atención', 0.00, b'1');
INSERT INTO `acciones` VALUES (93, 96, 'Seguimiento de indicadores de productividad y calidad de la atención', 0.00, b'1');
INSERT INTO `acciones` VALUES (94, 96, 'Seguimiento de indicadores de la calidad de la atención médica', 0.00, b'1');
INSERT INTO `acciones` VALUES (95, 97, 'Rincón de Romos ', 0.00, b'1');
INSERT INTO `acciones` VALUES (96, 97, 'Tepezalá', 0.00, b'1');
INSERT INTO `acciones` VALUES (97, 97, 'Asientos', 0.00, b'1');
INSERT INTO `acciones` VALUES (98, 97, 'Villas de Nuestra Señora de la Asunción', 0.00, b'1');
INSERT INTO `acciones` VALUES (99, 98, 'Adquirir los insumos necesarios para el primer nivel de atención', 0.00, b'1');
INSERT INTO `acciones` VALUES (100, 98, 'Distribución de insumos adquiridos para primer nivel de atención', 0.00, b'1');
INSERT INTO `acciones` VALUES (101, 98, 'Control de los Insumos en el Almacén Central y Unidades Administrativas de acuerdo a puntos de re-orden y análisis de inventarios', 0.00, b'1');
INSERT INTO `acciones` VALUES (102, 117, 'ok', 0.00, b'0');
INSERT INTO `acciones` VALUES (103, 117, 'ok', 0.00, b'0');
INSERT INTO `acciones` VALUES (104, 117, 'ok 1', 0.00, b'0');
INSERT INTO `acciones` VALUES (105, 76, 'ok56', 0.00, b'1');
INSERT INTO `acciones` VALUES (106, 118, 'Ok', 100.00, b'1');
INSERT INTO `acciones` VALUES (107, 122, 'OKR Beta', 80.00, b'0');
INSERT INTO `acciones` VALUES (108, 122, 'OKR Beta-A', 50.00, b'1');
INSERT INTO `acciones` VALUES (109, 122, 'OKR Beta-B', 90.00, b'0');

-- ----------------------------
-- Table structure for acuerdos
-- ----------------------------
DROP TABLE IF EXISTS `acuerdos`;
CREATE TABLE `acuerdos`  (
  `idAcuerdo` int(255) NOT NULL AUTO_INCREMENT,
  `idMInuta` int(255) NOT NULL,
  `actividad` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `responsable` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fecha` date NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `fechaAlta` date NOT NULL,
  `status` bit(1) NOT NULL,
  PRIMARY KEY (`idAcuerdo`) USING BTREE,
  INDEX `fk_acuerdo_minuta`(`idMInuta`) USING BTREE,
  CONSTRAINT `fk_acuerdo_minuta` FOREIGN KEY (`idMInuta`) REFERENCES `minuta` (`idMinuta`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 114 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of acuerdos
-- ----------------------------
INSERT INTO `acuerdos` VALUES (51, 56, 'Actividad 1', 'Responsable', '2019-02-09', 25.00, '2019-01-25', b'1');
INSERT INTO `acuerdos` VALUES (52, 56, 'Actividad 2', 'Responsable', '2019-02-01', 0.00, '2019-01-25', b'1');
INSERT INTO `acuerdos` VALUES (53, 56, 'Actividad 3', 'Responsable', '2019-04-01', 0.00, '2019-01-25', b'1');
INSERT INTO `acuerdos` VALUES (54, 56, 'Actividad 4', 'Responsable', '2019-03-01', 0.00, '2019-01-25', b'1');
INSERT INTO `acuerdos` VALUES (55, 56, 'Actividad 5', 'Responsable', '2019-10-01', 0.00, '2019-01-25', b'1');
INSERT INTO `acuerdos` VALUES (56, 57, 'Actividad 1', 'Responsable', '2019-02-09', 25.00, '2019-01-25', b'1');
INSERT INTO `acuerdos` VALUES (57, 57, 'Actividad 2', 'Responsable', '2019-02-01', 0.00, '2019-01-25', b'1');
INSERT INTO `acuerdos` VALUES (58, 57, 'Actividad 3', 'Responsable', '2019-04-01', 0.00, '2019-01-25', b'1');
INSERT INTO `acuerdos` VALUES (59, 57, 'Actividad 4', 'Responsable', '2019-03-01', 0.00, '2019-01-25', b'1');
INSERT INTO `acuerdos` VALUES (60, 57, 'Actividad 5', 'Responsable', '2022-12-31', 0.00, '2019-01-25', b'1');
INSERT INTO `acuerdos` VALUES (61, 58, 'Actividad 1', 'Responsable', '2019-02-09', 25.00, '2019-01-25', b'1');
INSERT INTO `acuerdos` VALUES (62, 58, 'Actividad 2', 'Responsable', '2019-02-01', 0.00, '2019-01-25', b'1');
INSERT INTO `acuerdos` VALUES (63, 58, 'Actividad 3', 'Responsable', '2019-04-01', 0.00, '2019-01-25', b'1');
INSERT INTO `acuerdos` VALUES (64, 58, 'Actividad 4', 'Responsable', '2019-03-01', 0.00, '2019-01-25', b'1');
INSERT INTO `acuerdos` VALUES (65, 58, 'Actividad 5 ', 'Responsable', '2022-12-20', 0.00, '2019-01-25', b'1');
INSERT INTO `acuerdos` VALUES (66, 59, 'Actividad 1', 'Responsable', '2019-02-09', 25.00, '2019-01-25', b'1');
INSERT INTO `acuerdos` VALUES (67, 59, 'Actividad 2', 'Responsable', '2019-02-01', 0.00, '2019-01-25', b'1');
INSERT INTO `acuerdos` VALUES (68, 59, 'Actividad 3', 'Responsable', '2019-04-01', 0.00, '2019-01-25', b'1');
INSERT INTO `acuerdos` VALUES (69, 59, 'Actividad 4', 'Responsable', '2019-03-01', 0.00, '2019-01-25', b'1');
INSERT INTO `acuerdos` VALUES (77, 64, 'Actividad 1', 'Responsable 1', '2019-07-01', 96.00, '2019-01-29', b'1');
INSERT INTO `acuerdos` VALUES (78, 64, 'Actividad 2', 'Responsable 2', '2019-06-01', 90.00, '2019-01-29', b'1');
INSERT INTO `acuerdos` VALUES (85, 71, 'Actividad A', 'Juan', '2019-08-01', 100.00, '2019-02-12', b'1');
INSERT INTO `acuerdos` VALUES (91, 77, 'a', 'a', '2019-01-01', 0.00, '2019-02-12', b'0');
INSERT INTO `acuerdos` VALUES (94, 81, '1', '1', '2019-01-01', 0.00, '2019-02-12', b'1');
INSERT INTO `acuerdos` VALUES (95, 101, 'Actividad 1', 'Responsable 1', '2019-07-01', 46.00, '2019-02-12', b'1');
INSERT INTO `acuerdos` VALUES (96, 101, 'Actividad 2', 'Responsable 2', '2019-06-01', 90.00, '2019-02-12', b'1');
INSERT INTO `acuerdos` VALUES (97, 101, 'Acuerdo 3', 'Responsable 3', '2019-01-01', 0.00, '2019-02-12', b'1');
INSERT INTO `acuerdos` VALUES (99, 103, 'Acuerdo A', 'Responsable A', '2019-09-18', 0.00, '2019-02-12', b'1');
INSERT INTO `acuerdos` VALUES (100, 103, 'Acuerdo B', 'Responsable B', '2019-04-01', 0.00, '2019-02-12', b'1');
INSERT INTO `acuerdos` VALUES (101, 104, 'Acuerdo A', 'Resonsable A', '2019-01-01', 0.00, '2019-02-12', b'1');
INSERT INTO `acuerdos` VALUES (105, 110, 'Actividad beta A', 'Responsable beta A', '2019-06-01', 60.00, '2019-02-12', b'1');
INSERT INTO `acuerdos` VALUES (106, 110, 'Actividad beta B', 'Responsable beta B', '2019-08-01', 0.00, '2019-02-12', b'1');
INSERT INTO `acuerdos` VALUES (107, 111, 'Acuerdo', 'Responsable', '2019-09-01', 0.00, '2019-02-12', b'1');
INSERT INTO `acuerdos` VALUES (108, 111, 'Acuerdo', 'Responsable', '2019-10-01', 0.00, '2019-02-12', b'1');
INSERT INTO `acuerdos` VALUES (109, 112, 'Actividad 1', 'Responsable 1', '2019-07-01', 100.00, '2019-02-12', b'1');
INSERT INTO `acuerdos` VALUES (110, 112, 'Actividad 2', 'Responsable 2', '2019-06-01', 100.00, '2019-02-12', b'1');
INSERT INTO `acuerdos` VALUES (111, 113, 'aaa', 'juan', '2019-10-15', 25.00, '2019-02-12', b'1');
INSERT INTO `acuerdos` VALUES (112, 114, 'solucion ', 'berni ', '2019-02-18', 45.00, '2019-02-13', b'1');
INSERT INTO `acuerdos` VALUES (113, 115, 'a', 'dasd', '2019-06-01', 20.00, '2019-02-13', b'1');

-- ----------------------------
-- Table structure for bitacoraacciones
-- ----------------------------
DROP TABLE IF EXISTS `bitacoraacciones`;
CREATE TABLE `bitacoraacciones`  (
  `idBitacora` int(255) NOT NULL AUTO_INCREMENT,
  `idKeyResult` int(255) NOT NULL,
  `descripcion` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ultimoAvance` float(10, 2) NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `aprobado` int(1) NOT NULL,
  `fecha` datetime NOT NULL,
  `user` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `motivo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `userNoAutorizo` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `userAprobado` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`idBitacora`) USING BTREE,
  INDEX `fk_bitacora_kr`(`idKeyResult`) USING BTREE,
  INDEX `fk_bitacora_kr_user`(`user`) USING BTREE,
  CONSTRAINT `bitacoraacciones_ibfk_1` FOREIGN KEY (`idKeyResult`) REFERENCES `acciones` (`idAcciones`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `bitacoraacciones_ibfk_2` FOREIGN KEY (`user`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 227 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of bitacoraacciones
-- ----------------------------
INSERT INTO `bitacoraacciones` VALUES (199, 2, 'test', 0.00, 12.00, 1, '2019-02-06 16:54:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacciones` VALUES (200, 5, 'Test', 0.00, 12.00, 1, '2019-02-06 16:56:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacciones` VALUES (201, 2, 'ok', 12.00, 100.00, 1, '2019-02-06 16:56:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacciones` VALUES (202, 3, 'ok', 0.00, 100.00, 1, '2019-02-06 16:57:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacciones` VALUES (203, 7, 'ok', 0.00, 100.00, 1, '2019-02-06 16:57:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacciones` VALUES (204, 8, 'ok', 0.00, 100.00, 1, '2019-02-06 16:57:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacciones` VALUES (205, 9, 'Test', 0.00, 10.00, 1, '2019-02-07 18:04:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacciones` VALUES (206, 10, 'Test', 0.00, 10.00, 1, '2019-02-07 18:04:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacciones` VALUES (207, 4, '80', 0.00, 80.00, 1, '2019-02-08 16:10:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacciones` VALUES (208, 106, '100', 0.00, 100.00, 1, '2019-02-08 16:11:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacciones` VALUES (209, 16, 'Test', 0.00, 50.00, 1, '2019-02-09 09:31:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacciones` VALUES (210, 26, 'Test', 0.00, 100.00, 1, '2019-02-09 09:32:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacciones` VALUES (211, 25, '80', 0.00, 80.00, 1, '2019-02-11 13:45:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacciones` VALUES (212, 25, '100', 80.00, 100.00, 1, '2019-02-11 13:46:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacciones` VALUES (213, 24, '10', 0.00, 10.00, 1, '2019-02-11 13:47:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacciones` VALUES (214, 24, '20', 10.00, 20.00, 1, '2019-02-11 13:49:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacciones` VALUES (215, 24, 'OK', 20.00, 30.00, 1, '2019-02-12 08:45:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacciones` VALUES (216, 24, '100', 30.00, 100.00, 1, '2019-02-12 08:45:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacciones` VALUES (217, 16, '1000', 50.00, 1000.00, 1, '2019-02-12 08:46:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacciones` VALUES (218, 16, '1000', 50.00, 1000.00, 1, '2019-02-12 08:46:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacciones` VALUES (219, 40, 'test', 0.00, 45.00, 1, '2019-02-12 10:59:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacciones` VALUES (220, 41, '´+l´pñl', 0.00, 10.00, 1, '2019-02-12 11:00:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacciones` VALUES (221, 82, 'hgfgh', 0.00, 50.00, 0, '2019-02-12 11:02:00', 'cap1', NULL, NULL, NULL);
INSERT INTO `bitacoraacciones` VALUES (222, 107, 'Beta 60', 0.00, 60.00, 1, '2019-02-12 18:09:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacciones` VALUES (223, 107, 'Beta 80', 60.00, 80.00, 1, '2019-02-12 18:09:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacciones` VALUES (224, 108, 'OKR Beta C', 0.00, 50.00, 1, '2019-02-12 18:10:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacciones` VALUES (225, 109, 'OKR Beta D', 0.00, 90.00, 1, '2019-02-12 18:11:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacciones` VALUES (226, 83, 'sssssss', 0.00, 11.00, 0, '2019-02-13 12:54:00', 'cap1', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for bitacoraacuerdo
-- ----------------------------
DROP TABLE IF EXISTS `bitacoraacuerdo`;
CREATE TABLE `bitacoraacuerdo`  (
  `idBitacoraGral` int(255) NOT NULL AUTO_INCREMENT,
  `idAcuerdo` int(255) NOT NULL,
  `descripcion` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ultimoAvance` float(10, 2) NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `aprobado` int(1) NOT NULL,
  `fecha` datetime NOT NULL,
  `user` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `motivo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `userNoAutorizo` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `userAprobado` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`idBitacoraGral`) USING BTREE,
  INDEX `fk_bitacora_acuerdo`(`idAcuerdo`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 96 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of bitacoraacuerdo
-- ----------------------------
INSERT INTO `bitacoraacuerdo` VALUES (14, 40, 'Test', 0.00, 50.00, 0, '2019-01-24 16:22:00', 'cap1', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (15, 42, 'tal cosa ', 0.00, 9.00, 1, '2019-01-25 15:34:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (16, 42, 'avance\n', 9.00, 70.00, 1, '2019-01-25 15:38:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (17, 42, 'retroceso\n', 70.00, 100.00, 1, '2019-01-25 15:39:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (18, 47, 'Testing', 0.00, 25.00, 1, '2019-01-25 17:56:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (19, 75, 'Actualización', 0.00, 10.00, 1, '2019-01-29 16:57:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (20, 76, 'Actualización B', 0.00, 25.00, 1, '2019-01-29 16:57:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (21, 77, 'Test', 10.00, 15.00, 1, '2019-02-06 12:25:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (22, 77, 'Test', 15.00, 16.00, 1, '2019-02-06 12:26:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (23, 78, 'TEst', 0.00, 4.00, 1, '2019-02-09 10:35:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (24, 78, 'OK', 4.00, 80.00, 1, '2019-02-11 13:42:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (25, 77, '80', 16.00, 80.00, 1, '2019-02-11 13:43:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (26, 80, '90', 0.00, 90.00, 1, '2019-02-11 13:44:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (27, 85, 'Test', 0.00, 100.00, 1, '2019-02-12 09:03:00', 'acuerdos', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (28, 77, 'T', 80.00, 85.00, 1, '2019-02-12 11:04:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (29, 77, 'Test', 85.00, 90.00, 1, '2019-02-12 11:24:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (30, 77, 'Test', 90.00, 91.00, 1, '2019-02-12 11:32:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (31, 78, 'Test', 80.00, 81.00, 1, '2019-02-12 11:32:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (32, 78, 'Test', 81.00, 82.00, 1, '2019-02-12 11:35:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (33, 77, 'Test', 91.00, 93.00, 1, '2019-02-12 11:37:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (34, 78, 'Test', 82.00, 83.00, 1, '2019-02-12 11:37:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (35, 78, 'Test', 83.00, 84.00, 1, '2019-02-12 11:38:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (36, 78, 'Test', 84.00, 85.00, 1, '2019-02-12 11:42:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (37, 77, 'Test', 93.00, 95.00, 1, '2019-02-12 11:46:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (38, 78, 'Test', 85.00, 90.00, 1, '2019-02-12 11:47:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (39, 77, 'avance', 95.00, 96.00, 1, '2019-02-12 12:18:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (40, 79, 'none', 10.00, 11.00, 1, '2019-02-12 12:21:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (41, 79, 'texto updated', 11.00, 12.00, 1, '2019-02-12 12:24:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (42, 79, 'trece texto', 12.00, 13.00, 1, '2019-02-12 12:57:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (43, 79, 'actualizacion', 13.00, 14.00, 1, '2019-02-12 13:06:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (44, 79, 'quince', 14.00, 15.00, 1, '2019-02-12 13:09:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (45, 79, 'diseicies', 15.00, 16.00, 1, '2019-02-12 13:10:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (46, 92, 'se hizo tal cosa ', 0.00, 4.00, 1, '2019-02-12 13:11:00', 'RocioGonzalez ', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (47, 79, 'kmklmlsk', 16.00, 17.00, 1, '2019-02-12 13:11:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (48, 79, 'diesiciocho', 17.00, 18.00, 1, '2019-02-12 13:14:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (49, 79, '19', 18.00, 19.00, 1, '2019-02-12 13:18:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (50, 79, 'descripcion', 19.00, 20.00, 1, '2019-02-12 13:58:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (51, 79, '21', 20.00, 21.00, 1, '2019-02-12 14:39:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (52, 79, '22', 21.00, 22.00, 1, '2019-02-12 14:44:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (53, 79, '33', 22.00, 33.00, 1, '2019-02-12 14:45:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (54, 79, '34', 33.00, 34.00, 1, '2019-02-12 14:46:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (55, 79, 'hola', 34.00, 35.00, 1, '2019-02-12 14:47:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (56, 79, '36', 35.00, 36.00, 1, '2019-02-12 14:55:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (57, 79, '37', 36.00, 37.00, 1, '2019-02-12 15:29:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (58, 79, '3/8', 37.00, 38.00, 1, '2019-02-12 15:43:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (59, 79, '39', 38.00, 39.00, 1, '2019-02-12 15:44:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (60, 79, '40', 39.00, 40.00, 1, '2019-02-12 15:45:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (61, 79, '41', 40.00, 41.00, 1, '2019-02-12 15:45:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (62, 79, '42', 41.00, 42.00, 1, '2019-02-12 15:46:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (63, 79, '43', 42.00, 43.00, 1, '2019-02-12 16:09:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (64, 79, '44', 43.00, 44.00, 1, '2019-02-12 16:26:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (65, 79, '45', 44.00, 45.00, 1, '2019-02-12 16:32:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (66, 79, '46', 45.00, 46.00, 1, '2019-02-12 16:42:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (67, 81, '11', 10.00, 11.00, 1, '2019-02-12 17:08:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (68, 81, '12', 11.00, 12.00, 1, '2019-02-12 17:47:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (69, 81, '13', 12.00, 13.00, 1, '2019-02-12 18:52:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (70, 83, '1', 0.00, 1.00, 1, '2019-02-12 18:52:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (71, 92, '165', 4.00, 15.00, 1, '2019-02-12 18:52:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (72, 104, 'Test', 0.00, 40.00, 1, '2019-02-12 18:52:00', 'lider-beta', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (73, 82, '14', 0.00, 12.00, 1, '2019-02-12 18:53:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (74, 81, '14', 13.00, 14.00, 1, '2019-02-12 18:53:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (75, 104, 'Test', 40.00, 60.00, 1, '2019-02-12 18:53:00', 'lider-beta', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (76, 82, 'Test', 12.00, 15.00, 1, '2019-02-12 18:56:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (77, 83, 'Test', 1.00, 15.00, 1, '2019-02-12 18:56:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (78, 81, 'Test', 14.00, 15.00, 1, '2019-02-12 19:12:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (79, 82, 'asd', 15.00, 16.00, 1, '2019-02-12 19:14:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (80, 81, 'Test', 15.00, 16.00, 1, '2019-02-12 19:19:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (81, 81, 'Test', 16.00, 17.00, 1, '2019-02-12 19:21:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (82, 81, 'Tst', 17.00, 19.00, 1, '2019-02-12 19:25:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (83, 81, 'Test', 19.00, 20.00, 1, '2019-02-12 19:26:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (84, 81, 'ASD', 20.00, 21.00, 1, '2019-02-12 19:28:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (85, 81, 'test', 21.00, 50.00, 1, '2019-02-12 19:31:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (86, 82, 'Test', 16.00, 50.00, 1, '2019-02-12 19:31:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (87, 82, 'Tets', 50.00, 100.00, 1, '2019-02-12 19:35:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (88, 81, 'Test', 50.00, 60.00, 1, '2019-02-12 19:36:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (89, 81, 'Test', 60.00, 70.00, 1, '2019-02-12 19:39:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (90, 81, 'Test', 70.00, 90.00, 1, '2019-02-12 19:48:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (91, 81, 'Test', 90.00, 100.00, 1, '2019-02-12 19:48:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (92, 83, 'Test', 15.00, 20.00, 1, '2019-02-12 19:51:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (93, 83, 'Test', 20.00, 25.00, 1, '2019-02-12 19:53:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (94, 84, 'Test', 0.00, 20.00, 1, '2019-02-12 19:53:00', 'EderNoda', NULL, NULL, NULL);
INSERT INTO `bitacoraacuerdo` VALUES (95, 112, 'jjsajjkasj', 15.00, 45.00, 1, '2019-02-13 11:27:00', 'RocioGonzalez ', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for bitacoraindicadores
-- ----------------------------
DROP TABLE IF EXISTS `bitacoraindicadores`;
CREATE TABLE `bitacoraindicadores`  (
  `idBitacora` int(255) NOT NULL AUTO_INCREMENT,
  `idIndicador` int(255) NOT NULL,
  `descripcion` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ultimoAvance` float(10, 2) NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `aprobado` int(1) NOT NULL,
  `fecha` datetime NOT NULL,
  `user` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `motivo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `userNoAutorizo` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `userAprobado` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`idBitacora`) USING BTREE,
  INDEX `fk_bitacora_kr`(`idIndicador`) USING BTREE,
  INDEX `bitacoraIndicador_user_fk`(`user`) USING BTREE,
  CONSTRAINT `bitacoraindicadores_ibfk_1` FOREIGN KEY (`idIndicador`) REFERENCES `indicadores` (`idIndicadores`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `bitacoraIndicador_user_fk` FOREIGN KEY (`user`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 91 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of bitacoraindicadores
-- ----------------------------
INSERT INTO `bitacoraindicadores` VALUES (65, 41, 'Descripcion de prueba', 0.00, 2500.00, 1, '2019-01-24 08:16:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraindicadores` VALUES (66, 42, 'Test', 0.00, 50.00, 3, '2019-01-24 08:26:00', 'sadmin', 'No autorizar de prueba', 'sadmin', NULL);
INSERT INTO `bitacoraindicadores` VALUES (67, 49, 'Prueba', 0.00, 12.00, 1, '2019-01-28 16:50:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraindicadores` VALUES (68, 201, 'OK', 0.00, 2.00, 1, '2019-02-07 18:11:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraindicadores` VALUES (69, 202, 'ok', 0.00, 5.00, 3, '2019-02-07 18:20:00', 'sadmin', 'Ok', 'sadmin', NULL);
INSERT INTO `bitacoraindicadores` VALUES (70, 202, 'Test', 0.00, 5.00, 1, '2019-02-07 18:31:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraindicadores` VALUES (71, 204, 'Test', 0.00, 5.00, 3, '2019-02-07 18:40:00', 'sadmin', 'ok', 'sadmin', NULL);
INSERT INTO `bitacoraindicadores` VALUES (72, 205, 'OK', 0.00, 5.00, 3, '2019-02-07 18:41:00', 'sadmin', 'ok', 'sadmin', NULL);
INSERT INTO `bitacoraindicadores` VALUES (73, 206, 'Test', 0.00, 10.00, 3, '2019-02-08 12:08:00', 'lider1', 'Test', 'lider1', NULL);
INSERT INTO `bitacoraindicadores` VALUES (74, 201, 'Avance 5', 2.00, 5.00, 1, '2019-02-08 17:13:00', 'cap1', NULL, NULL, 'sadmin');
INSERT INTO `bitacoraindicadores` VALUES (75, 204, 'Tes', 0.00, 2.00, 1, '2019-02-12 05:35:00', 'sadmin', NULL, NULL, 'sadmin');
INSERT INTO `bitacoraindicadores` VALUES (76, 201, 'Terminado', 5.00, 15.00, 1, '2019-02-12 05:36:00', 'sadmin', NULL, NULL, 'sadmin');
INSERT INTO `bitacoraindicadores` VALUES (77, 202, 'Test', 5.00, 11.00, 1, '2019-02-12 08:49:00', 'lider1', NULL, NULL, 'lider1');
INSERT INTO `bitacoraindicadores` VALUES (78, 203, 'Test 2', 0.00, 2.00, 1, '2019-02-12 08:49:00', 'lider1', NULL, NULL, 'lider1');
INSERT INTO `bitacoraindicadores` VALUES (79, 203, 'Test', 2.00, 5.00, 1, '2019-02-12 08:50:00', 'lider1', NULL, NULL, 'lider1');
INSERT INTO `bitacoraindicadores` VALUES (80, 207, '50', 0.00, 2.00, 1, '2019-02-12 09:22:00', 'acuerdos', NULL, NULL, 'acuerdos');
INSERT INTO `bitacoraindicadores` VALUES (81, 208, 'Test', 0.00, 5.00, 1, '2019-02-12 12:44:00', 'sadmin', NULL, NULL, 'sadmin');
INSERT INTO `bitacoraindicadores` VALUES (82, 208, 'tEST', 5.00, 6.00, 1, '2019-02-12 13:45:00', 'sadmin', NULL, NULL, 'sadmin');
INSERT INTO `bitacoraindicadores` VALUES (83, 225, 'Beta 5', 0.00, 5.00, 1, '2019-02-12 18:02:00', 'sadmin', NULL, NULL, 'sadmin');
INSERT INTO `bitacoraindicadores` VALUES (84, 225, 'Beta 60', 5.00, 60.00, 1, '2019-02-12 18:03:00', 'sadmin', NULL, NULL, 'sadmin');
INSERT INTO `bitacoraindicadores` VALUES (85, 225, 'Beta 50', 60.00, 96.00, 3, '2019-02-12 18:04:00', 'sadmin', 'Beta 60', 'sadmin', NULL);
INSERT INTO `bitacoraindicadores` VALUES (86, 232, 'Beta 100', 0.00, 100.00, 1, '2019-02-12 18:05:00', 'sadmin', NULL, NULL, 'sadmin');
INSERT INTO `bitacoraindicadores` VALUES (87, 231, 'Test', 0.00, 3.00, 0, '2019-02-13 09:14:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraindicadores` VALUES (88, 230, 'Test', 0.00, 5.00, 0, '2019-02-13 09:15:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraindicadores` VALUES (89, 226, 'ok', 0.00, 12.00, 0, '2019-02-13 11:17:00', 'enlace-beta', NULL, NULL, NULL);
INSERT INTO `bitacoraindicadores` VALUES (90, 227, 'OK', 0.00, 12.00, 0, '2019-02-13 11:18:00', 'enlace-beta', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for bitacorakeyresult
-- ----------------------------
DROP TABLE IF EXISTS `bitacorakeyresult`;
CREATE TABLE `bitacorakeyresult`  (
  `idBitacora` int(255) NOT NULL AUTO_INCREMENT,
  `idKeyResult` int(255) NOT NULL,
  `descripcion` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ultimoAvance` float(10, 2) NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `aprobado` int(1) NOT NULL,
  `fecha` datetime NOT NULL,
  `user` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `motivo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `userNoAutorizo` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `userAprobado` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`idBitacora`) USING BTREE,
  INDEX `fk_bitacora_kr`(`idKeyResult`) USING BTREE,
  INDEX `fk_bitacora_kr_user`(`user`) USING BTREE,
  CONSTRAINT `fk_bitacora_kr` FOREIGN KEY (`idKeyResult`) REFERENCES `keyresult` (`idKeyResult`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_bitacora_kr_user` FOREIGN KEY (`user`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 160 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of bitacorakeyresult
-- ----------------------------
INSERT INTO `bitacorakeyresult` VALUES (151, 70, 'Test', 0.00, 12.00, 1, '2019-01-24 09:10:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacorakeyresult` VALUES (152, 70, 'Test', 12.00, 20.00, 1, '2019-01-24 09:11:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacorakeyresult` VALUES (153, 74, 'Prueba', 0.00, 1.00, 1, '2019-01-28 16:52:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacorakeyresult` VALUES (154, 76, 'Prueba', 0.00, 100.00, 1, '2019-01-28 16:52:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacorakeyresult` VALUES (155, 75, 'Prueba', 0.00, 12.00, 1, '2019-01-28 16:53:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacorakeyresult` VALUES (156, 75, 'Test', 12.00, 25.00, 1, '2019-01-28 16:54:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacorakeyresult` VALUES (157, 77, 'Test', 0.00, 12.00, 1, '2019-01-29 11:13:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacorakeyresult` VALUES (158, 117, 'Test', 0.00, 100.00, 1, '2019-02-12 09:06:00', 'acuerdos', NULL, NULL, NULL);
INSERT INTO `bitacorakeyresult` VALUES (159, 120, 'se puso un porcentaje del 50% en camas', 0.00, 9.00, 1, '2019-02-12 13:04:00', 'Jorge.Uria', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for bitacorakeyresult_copy
-- ----------------------------
DROP TABLE IF EXISTS `bitacorakeyresult_copy`;
CREATE TABLE `bitacorakeyresult_copy`  (
  `idBitacora` int(255) NOT NULL AUTO_INCREMENT,
  `idKeyResult` int(255) NOT NULL,
  `descripcion` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ultimoAvance` float(10, 2) NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `aprobado` int(1) NOT NULL,
  `fecha` datetime NOT NULL,
  `user` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `motivo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `userNoAutorizo` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`idBitacora`) USING BTREE,
  INDEX `fk_bitacora_kr`(`idKeyResult`) USING BTREE,
  INDEX `fk_bitacora_kr_user`(`user`) USING BTREE,
  CONSTRAINT `bitacorakeyresult_copy_ibfk_1` FOREIGN KEY (`idKeyResult`) REFERENCES `keyresult` (`idKeyResult`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `bitacorakeyresult_copy_ibfk_2` FOREIGN KEY (`user`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 158 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of bitacorakeyresult_copy
-- ----------------------------
INSERT INTO `bitacorakeyresult_copy` VALUES (151, 70, 'Test', 0.00, 12.00, 1, '2019-01-24 09:10:00', 'sadmin', NULL, NULL);
INSERT INTO `bitacorakeyresult_copy` VALUES (152, 70, 'Test', 12.00, 20.00, 1, '2019-01-24 09:11:00', 'sadmin', NULL, NULL);
INSERT INTO `bitacorakeyresult_copy` VALUES (153, 74, 'Prueba', 0.00, 1.00, 1, '2019-01-28 16:52:00', 'sadmin', NULL, NULL);
INSERT INTO `bitacorakeyresult_copy` VALUES (154, 76, 'Prueba', 0.00, 100.00, 1, '2019-01-28 16:52:00', 'sadmin', NULL, NULL);
INSERT INTO `bitacorakeyresult_copy` VALUES (155, 75, 'Prueba', 0.00, 12.00, 1, '2019-01-28 16:53:00', 'sadmin', NULL, NULL);
INSERT INTO `bitacorakeyresult_copy` VALUES (156, 75, 'Test', 12.00, 25.00, 1, '2019-01-28 16:54:00', 'sadmin', NULL, NULL);
INSERT INTO `bitacorakeyresult_copy` VALUES (157, 77, 'Test', 0.00, 12.00, 1, '2019-01-29 11:13:00', 'sadmin', NULL, NULL);

-- ----------------------------
-- Table structure for bitacoramovimientos
-- ----------------------------
DROP TABLE IF EXISTS `bitacoramovimientos`;
CREATE TABLE `bitacoramovimientos`  (
  `idBitacora` int(255) NOT NULL AUTO_INCREMENT,
  `movimiento` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `usuario` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  PRIMARY KEY (`idBitacora`) USING BTREE,
  INDEX `fk_bitacora_usuario`(`usuario`) USING BTREE,
  CONSTRAINT `fk_bitacora_usuario` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 211 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of bitacoramovimientos
-- ----------------------------
INSERT INTO `bitacoramovimientos` VALUES (108, 'Alta de nuevo plan', 'sadmin', '2019-01-24', '08:14:00');
INSERT INTO `bitacoramovimientos` VALUES (109, 'Alta de objetivo', 'sadmin', '2019-01-24', '09:04:00');
INSERT INTO `bitacoramovimientos` VALUES (110, 'Alta de objetivo', 'sadmin', '2019-01-24', '09:04:00');
INSERT INTO `bitacoramovimientos` VALUES (111, 'Alta de usuario', 'sadmin', '2019-01-24', '09:08:00');
INSERT INTO `bitacoramovimientos` VALUES (112, 'Alta de objetivo', 'sadmin', '2019-01-24', '11:00:00');
INSERT INTO `bitacoramovimientos` VALUES (113, 'Alta de usuario', 'sadmin', '2019-01-24', '11:01:00');
INSERT INTO `bitacoramovimientos` VALUES (114, 'Alta de nuevo plan', 'sadmin', '2019-01-28', '16:49:00');
INSERT INTO `bitacoramovimientos` VALUES (115, 'Alta de objetivo', 'sadmin', '2019-01-28', '16:51:00');
INSERT INTO `bitacoramovimientos` VALUES (116, 'Alta de usuario', 'sadmin', '2019-01-28', '16:51:00');
INSERT INTO `bitacoramovimientos` VALUES (117, 'Alta de usuario', 'sadmin', '2019-01-28', '16:51:00');
INSERT INTO `bitacoramovimientos` VALUES (118, 'Alta de usuario', 'sadmin', '2019-01-28', '16:51:00');
INSERT INTO `bitacoramovimientos` VALUES (119, 'Alta de nuevo plan', 'sadmin', '2019-01-29', '13:33:00');
INSERT INTO `bitacoramovimientos` VALUES (120, 'Alta de usuario: RocioGonzalez ', 'sadmin', '2019-01-30', '09:52:00');
INSERT INTO `bitacoramovimientos` VALUES (121, 'Alta de usuario: kevinreyes', 'sadmin', '2019-01-30', '12:12:00');
INSERT INTO `bitacoramovimientos` VALUES (122, 'Alta de usuario: EderNoda', 'kevinreyes', '2019-01-30', '12:19:00');
INSERT INTO `bitacoramovimientos` VALUES (123, 'Alta de usuario: BernardoLópez  ', 'kevinreyes', '2019-01-30', '12:22:00');
INSERT INTO `bitacoramovimientos` VALUES (124, 'Alta de usuario: JuanBarcenas ', 'kevinreyes', '2019-01-30', '12:25:00');
INSERT INTO `bitacoramovimientos` VALUES (125, 'Alta de usuario: FernandaNavarro', 'kevinreyes', '2019-01-30', '14:34:00');
INSERT INTO `bitacoramovimientos` VALUES (126, 'Eliminacion del plan: 6', 'kevinreyes', '2019-02-05', '12:26:00');
INSERT INTO `bitacoramovimientos` VALUES (127, 'Eliminacion del plan: 7', 'kevinreyes', '2019-02-05', '12:26:00');
INSERT INTO `bitacoramovimientos` VALUES (128, 'Alta de nuevo plan', 'kevinreyes', '2019-02-05', '12:35:00');
INSERT INTO `bitacoramovimientos` VALUES (129, 'Alta de nuevo plan', 'kevinreyes', '2019-02-05', '12:39:00');
INSERT INTO `bitacoramovimientos` VALUES (130, 'Alta de nuevo plan', 'kevinreyes', '2019-02-05', '12:52:00');
INSERT INTO `bitacoramovimientos` VALUES (131, 'Alta de nuevo plan', 'kevinreyes', '2019-02-05', '12:52:00');
INSERT INTO `bitacoramovimientos` VALUES (132, 'Eliminacion del plan: 12', 'kevinreyes', '2019-02-05', '12:52:00');
INSERT INTO `bitacoramovimientos` VALUES (133, 'Alta de nuevo plan', 'kevinreyes', '2019-02-05', '13:05:00');
INSERT INTO `bitacoramovimientos` VALUES (134, 'Alta de nuevo plan', 'kevinreyes', '2019-02-05', '13:11:00');
INSERT INTO `bitacoramovimientos` VALUES (135, 'Alta de nuevo plan', 'kevinreyes', '2019-02-05', '13:15:00');
INSERT INTO `bitacoramovimientos` VALUES (136, 'Alta de nuevo plan', 'kevinreyes', '2019-02-05', '13:29:00');
INSERT INTO `bitacoramovimientos` VALUES (137, 'Alta de nuevo plan', 'kevinreyes', '2019-02-05', '13:32:00');
INSERT INTO `bitacoramovimientos` VALUES (138, 'Alta de nuevo plan', 'kevinreyes', '2019-02-05', '13:54:00');
INSERT INTO `bitacoramovimientos` VALUES (139, 'Alta de nuevo plan', 'kevinreyes', '2019-02-05', '14:03:00');
INSERT INTO `bitacoramovimientos` VALUES (140, 'Alta de nuevo plan', 'kevinreyes', '2019-02-05', '14:08:00');
INSERT INTO `bitacoramovimientos` VALUES (141, 'Alta de nuevo plan', 'kevinreyes', '2019-02-05', '14:13:00');
INSERT INTO `bitacoramovimientos` VALUES (142, 'Alta de nuevo plan', 'kevinreyes', '2019-02-05', '14:17:00');
INSERT INTO `bitacoramovimientos` VALUES (143, 'Alta de nuevo plan', 'kevinreyes', '2019-02-05', '14:25:00');
INSERT INTO `bitacoramovimientos` VALUES (144, 'Alta de nuevo plan', 'kevinreyes', '2019-02-05', '14:28:00');
INSERT INTO `bitacoramovimientos` VALUES (145, 'Alta de nuevo plan', 'kevinreyes', '2019-02-05', '14:31:00');
INSERT INTO `bitacoramovimientos` VALUES (146, 'Alta de objetivo', 'kevinreyes', '2019-02-05', '14:36:00');
INSERT INTO `bitacoramovimientos` VALUES (147, 'Alta de usuario', 'kevinreyes', '2019-02-05', '14:36:00');
INSERT INTO `bitacoramovimientos` VALUES (148, 'Alta de objetivo', 'kevinreyes', '2019-02-05', '15:05:00');
INSERT INTO `bitacoramovimientos` VALUES (149, 'Alta de objetivo', 'kevinreyes', '2019-02-05', '15:05:00');
INSERT INTO `bitacoramovimientos` VALUES (150, 'Alta de usuario', 'kevinreyes', '2019-02-05', '15:06:00');
INSERT INTO `bitacoramovimientos` VALUES (151, 'Alta de objetivo', 'kevinreyes', '2019-02-05', '15:22:00');
INSERT INTO `bitacoramovimientos` VALUES (152, 'Alta de usuario', 'kevinreyes', '2019-02-05', '15:22:00');
INSERT INTO `bitacoramovimientos` VALUES (153, 'Alta de objetivo', 'kevinreyes', '2019-02-05', '15:25:00');
INSERT INTO `bitacoramovimientos` VALUES (154, 'Alta de usuario', 'kevinreyes', '2019-02-05', '15:26:00');
INSERT INTO `bitacoramovimientos` VALUES (155, 'Alta de objetivo', 'kevinreyes', '2019-02-05', '16:00:00');
INSERT INTO `bitacoramovimientos` VALUES (156, 'Alta de usuario', 'kevinreyes', '2019-02-05', '16:00:00');
INSERT INTO `bitacoramovimientos` VALUES (157, 'Alta de objetivo', 'kevinreyes', '2019-02-05', '16:05:00');
INSERT INTO `bitacoramovimientos` VALUES (158, 'Alta de usuario', 'kevinreyes', '2019-02-05', '16:06:00');
INSERT INTO `bitacoramovimientos` VALUES (159, 'Alta de objetivo', 'kevinreyes', '2019-02-05', '16:07:00');
INSERT INTO `bitacoramovimientos` VALUES (160, 'Alta de usuario', 'kevinreyes', '2019-02-05', '16:07:00');
INSERT INTO `bitacoramovimientos` VALUES (161, 'Alta de objetivo', 'kevinreyes', '2019-02-05', '16:09:00');
INSERT INTO `bitacoramovimientos` VALUES (162, 'Alta de usuario', 'kevinreyes', '2019-02-05', '16:09:00');
INSERT INTO `bitacoramovimientos` VALUES (163, 'Alta de objetivo', 'kevinreyes', '2019-02-06', '17:21:00');
INSERT INTO `bitacoramovimientos` VALUES (164, 'Alta de objetivo', 'kevinreyes', '2019-02-06', '17:25:00');
INSERT INTO `bitacoramovimientos` VALUES (165, 'Alta de usuario', 'kevinreyes', '2019-02-06', '17:25:00');
INSERT INTO `bitacoramovimientos` VALUES (166, 'Alta de nuevo plan', 'sadmin', '2019-02-07', '18:09:00');
INSERT INTO `bitacoramovimientos` VALUES (167, 'Edicio de usuario: cap1', 'sadmin', '2019-02-07', '18:11:00');
INSERT INTO `bitacoramovimientos` VALUES (168, 'Alta de usuario: lider1', 'sadmin', '2019-02-07', '18:19:00');
INSERT INTO `bitacoramovimientos` VALUES (169, 'Alta de objetivo', 'sadmin', '2019-02-07', '18:52:00');
INSERT INTO `bitacoramovimientos` VALUES (170, 'Alta de usuario', 'sadmin', '2019-02-07', '18:52:00');
INSERT INTO `bitacoramovimientos` VALUES (171, 'Alta de usuario: usuario-super-admin', 'sadmin', '2019-02-09', '10:39:00');
INSERT INTO `bitacoramovimientos` VALUES (172, 'Edicio de usuario: usuario-super-a', 'sadmin', '2019-02-09', '11:10:00');
INSERT INTO `bitacoramovimientos` VALUES (173, 'Edicio de usuario: usuario-super-a', 'sadmin', '2019-02-09', '11:11:00');
INSERT INTO `bitacoramovimientos` VALUES (174, 'Edicio de usuario: usuario-super-a', 'sadmin', '2019-02-09', '11:19:00');
INSERT INTO `bitacoramovimientos` VALUES (175, 'Edicio de usuario: usuario-super-a', 'sadmin', '2019-02-09', '11:20:00');
INSERT INTO `bitacoramovimientos` VALUES (176, 'Edicio de usuario: usuario-super-a', 'sadmin', '2019-02-09', '11:21:00');
INSERT INTO `bitacoramovimientos` VALUES (177, 'Edicio de usuario: usuario-super-a', 'sadmin', '2019-02-09', '11:21:00');
INSERT INTO `bitacoramovimientos` VALUES (178, 'Alta de usuario: lidera', 'sadmin', '2019-02-12', '05:42:00');
INSERT INTO `bitacoramovimientos` VALUES (179, 'Alta de usuario: acuerdos', 'sadmin', '2019-02-12', '08:59:00');
INSERT INTO `bitacoramovimientos` VALUES (180, 'Edicio de usuario: acuerdos', 'sadmin', '2019-02-12', '10:02:00');
INSERT INTO `bitacoramovimientos` VALUES (181, 'Edicio de usuario: lider', 'sadmin', '2019-02-12', '10:04:00');
INSERT INTO `bitacoramovimientos` VALUES (182, 'Edicio de usuario: cap1', 'sadmin', '2019-02-12', '11:01:00');
INSERT INTO `bitacoramovimientos` VALUES (183, 'Edicio de usuario: cap1', 'sadmin', '2019-02-12', '11:39:00');
INSERT INTO `bitacoramovimientos` VALUES (184, 'Alta de usuario: Jorge.Uria', 'kevinreyes', '2019-02-12', '12:57:00');
INSERT INTO `bitacoramovimientos` VALUES (185, 'Edicio de usuario: Jorge.Uria', 'kevinreyes', '2019-02-12', '12:59:00');
INSERT INTO `bitacoramovimientos` VALUES (186, 'Alta de objetivo', 'Jorge.Uria', '2019-02-12', '13:01:00');
INSERT INTO `bitacoramovimientos` VALUES (187, 'Edicio de usuario: Jorge.Uria', 'kevinreyes', '2019-02-12', '13:20:00');
INSERT INTO `bitacoramovimientos` VALUES (188, 'Edicio de usuario: acuerdos', 'sadmin', '2019-02-12', '13:49:00');
INSERT INTO `bitacoramovimientos` VALUES (189, 'Edicio de usuario: acuerdos', 'sadmin', '2019-02-12', '13:50:00');
INSERT INTO `bitacoramovimientos` VALUES (190, 'Edicio de usuario: acuerdos', 'sadmin', '2019-02-12', '13:50:00');
INSERT INTO `bitacoramovimientos` VALUES (191, 'Alta de nuevo plan', 'acuerdos', '2019-02-12', '13:54:00');
INSERT INTO `bitacoramovimientos` VALUES (192, 'Edicio de usuario: acuerdos', 'sadmin', '2019-02-12', '14:07:00');
INSERT INTO `bitacoramovimientos` VALUES (193, 'Alta de usuario: lider001', 'sadmin', '2019-02-12', '14:08:00');
INSERT INTO `bitacoramovimientos` VALUES (194, 'Edicio de usuario: lider001', 'sadmin', '2019-02-12', '14:09:00');
INSERT INTO `bitacoramovimientos` VALUES (195, 'Alta de nuevo plan', 'acuerdos', '2019-02-12', '14:11:00');
INSERT INTO `bitacoramovimientos` VALUES (196, 'Alta de usuario: David Sánchez Padilla', 'kevinreyes', '2019-02-12', '14:57:00');
INSERT INTO `bitacoramovimientos` VALUES (197, 'Alta de usuario: superadmin-a', 'sadmin', '2019-02-12', '17:48:00');
INSERT INTO `bitacoramovimientos` VALUES (198, 'Alta de nuevo plan', 'sadmin', '2019-02-12', '18:02:00');
INSERT INTO `bitacoramovimientos` VALUES (199, 'Alta de objetivo', 'sadmin', '2019-02-12', '18:08:00');
INSERT INTO `bitacoramovimientos` VALUES (200, 'Alta de usuario', 'sadmin', '2019-02-12', '18:08:00');
INSERT INTO `bitacoramovimientos` VALUES (201, 'Alta de usuario: enlace-beta', 'sadmin', '2019-02-12', '18:14:00');
INSERT INTO `bitacoramovimientos` VALUES (202, 'Edicio de usuario: enlace-beta', 'sadmin', '2019-02-12', '18:30:00');
INSERT INTO `bitacoramovimientos` VALUES (203, 'Alta de usuario: lider-beta', 'sadmin', '2019-02-12', '18:52:00');
INSERT INTO `bitacoramovimientos` VALUES (204, 'Eliminacion del plan: 26', 'sadmin', '2019-02-12', '20:59:00');
INSERT INTO `bitacoramovimientos` VALUES (205, 'Edicio de usuario: acuerdos', 'sadmin', '2019-02-13', '09:18:00');
INSERT INTO `bitacoramovimientos` VALUES (206, 'Alta de nuevo plan', 'kevinreyes', '2019-02-13', '12:25:00');
INSERT INTO `bitacoramovimientos` VALUES (207, 'Alta de nuevo plan', 'kevinreyes', '2019-02-13', '12:25:00');
INSERT INTO `bitacoramovimientos` VALUES (208, 'Alta de nuevo plan', 'kevinreyes', '2019-02-13', '12:26:00');
INSERT INTO `bitacoramovimientos` VALUES (209, 'Alta de nuevo plan', 'kevinreyes', '2019-02-13', '12:26:00');
INSERT INTO `bitacoramovimientos` VALUES (210, 'Alta de nuevo plan', 'kevinreyes', '2019-02-13', '12:26:00');

-- ----------------------------
-- Table structure for chat
-- ----------------------------
DROP TABLE IF EXISTS `chat`;
CREATE TABLE `chat`  (
  `idChat` int(255) NOT NULL AUTO_INCREMENT,
  `mensaje` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `idUsuario` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `tipo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `idTipo` int(255) NOT NULL,
  `fechahora` datetime NOT NULL,
  `status` bit(1) NOT NULL,
  PRIMARY KEY (`idChat`) USING BTREE,
  INDEX `fk_chat_usuario`(`idUsuario`) USING BTREE,
  CONSTRAINT `fk_chat_usuario` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 116 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of chat
-- ----------------------------
INSERT INTO `chat` VALUES (112, 'OK', 'sadmin', 'objetivo', 60, '2019-02-06 17:01:00', b'0');
INSERT INTO `chat` VALUES (113, 'Favor de actualizar los datos lo antes posible.', 'sadmin', 'objetivo', 60, '2019-02-06 17:02:00', b'0');
INSERT INTO `chat` VALUES (114, 'Test', 'gob', 'objetivo', 74, '2019-02-12 20:06:00', b'0');
INSERT INTO `chat` VALUES (115, 'este objetivo no muestra los avances adecuados', 'kevinreyes', 'objetivo', 61, '2019-02-13 11:18:00', b'0');

-- ----------------------------
-- Table structure for indicadores
-- ----------------------------
DROP TABLE IF EXISTS `indicadores`;
CREATE TABLE `indicadores`  (
  `idIndicadores` int(255) NOT NULL AUTO_INCREMENT,
  `nombreIndicador` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `inicio` float(10, 2) NOT NULL,
  `final` float(10, 2) NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `idPlan` int(255) NOT NULL,
  `status` bit(1) NOT NULL,
  `avancePorcentaje` float(10, 2) NOT NULL,
  PRIMARY KEY (`idIndicadores`) USING BTREE,
  INDEX `fk_indicador_plan`(`idPlan`) USING BTREE,
  CONSTRAINT `fk_indicador_plan` FOREIGN KEY (`idPlan`) REFERENCES `plan` (`idMv`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 238 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of indicadores
-- ----------------------------
INSERT INTO `indicadores` VALUES (41, 'Avance Financiero', 2000.00, 4000.00, 2500.00, 6, b'0', 62.50);
INSERT INTO `indicadores` VALUES (42, 'Avance Físico', 1.00, 100.00, 0.00, 6, b'0', 50.00);
INSERT INTO `indicadores` VALUES (43, 'Gestión de Inversión', 1000.00, 3000.00, 0.00, 6, b'0', 0.00);
INSERT INTO `indicadores` VALUES (44, 'Efectividad Financiera', 1.00, 100.00, 0.00, 6, b'0', 0.00);
INSERT INTO `indicadores` VALUES (45, 'Efectividad Administrativa', 1.00, 200.00, 0.00, 6, b'0', 0.00);
INSERT INTO `indicadores` VALUES (46, 'Efectividad Jurídica', 0.00, 1000.00, 0.00, 6, b'0', 0.00);
INSERT INTO `indicadores` VALUES (47, 'Resprogramación Presupuestaria', 3000.00, 34000.00, 0.00, 6, b'0', 0.00);
INSERT INTO `indicadores` VALUES (48, 'Satisfacción Ciudadana', 0.00, 1000.00, 0.00, 6, b'0', 0.00);
INSERT INTO `indicadores` VALUES (49, 'Avance Financiero', 0.00, 100.00, 12.00, 7, b'0', 12.00);
INSERT INTO `indicadores` VALUES (50, 'Avance Físico', 0.00, 100.00, 0.00, 7, b'0', 0.00);
INSERT INTO `indicadores` VALUES (51, 'Gestión de Inversión', 0.00, 100.00, 0.00, 7, b'0', 0.00);
INSERT INTO `indicadores` VALUES (52, 'Efectividad Financiera', 0.00, 100.00, 0.00, 7, b'0', 0.00);
INSERT INTO `indicadores` VALUES (53, 'Efectividad Administrativa', 0.00, 100.00, 0.00, 7, b'0', 0.00);
INSERT INTO `indicadores` VALUES (54, 'Efectividad Jurídica', 0.00, 100.00, 0.00, 7, b'0', 0.00);
INSERT INTO `indicadores` VALUES (55, 'Resprogramación Presupuestaria', 0.00, 100.00, 0.00, 7, b'0', 0.00);
INSERT INTO `indicadores` VALUES (56, 'Satisfacción Ciudadana', 0.00, 100.00, 0.00, 7, b'0', 0.00);
INSERT INTO `indicadores` VALUES (57, 'Avance Financiero', 0.00, 0.00, 0.00, 8, b'0', 0.00);
INSERT INTO `indicadores` VALUES (58, 'Avance Físico', 0.00, 0.00, 0.00, 8, b'0', 0.00);
INSERT INTO `indicadores` VALUES (59, 'Gestión de Inversión', 0.00, 0.00, 0.00, 8, b'0', 0.00);
INSERT INTO `indicadores` VALUES (60, 'Efectividad Financiera', 0.00, 0.00, 0.00, 8, b'0', 0.00);
INSERT INTO `indicadores` VALUES (61, 'Efectividad Administrativa', 0.00, 0.00, 0.00, 8, b'0', 0.00);
INSERT INTO `indicadores` VALUES (62, 'Efectividad Jurídica', 0.00, 0.00, 0.00, 8, b'0', 0.00);
INSERT INTO `indicadores` VALUES (63, 'Resprogramación Presupuestaria', 0.00, 0.00, 0.00, 8, b'0', 0.00);
INSERT INTO `indicadores` VALUES (64, 'Satisfacción Ciudadana', 0.00, 0.00, 0.00, 8, b'0', 0.00);
INSERT INTO `indicadores` VALUES (65, 'Avance Financiero', 0.00, 100.00, 0.00, 9, b'0', 0.00);
INSERT INTO `indicadores` VALUES (66, 'Avance Físico', 0.00, 100.00, 0.00, 9, b'0', 0.00);
INSERT INTO `indicadores` VALUES (67, 'Gestión de Inversión', 0.00, 100.00, 0.00, 9, b'0', 0.00);
INSERT INTO `indicadores` VALUES (68, 'Efectividad Financiera', 0.00, 100.00, 0.00, 9, b'0', 0.00);
INSERT INTO `indicadores` VALUES (69, 'Efectividad Administrativa', 0.00, 100.00, 0.00, 9, b'0', 0.00);
INSERT INTO `indicadores` VALUES (70, 'Efectividad Jurídica', 0.00, 100.00, 0.00, 9, b'0', 0.00);
INSERT INTO `indicadores` VALUES (71, 'Resprogramación Presupuestaria', 0.00, 100.00, 0.00, 9, b'0', 0.00);
INSERT INTO `indicadores` VALUES (72, 'Satisfacción Ciudadana', 0.00, 100.00, 0.00, 9, b'0', 0.00);
INSERT INTO `indicadores` VALUES (73, 'Avance Financiero', 0.00, 100.00, 0.00, 10, b'0', 0.00);
INSERT INTO `indicadores` VALUES (74, 'Avance Físico', 0.00, 100.00, 0.00, 10, b'0', 0.00);
INSERT INTO `indicadores` VALUES (75, 'Gestión de Inversión', 0.00, 100.00, 0.00, 10, b'0', 0.00);
INSERT INTO `indicadores` VALUES (76, 'Efectividad Financiera', 0.00, 100.00, 0.00, 10, b'0', 0.00);
INSERT INTO `indicadores` VALUES (77, 'Efectividad Administrativa', 0.00, 100.00, 0.00, 10, b'0', 0.00);
INSERT INTO `indicadores` VALUES (78, 'Efectividad Jurídica', 0.00, 100.00, 0.00, 10, b'0', 0.00);
INSERT INTO `indicadores` VALUES (79, 'Resprogramación Presupuestaria', 0.00, 100.00, 0.00, 10, b'0', 0.00);
INSERT INTO `indicadores` VALUES (80, 'Satisfacción Ciudadana', 0.00, 100.00, 0.00, 10, b'0', 0.00);
INSERT INTO `indicadores` VALUES (81, 'Avance Financiero', 0.00, 100.00, 0.00, 11, b'0', 0.00);
INSERT INTO `indicadores` VALUES (82, 'Avance Físico', 0.00, 100.00, 0.00, 11, b'0', 0.00);
INSERT INTO `indicadores` VALUES (83, 'Gestión de Inversión', 0.00, 100.00, 0.00, 11, b'0', 0.00);
INSERT INTO `indicadores` VALUES (84, 'Efectividad Financiera', 0.00, 100.00, 0.00, 11, b'0', 0.00);
INSERT INTO `indicadores` VALUES (85, 'Efectividad Administrativa', 0.00, 100.00, 0.00, 11, b'0', 0.00);
INSERT INTO `indicadores` VALUES (86, 'Efectividad Jurídica', 0.00, 100.00, 0.00, 11, b'0', 0.00);
INSERT INTO `indicadores` VALUES (87, 'Resprogramación Presupuestaria', 0.00, 100.00, 0.00, 11, b'0', 0.00);
INSERT INTO `indicadores` VALUES (88, 'Satisfacción Ciudadana', 0.00, 100.00, 0.00, 11, b'0', 0.00);
INSERT INTO `indicadores` VALUES (89, 'Avance Financiero', 0.00, 100.00, 0.00, 12, b'0', 0.00);
INSERT INTO `indicadores` VALUES (90, 'Avance Físico', 0.00, 100.00, 0.00, 12, b'0', 0.00);
INSERT INTO `indicadores` VALUES (91, 'Gestión de Inversión', 0.00, 100.00, 0.00, 12, b'0', 0.00);
INSERT INTO `indicadores` VALUES (92, 'Efectividad Financiera', 0.00, 100.00, 0.00, 12, b'0', 0.00);
INSERT INTO `indicadores` VALUES (93, 'Efectividad Administrativa', 0.00, 100.00, 0.00, 12, b'0', 0.00);
INSERT INTO `indicadores` VALUES (94, 'Efectividad Jurídica', 0.00, 100.00, 0.00, 12, b'0', 0.00);
INSERT INTO `indicadores` VALUES (95, 'Resprogramación Presupuestaria', 0.00, 100.00, 0.00, 12, b'0', 0.00);
INSERT INTO `indicadores` VALUES (96, 'Satisfacción Ciudadana', 0.00, 100.00, 0.00, 12, b'0', 0.00);
INSERT INTO `indicadores` VALUES (97, 'Avance Financiero', 0.00, 100.00, 0.00, 13, b'0', 0.00);
INSERT INTO `indicadores` VALUES (98, 'Avance Físico', 0.00, 100.00, 0.00, 13, b'0', 0.00);
INSERT INTO `indicadores` VALUES (99, 'Gestión de Inversión', 0.00, 100.00, 0.00, 13, b'0', 0.00);
INSERT INTO `indicadores` VALUES (100, 'Efectividad Financiera', 0.00, 100.00, 0.00, 13, b'0', 0.00);
INSERT INTO `indicadores` VALUES (101, 'Efectividad Administrativa', 0.00, 100.00, 0.00, 13, b'0', 0.00);
INSERT INTO `indicadores` VALUES (102, 'Efectividad Jurídica', 0.00, 100.00, 0.00, 13, b'0', 0.00);
INSERT INTO `indicadores` VALUES (103, 'Resprogramación Presupuestaria', 0.00, 100.00, 0.00, 13, b'0', 0.00);
INSERT INTO `indicadores` VALUES (104, 'Satisfacción Ciudadana', 0.00, 100.00, 0.00, 13, b'0', 0.00);
INSERT INTO `indicadores` VALUES (105, 'Avance Financiero', 0.00, 100.00, 0.00, 14, b'0', 0.00);
INSERT INTO `indicadores` VALUES (106, 'Avance Físico', 0.00, 100.00, 0.00, 14, b'0', 0.00);
INSERT INTO `indicadores` VALUES (107, 'Gestión de Inversión', 0.00, 100.00, 0.00, 14, b'0', 0.00);
INSERT INTO `indicadores` VALUES (108, 'Efectividad Financiera', 0.00, 100.00, 0.00, 14, b'0', 0.00);
INSERT INTO `indicadores` VALUES (109, 'Efectividad Administrativa', 0.00, 100.00, 0.00, 14, b'0', 0.00);
INSERT INTO `indicadores` VALUES (110, 'Efectividad Jurídica', 0.00, 100.00, 0.00, 14, b'0', 0.00);
INSERT INTO `indicadores` VALUES (111, 'Resprogramación Presupuestaria', 0.00, 100.00, 0.00, 14, b'0', 0.00);
INSERT INTO `indicadores` VALUES (112, 'Satisfacción Ciudadana', 0.00, 100.00, 0.00, 14, b'0', 0.00);
INSERT INTO `indicadores` VALUES (113, 'Avance Financiero', 0.00, 100.00, 0.00, 15, b'0', 0.00);
INSERT INTO `indicadores` VALUES (114, 'Avance Físico', 0.00, 100.00, 0.00, 15, b'0', 0.00);
INSERT INTO `indicadores` VALUES (115, 'Gestión de Inversión', 0.00, 100.00, 0.00, 15, b'0', 0.00);
INSERT INTO `indicadores` VALUES (116, 'Efectividad Financiera', 0.00, 100.00, 0.00, 15, b'0', 0.00);
INSERT INTO `indicadores` VALUES (117, 'Efectividad Administrativa', 0.00, 100.00, 0.00, 15, b'0', 0.00);
INSERT INTO `indicadores` VALUES (118, 'Efectividad Jurídica', 0.00, 100.00, 0.00, 15, b'0', 0.00);
INSERT INTO `indicadores` VALUES (119, 'Resprogramación Presupuestaria', 0.00, 100.00, 0.00, 15, b'0', 0.00);
INSERT INTO `indicadores` VALUES (120, 'Satisfacción Ciudadana', 0.00, 100.00, 0.00, 15, b'0', 0.00);
INSERT INTO `indicadores` VALUES (121, 'Avance Financiero', 0.00, 100.00, 0.00, 16, b'0', 0.00);
INSERT INTO `indicadores` VALUES (122, 'Avance Físico', 0.00, 100.00, 0.00, 16, b'0', 0.00);
INSERT INTO `indicadores` VALUES (123, 'Gestión de Inversión', 0.00, 100.00, 0.00, 16, b'0', 0.00);
INSERT INTO `indicadores` VALUES (124, 'Efectividad Financiera', 0.00, 100.00, 0.00, 16, b'0', 0.00);
INSERT INTO `indicadores` VALUES (125, 'Efectividad Administrativa', 0.00, 100.00, 0.00, 16, b'0', 0.00);
INSERT INTO `indicadores` VALUES (126, 'Efectividad Jurídica', 0.00, 100.00, 0.00, 16, b'0', 0.00);
INSERT INTO `indicadores` VALUES (127, 'Resprogramación Presupuestaria', 0.00, 100.00, 0.00, 16, b'0', 0.00);
INSERT INTO `indicadores` VALUES (128, 'Satisfacción Ciudadana', 0.00, 100.00, 0.00, 16, b'0', 0.00);
INSERT INTO `indicadores` VALUES (129, 'Avance Financiero', 0.00, 100.00, 0.00, 17, b'0', 0.00);
INSERT INTO `indicadores` VALUES (130, 'Avance Físico', 0.00, 100.00, 0.00, 17, b'0', 0.00);
INSERT INTO `indicadores` VALUES (131, 'Gestión de Inversión', 0.00, 100.00, 0.00, 17, b'0', 0.00);
INSERT INTO `indicadores` VALUES (132, 'Efectividad Financiera', 0.00, 100.00, 0.00, 17, b'0', 0.00);
INSERT INTO `indicadores` VALUES (133, 'Efectividad Administrativa', 0.00, 100.00, 0.00, 17, b'0', 0.00);
INSERT INTO `indicadores` VALUES (134, 'Efectividad Jurídica', 0.00, 100.00, 0.00, 17, b'0', 0.00);
INSERT INTO `indicadores` VALUES (135, 'Resprogramación Presupuestaria', 0.00, 100.00, 0.00, 17, b'0', 0.00);
INSERT INTO `indicadores` VALUES (136, 'Satisfacción Ciudadana', 0.00, 100.00, 0.00, 17, b'0', 0.00);
INSERT INTO `indicadores` VALUES (137, 'Avance Financiero', 0.00, 100.00, 0.00, 18, b'0', 0.00);
INSERT INTO `indicadores` VALUES (138, 'Avance Físico', 0.00, 100.00, 0.00, 18, b'0', 0.00);
INSERT INTO `indicadores` VALUES (139, 'Gestión de Inversión', 0.00, 100.00, 0.00, 18, b'0', 0.00);
INSERT INTO `indicadores` VALUES (140, 'Efectividad Financiera', 0.00, 100.00, 0.00, 18, b'0', 0.00);
INSERT INTO `indicadores` VALUES (141, 'Efectividad Administrativa', 0.00, 100.00, 0.00, 18, b'0', 0.00);
INSERT INTO `indicadores` VALUES (142, 'Efectividad Jurídica', 0.00, 100.00, 0.00, 18, b'0', 0.00);
INSERT INTO `indicadores` VALUES (143, 'Resprogramación Presupuestaria', 0.00, 100.00, 0.00, 18, b'0', 0.00);
INSERT INTO `indicadores` VALUES (144, 'Satisfacción Ciudadana', 0.00, 100.00, 0.00, 18, b'0', 0.00);
INSERT INTO `indicadores` VALUES (145, 'Avance Financiero', 0.00, 100.00, 0.00, 19, b'0', 0.00);
INSERT INTO `indicadores` VALUES (146, 'Avance Físico', 0.00, 100.00, 0.00, 19, b'0', 0.00);
INSERT INTO `indicadores` VALUES (147, 'Gestión de Inversión', 0.00, 100.00, 0.00, 19, b'0', 0.00);
INSERT INTO `indicadores` VALUES (148, 'Efectividad Financiera', 0.00, 100.00, 0.00, 19, b'0', 0.00);
INSERT INTO `indicadores` VALUES (149, 'Efectividad Administrativa', 0.00, 100.00, 0.00, 19, b'0', 0.00);
INSERT INTO `indicadores` VALUES (150, 'Efectividad Jurídica', 0.00, 100.00, 0.00, 19, b'0', 0.00);
INSERT INTO `indicadores` VALUES (151, 'Resprogramación Presupuestaria', 0.00, 100.00, 0.00, 19, b'0', 0.00);
INSERT INTO `indicadores` VALUES (152, 'Satisfacción Ciudadana', 0.00, 100.00, 0.00, 19, b'0', 0.00);
INSERT INTO `indicadores` VALUES (153, 'Avance Financiero', 0.00, 100.00, 0.00, 20, b'0', 0.00);
INSERT INTO `indicadores` VALUES (154, 'Avance Físico', 0.00, 100.00, 0.00, 20, b'0', 0.00);
INSERT INTO `indicadores` VALUES (155, 'Gestión de Inversión', 0.00, 100.00, 0.00, 20, b'0', 0.00);
INSERT INTO `indicadores` VALUES (156, 'Efectividad Financiera', 0.00, 100.00, 0.00, 20, b'0', 0.00);
INSERT INTO `indicadores` VALUES (157, 'Efectividad Administrativa', 0.00, 100.00, 0.00, 20, b'0', 0.00);
INSERT INTO `indicadores` VALUES (158, 'Efectividad Jurídica', 0.00, 100.00, 0.00, 20, b'0', 0.00);
INSERT INTO `indicadores` VALUES (159, 'Resprogramación Presupuestaria', 0.00, 100.00, 0.00, 20, b'0', 0.00);
INSERT INTO `indicadores` VALUES (160, 'Satisfacción Ciudadana', 0.00, 100.00, 0.00, 20, b'0', 0.00);
INSERT INTO `indicadores` VALUES (161, 'Avance Financiero', 0.00, 100.00, 0.00, 21, b'0', 0.00);
INSERT INTO `indicadores` VALUES (162, 'Avance Físico', 0.00, 100.00, 0.00, 21, b'0', 0.00);
INSERT INTO `indicadores` VALUES (163, 'Gestión de Inversión', 0.00, 100.00, 0.00, 21, b'0', 0.00);
INSERT INTO `indicadores` VALUES (164, 'Efectividad Financiera', 0.00, 100.00, 0.00, 21, b'0', 0.00);
INSERT INTO `indicadores` VALUES (165, 'Efectividad Administrativa', 0.00, 100.00, 0.00, 21, b'0', 0.00);
INSERT INTO `indicadores` VALUES (166, 'Efectividad Jurídica', 0.00, 100.00, 0.00, 21, b'0', 0.00);
INSERT INTO `indicadores` VALUES (167, 'Resprogramación Presupuestaria', 0.00, 100.00, 0.00, 21, b'0', 0.00);
INSERT INTO `indicadores` VALUES (168, 'Satisfacción Ciudadana', 0.00, 100.00, 0.00, 21, b'0', 0.00);
INSERT INTO `indicadores` VALUES (169, 'Avance Financiero', 0.00, 100.00, 0.00, 22, b'0', 0.00);
INSERT INTO `indicadores` VALUES (170, 'Avance Físico', 0.00, 100.00, 0.00, 22, b'0', 0.00);
INSERT INTO `indicadores` VALUES (171, 'Gestión de Inversión', 0.00, 100.00, 0.00, 22, b'0', 0.00);
INSERT INTO `indicadores` VALUES (172, 'Efectividad Financiera', 0.00, 100.00, 0.00, 22, b'0', 0.00);
INSERT INTO `indicadores` VALUES (173, 'Efectividad Administrativa', 0.00, 100.00, 0.00, 22, b'0', 0.00);
INSERT INTO `indicadores` VALUES (174, 'Efectividad Jurídica', 0.00, 100.00, 0.00, 22, b'0', 0.00);
INSERT INTO `indicadores` VALUES (175, 'Resprogramación Presupuestaria', 0.00, 100.00, 0.00, 22, b'0', 0.00);
INSERT INTO `indicadores` VALUES (176, 'Satisfacción Ciudadana', 0.00, 100.00, 0.00, 22, b'0', 0.00);
INSERT INTO `indicadores` VALUES (177, 'Avance Financiero', 0.00, 100.00, 0.00, 23, b'0', 0.00);
INSERT INTO `indicadores` VALUES (178, 'Avance Físico', 0.00, 100.00, 0.00, 23, b'0', 0.00);
INSERT INTO `indicadores` VALUES (179, 'Gestión de Inversión', 0.00, 100.00, 0.00, 23, b'0', 0.00);
INSERT INTO `indicadores` VALUES (180, 'Efectividad Financiera', 0.00, 100.00, 0.00, 23, b'0', 0.00);
INSERT INTO `indicadores` VALUES (181, 'Efectividad Administrativa', 0.00, 100.00, 0.00, 23, b'0', 0.00);
INSERT INTO `indicadores` VALUES (182, 'Efectividad Jurídica', 0.00, 100.00, 0.00, 23, b'0', 0.00);
INSERT INTO `indicadores` VALUES (183, 'Resprogramación Presupuestaria', 0.00, 100.00, 0.00, 23, b'0', 0.00);
INSERT INTO `indicadores` VALUES (184, 'Satisfacción Ciudadana', 0.00, 100.00, 0.00, 23, b'0', 0.00);
INSERT INTO `indicadores` VALUES (185, 'Avance Financiero', 0.00, 100.00, 0.00, 24, b'0', 0.00);
INSERT INTO `indicadores` VALUES (186, 'Avance Físico', 0.00, 100.00, 0.00, 24, b'0', 0.00);
INSERT INTO `indicadores` VALUES (187, 'Gestión de Inversión', 0.00, 100.00, 0.00, 24, b'0', 0.00);
INSERT INTO `indicadores` VALUES (188, 'Efectividad Financiera', 0.00, 100.00, 0.00, 24, b'0', 0.00);
INSERT INTO `indicadores` VALUES (189, 'Efectividad Administrativa', 0.00, 100.00, 0.00, 24, b'0', 0.00);
INSERT INTO `indicadores` VALUES (190, 'Efectividad Jurídica', 0.00, 100.00, 0.00, 24, b'0', 0.00);
INSERT INTO `indicadores` VALUES (191, 'Resprogramación Presupuestaria', 0.00, 100.00, 0.00, 24, b'0', 0.00);
INSERT INTO `indicadores` VALUES (192, 'Satisfacción Ciudadana', 0.00, 100.00, 0.00, 24, b'0', 0.00);
INSERT INTO `indicadores` VALUES (193, 'Avance Financiero', 0.00, 100.00, 0.00, 25, b'0', 0.00);
INSERT INTO `indicadores` VALUES (194, 'Avance Físico', 0.00, 100.00, 0.00, 25, b'0', 0.00);
INSERT INTO `indicadores` VALUES (195, 'Gestión de Inversión', 0.00, 100.00, 0.00, 25, b'0', 0.00);
INSERT INTO `indicadores` VALUES (196, 'Efectividad Financiera', 0.00, 100.00, 0.00, 25, b'0', 0.00);
INSERT INTO `indicadores` VALUES (197, 'Efectividad Administrativa', 0.00, 100.00, 0.00, 25, b'0', 0.00);
INSERT INTO `indicadores` VALUES (198, 'Efectividad Jurídica', 0.00, 100.00, 0.00, 25, b'0', 0.00);
INSERT INTO `indicadores` VALUES (199, 'Resprogramación Presupuestaria', 0.00, 100.00, 0.00, 25, b'0', 0.00);
INSERT INTO `indicadores` VALUES (200, 'Satisfacción Ciudadana', 0.00, 100.00, 0.00, 25, b'0', 0.00);
INSERT INTO `indicadores` VALUES (201, 'Avance Financiero', 1.00, 10.00, 15.00, 26, b'0', 150.00);
INSERT INTO `indicadores` VALUES (202, 'Avance Físico', 1.00, 10.00, 11.00, 26, b'0', 110.00);
INSERT INTO `indicadores` VALUES (203, 'Gestión de Inversión', 1.00, 10.00, 5.00, 26, b'0', 50.00);
INSERT INTO `indicadores` VALUES (204, 'Efectividad Financiera', 1.00, 10.00, 2.00, 26, b'0', 20.00);
INSERT INTO `indicadores` VALUES (205, 'Efectividad Administrativa', 1.00, 10.00, 0.00, 26, b'0', 50.00);
INSERT INTO `indicadores` VALUES (206, 'Efectividad Jurídica', 1.00, 10.00, 0.00, 26, b'0', 100.00);
INSERT INTO `indicadores` VALUES (207, 'Resprogramación Presupuestaria', 1.00, 10.00, 2.00, 26, b'0', 20.00);
INSERT INTO `indicadores` VALUES (208, 'Satisfacción Ciudadana', 1.00, 10.00, 6.00, 26, b'0', 60.00);
INSERT INTO `indicadores` VALUES (209, 'Avance Financiero', 0.00, 100.00, 0.00, 27, b'0', 0.00);
INSERT INTO `indicadores` VALUES (210, 'Avance Físico', 0.00, 100.00, 0.00, 27, b'0', 0.00);
INSERT INTO `indicadores` VALUES (211, 'Gestión de Inversión', 0.00, 100.00, 0.00, 27, b'0', 0.00);
INSERT INTO `indicadores` VALUES (212, 'Efectividad Financiera', 0.00, 100.00, 0.00, 27, b'0', 0.00);
INSERT INTO `indicadores` VALUES (213, 'Efectividad Administrativa', 0.00, 100.00, 0.00, 27, b'0', 0.00);
INSERT INTO `indicadores` VALUES (214, 'Efectividad Jurídica', 0.00, 100.00, 0.00, 27, b'0', 0.00);
INSERT INTO `indicadores` VALUES (215, 'Resprogramación Presupuestaria', 0.00, 100.00, 0.00, 27, b'0', 0.00);
INSERT INTO `indicadores` VALUES (216, 'Satisfacción Ciudadana', 0.00, 100.00, 0.00, 27, b'0', 0.00);
INSERT INTO `indicadores` VALUES (217, 'Avance Financiero', 0.00, 1.00, 0.00, 28, b'0', 0.00);
INSERT INTO `indicadores` VALUES (218, 'Avance Físico', 0.00, 1.00, 0.00, 28, b'0', 0.00);
INSERT INTO `indicadores` VALUES (219, 'Gestión de Inversión', 0.00, 1.00, 0.00, 28, b'0', 0.00);
INSERT INTO `indicadores` VALUES (220, 'Efectividad Financiera', 0.00, 1.00, 0.00, 28, b'0', 0.00);
INSERT INTO `indicadores` VALUES (221, 'Efectividad Administrativa', 0.00, 1.00, 0.00, 28, b'0', 0.00);
INSERT INTO `indicadores` VALUES (222, 'Efectividad Jurídica', 0.00, 1.00, 0.00, 28, b'0', 0.00);
INSERT INTO `indicadores` VALUES (223, 'Resprogramación Presupuestaria', 0.00, 1.00, 0.00, 28, b'0', 0.00);
INSERT INTO `indicadores` VALUES (224, 'Satisfacción Ciudadana', 0.00, 1.00, 0.00, 28, b'0', 0.00);
INSERT INTO `indicadores` VALUES (225, 'Avance Financiero', 0.00, 100.00, 60.00, 29, b'0', 60.00);
INSERT INTO `indicadores` VALUES (226, 'Avance Físico', 0.00, 100.00, 0.00, 29, b'0', 0.00);
INSERT INTO `indicadores` VALUES (227, 'Gestión de Inversión', 0.00, 100.00, 0.00, 29, b'0', 0.00);
INSERT INTO `indicadores` VALUES (228, 'Efectividad Financiera', 0.00, 100.00, 0.00, 29, b'0', 0.00);
INSERT INTO `indicadores` VALUES (229, 'Efectividad Administrativa', 0.00, 100.00, 0.00, 29, b'0', 0.00);
INSERT INTO `indicadores` VALUES (230, 'Efectividad Jurídica', 0.00, 100.00, 0.00, 29, b'0', 0.00);
INSERT INTO `indicadores` VALUES (231, 'Resprogramación Presupuestaria', 0.00, 100.00, 0.00, 29, b'0', 0.00);
INSERT INTO `indicadores` VALUES (232, 'Satisfacción Ciudadana', 0.00, 100.00, 100.00, 29, b'0', 100.00);

-- ----------------------------
-- Table structure for indicadores_temp
-- ----------------------------
DROP TABLE IF EXISTS `indicadores_temp`;
CREATE TABLE `indicadores_temp`  (
  `idIndicadores` int(255) NOT NULL,
  `inicio` float(10, 2) NOT NULL,
  `final` float(10, 2) NOT NULL,
  `status` int(1) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for indicadorespdfs
-- ----------------------------
DROP TABLE IF EXISTS `indicadorespdfs`;
CREATE TABLE `indicadorespdfs`  (
  `idPdfs` int(255) NOT NULL AUTO_INCREMENT,
  `idBitacoraIndicador` int(255) NOT NULL,
  `file` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `estatus` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `idIndicador` int(255) NOT NULL,
  PRIMARY KEY (`idPdfs`) USING BTREE,
  INDEX `fk_pdf_bitacora_kr`(`idBitacoraIndicador`) USING BTREE,
  INDEX `fk_pdf_kr`(`idIndicador`) USING BTREE,
  CONSTRAINT `indicadorespdfs_idBitacoraIndicador` FOREIGN KEY (`idBitacoraIndicador`) REFERENCES `bitacoraindicadores` (`idBitacora`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `indicadorespdfs_idIndicador` FOREIGN KEY (`idIndicador`) REFERENCES `indicadores` (`idIndicadores`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of indicadorespdfs
-- ----------------------------
INSERT INTO `indicadorespdfs` VALUES (2, 69, 'Tablero _2019-02-07.pdf', '2', 202);
INSERT INTO `indicadorespdfs` VALUES (3, 80, 'Tablero _2019-02-12.pdf', '0', 207);
INSERT INTO `indicadorespdfs` VALUES (4, 83, 'Tablero _2019-02-12.pdf', '0', 225);
INSERT INTO `indicadorespdfs` VALUES (5, 84, 'Tablero _2019-02-12.pdf', '0', 225);
INSERT INTO `indicadorespdfs` VALUES (6, 87, 'Tablero _2019-02-13.pdf', '0', 231);
INSERT INTO `indicadorespdfs` VALUES (7, 88, 'Tablero _2019-02-13.pdf', '0', 230);

-- ----------------------------
-- Table structure for keyresult
-- ----------------------------
DROP TABLE IF EXISTS `keyresult`;
CREATE TABLE `keyresult`  (
  `idKeyResult` int(255) NOT NULL AUTO_INCREMENT,
  `idObjetivo` int(255) NOT NULL,
  `descripcion` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `metrica` varchar(35) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `orden` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `medicioncomienzo` float(10, 2) NOT NULL,
  `medicionfinal` float(10, 2) NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `estado` bit(1) NOT NULL,
  `avancePorcentaje` float(10, 2) NOT NULL,
  PRIMARY KEY (`idKeyResult`) USING BTREE,
  INDEX `kf_kr_objetivos`(`idObjetivo`) USING BTREE,
  CONSTRAINT `kf_kr_objetivos` FOREIGN KEY (`idObjetivo`) REFERENCES `objetivos` (`idObjetivo`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 123 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of keyresult
-- ----------------------------
INSERT INTO `keyresult` VALUES (70, 58, 'Key result 1', 'Porcentaje', 'Ascendente', 1.00, 100.00, 20.00, b'0', 19.19);
INSERT INTO `keyresult` VALUES (71, 58, 'De pruena 2', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (72, 58, 'Ok', 'Numérico', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (73, 59, 'li', 'Porcentaje', 'Ascendente', 0.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (74, 60, 'Key 1', 'Porcentaje', 'Ascendente', 0.00, 100.00, 30.00, b'0', 30.00);
INSERT INTO `keyresult` VALUES (75, 60, 'Key 2', 'Porcentaje', 'Ascendente', 0.00, 100.00, 325.00, b'1', 325.00);
INSERT INTO `keyresult` VALUES (76, 60, 'Key 3', 'Porcentaje', 'Ascendente', 0.00, 100.00, 1.00, b'0', 1.00);
INSERT INTO `keyresult` VALUES (77, 58, 'Test', 'Porcentaje', 'Ascendente', 1.00, 100.00, 12.00, b'1', 11.11);
INSERT INTO `keyresult` VALUES (78, 61, 'Evaluación diagnóstica a profesores y alumnos', 'Porcentaje', 'Ascendente', 0.00, 100.00, 27.50, b'1', 27.50);
INSERT INTO `keyresult` VALUES (79, 61, 'Cursos, talleres de fortalecimiento, además de la certificación del idioma', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (80, 61, 'Gestionar compras de 2 millones de pesos de material de apoyo educativo para el óptimo funcionamiento del programa', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (81, 61, 'Asesoría, acompañamiento y seguimiento a los maestros participantes del programa', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (82, 63, 'Incrementar en un 10% la matricula en el subsistema Preparatoria Abierta. \n', 'Porcentaje', 'Ascendente', 0.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (83, 63, 'Certificar las competencias docentes del 100% de los maestros de nuevo ingreso de los subsistemas estatales de EMS.', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (84, 63, 'Sostener la participación del 100% de los Subsistemas en el Sistema de Ingreso y oferta educativa a la EMS', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (85, 63, 'Realizar un estudio  en TBC para la creación, absorción o reubicación de planteles.', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (86, 63, 'Incrementar  en un 17% los planteles inscritos en el Padrón de Buena Calidad y mantener los ya inscritos.', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (87, 63, 'Establecer servicios de EMS aprovechando los contraturnos  e infraestructura disponible en las escuelas primarias y secu', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (88, 64, 'Diagnóstico del estatus de infraestructura, conectividad y uso de plataformas en 146 planteles del estado', 'Porcentaje', 'Ascendente', 0.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (89, 64, 'Desarrollo del proyecto ejecutivo\n', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (90, 64, 'Elaborar un proyecto de gestión de recursos\n', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (91, 64, 'Ejecución del Proyecto', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (92, 65, 'Promover la Incorporación de 6 IES Públicas y 2 IEMS   dentro del Modelo de Formación Dual.\n', 'Porcentaje', 'Ascendente', 0.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (93, 65, 'Promover la integración  del número de  empresas necesarias para trabajar de conjuntamente con las IES Públicas   para  ', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (94, 65, 'Promover el desarrollo de convenios necesarios para trabajar conjuntamente con las IES Públicas para  la formación Dual ', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (95, 65, 'Diseñar y operacionalizar la  formalización  del consejo consultivo', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (96, 66, 'Garantizar el capital humano adecuado ', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (97, 66, 'Habilitar las 4 unidades de salud programadas ', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (98, 66, 'Garantizar los medicamentos e insumos necesarios', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (99, 67, 'Comprar $ 124,075,407.21 1er N y  $ $211,279,037 2do N\n(proyección) de insumos en primer y segundo nivel', 'Porcentaje', 'Ascendente', 0.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (100, 67, 'Distribución de insumos adquiridos de primer y segundo nivel', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (101, 67, 'Control de los Insumos en el Almacén Central y Unidades Administrativas de acuerdo a puntos de re-orden y análisis de in', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (102, 68, 'Escrutinio de detección de enfermedad renal (prevención)', 'Porcentaje', 'Ascendente', 0.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (103, 68, 'Gestión del Centro Estatal de Trasplante (tratamiento)', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (104, 68, 'Supervisión de trasplantes de riñones por medio del CETRA: 18 cadavéricos y 126 no cadavéricos en promedio', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (105, 68, 'Diseño e implementación de una política pública de prevención y tratamiento de enfermedades renales', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (106, 69, 'Supervisión técnica, inspección y seguimiento de la implementación del C5 SITEC por parte de INFOTEC', 'Porcentaje', 'Ascendente', 0.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (107, 69, 'Construcción y equipamiento del edificio C5 SITEC', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (108, 69, 'Equipamiento y Sistemas Tecnológicos', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (109, 69, 'Adecuación de espacios e instalación de cámaras y sensores', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (110, 70, 'Regularizar el tramo federal ante SCT', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (111, 70, 'Conseguir la revocación de la concesión actual', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (112, 70, 'Licitar la nueva concesión para el libramiento', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (113, 70, 'Tener el 100% de avance de los derechos de vía del troncal', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (114, 71, 'Elaboración del Plan Maestro y de Ejes Troncales de Movilidad Ciclista en la ZMAGS.\n', 'Porcentaje', 'Ascendente', 0.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (115, 71, 'Elaboración del Proyecto de Ejes de Movilidad Ciclista.', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (116, 71, 'Ejecución de obra de 1 circuito y 4 ejes de movilidad ciclista.', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (117, 72, 'Test A', 'Numero', 'Ascendente', 0.00, 100.00, 100.00, b'0', 100.00);
INSERT INTO `keyresult` VALUES (118, 60, 'Prueba', 'Porcentaje', 'Ascendente', 1.00, 100.00, 100.00, b'1', 100.00);
INSERT INTO `keyresult` VALUES (119, 60, 'Otra', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (120, 73, 'poner camaras', 'Porcentaje', 'Ascendente', 1.00, 100.00, 9.00, b'0', 8.08);
INSERT INTO `keyresult` VALUES (121, 73, 'sensores ', 'Porcentaje', 'Ascendente', 1.00, 100.00, 0.00, b'0', 0.00);
INSERT INTO `keyresult` VALUES (122, 74, 'Key result beta', 'Porcentaje', 'Ascendente', 0.00, 100.00, 50.00, b'1', 50.00);

-- ----------------------------
-- Table structure for krpdfs
-- ----------------------------
DROP TABLE IF EXISTS `krpdfs`;
CREATE TABLE `krpdfs`  (
  `idPdfs` int(255) NOT NULL AUTO_INCREMENT,
  `idBitacoraKr` int(255) NOT NULL,
  `file` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `estatus` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `idKr` int(255) NOT NULL,
  PRIMARY KEY (`idPdfs`) USING BTREE,
  INDEX `fk_pdf_bitacora_kr`(`idBitacoraKr`) USING BTREE,
  INDEX `fk_pdf_kr`(`idKr`) USING BTREE,
  CONSTRAINT `fk_pdf_bitacora_kr` FOREIGN KEY (`idBitacoraKr`) REFERENCES `bitacorakeyresult` (`idBitacora`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_pdf_kr` FOREIGN KEY (`idKr`) REFERENCES `keyresult` (`idKeyResult`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for minuta
-- ----------------------------
DROP TABLE IF EXISTS `minuta`;
CREATE TABLE `minuta`  (
  `idMinuta` int(255) NOT NULL AUTO_INCREMENT,
  `idPlan` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `codigo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lugar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `objetivo` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asunto` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `status` bit(1) NOT NULL,
  `avancePorcentaje` float(10, 2) NOT NULL,
  PRIMARY KEY (`idMinuta`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 116 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of minuta
-- ----------------------------
INSERT INTO `minuta` VALUES (56, 6, '2019-10-01', '05:56:00', '02019-03-01', 'Aguascalientes', 'Objetivo', 'Asunto a tratar', 0.00, b'0', 0.00);
INSERT INTO `minuta` VALUES (57, 6, '2022-12-31', '05:58:00', '02019-03-01', 'Aguascalientes', 'Objetivo', 'Asunto a tratar', 0.00, b'0', 0.00);
INSERT INTO `minuta` VALUES (58, 6, '2022-12-20', '06:01:00', '001/01/2019', 'Aguascalientes', 'Objetivo', 'Asunto a tratar', 0.00, b'0', 0.00);
INSERT INTO `minuta` VALUES (59, 6, '2019-03-01', '06:02:00', '016/01/2019', 'Aguascalientes', 'Objetivo', 'Asunto a tratar', 0.00, b'0', 0.00);
INSERT INTO `minuta` VALUES (64, 7, '2019-06-01', '04:57:00', '02019-01-29', 'Aguascalientes', 'Objetivos', 'Asunto a tratar', 0.00, b'0', 0.00);
INSERT INTO `minuta` VALUES (71, 26, '2019-08-01', '08:59:00', '02019-02-01', 'Test', 'Objetivo', 'Asunto', 0.00, b'0', 0.00);
INSERT INTO `minuta` VALUES (77, 26, '2019-01-01', '01:02:00', '02019-01-01', 'Test', 'Objetivo', 'Asunto', 0.00, b'0', 0.00);
INSERT INTO `minuta` VALUES (81, 8, '2019-01-01', '02:37:00', '02019-01-01', 'test', 'test', 'test', 0.00, b'1', 0.00);
INSERT INTO `minuta` VALUES (100, 9, '2019-01-01', '02:37:00', '02019-01-01', 'test', 'test', 'test', 0.00, b'1', 0.00);
INSERT INTO `minuta` VALUES (101, 0, '2019-01-01', '04:57:00', '', 'Aguascalientes', 'Objetivo 5', 'Asunto a tratar', 0.00, b'1', 0.00);
INSERT INTO `minuta` VALUES (103, 0, '2019-04-01', '05:07:00', '', 'Aguascalientes', 'Objetivo A', 'Asunto a tratar A', 0.00, b'1', 0.00);
INSERT INTO `minuta` VALUES (104, 9, '2019-01-01', '05:08:00', '001/02/2019', 'Puebla', 'Objetivo B', 'Asunto B', 0.00, b'1', 0.00);
INSERT INTO `minuta` VALUES (105, 8, '2019-02-11', '06:01:00', '011/02/2019', 'Puebla', 'Prueba', 'Prueba', 0.00, b'0', 0.00);
INSERT INTO `minuta` VALUES (106, 14, '2019-02-05', '06:02:00', '005/02/2019', 'Puebla', 'q', 'q', 0.00, b'1', 0.00);
INSERT INTO `minuta` VALUES (110, 0, '2019-08-01', '06:55:00', '', 'Puebla', 'Beta acuerdo objetivo', 'Beta asunto a tratar acuerdo', 0.00, b'1', 0.00);
INSERT INTO `minuta` VALUES (111, 0, '2019-10-01', '06:55:00', '', 'Lugar', 'Objetivo Beta Z', 'Asunto', 0.00, b'1', 0.00);
INSERT INTO `minuta` VALUES (112, 29, '2019-06-01', '07:54:00', '02019-06-01', 'Aguascalientes', 'Objetivo 6', 'Asunto a tratar', 0.00, b'1', 0.00);
INSERT INTO `minuta` VALUES (113, 29, '2019-10-15', '07:55:00', '02019-10-15', 'ags', 'xxxxxxxxxx', 'xxxxxxxxx', 0.00, b'1', 0.00);
INSERT INTO `minuta` VALUES (114, 8, '2019-02-18', '09:19:00', '', 'aguascalientes ', 'solucional tal cosa ', 'solucion ', 0.00, b'1', 45.00);
INSERT INTO `minuta` VALUES (115, 29, '2019-06-01', '09:33:00', '02019-06-01', 'ags', 'asasas', 'asasas', 0.00, b'1', 20.00);

-- ----------------------------
-- Table structure for objetivos
-- ----------------------------
DROP TABLE IF EXISTS `objetivos`;
CREATE TABLE `objetivos`  (
  `idObjetivo` int(255) NOT NULL AUTO_INCREMENT,
  `anual` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `idmv` int(255) NOT NULL,
  `objetivo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `descripcion` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `finicio` date NOT NULL,
  `ffinal` date NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `estado` bit(1) NOT NULL,
  `avancePorcentaje` float(10, 2) NOT NULL,
  PRIMARY KEY (`idObjetivo`) USING BTREE,
  INDEX `fk_objetivos_mv`(`idmv`) USING BTREE,
  CONSTRAINT `fk_objetivos_mv` FOREIGN KEY (`idmv`) REFERENCES `plan` (`idMv`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 75 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of objetivos
-- ----------------------------
INSERT INTO `objetivos` VALUES (58, '0', 6, 'Plan de prueba', 'Descripcion de prueba', '2019-01-30', '2019-01-31', 0.00, b'0', 0.00);
INSERT INTO `objetivos` VALUES (59, '0', 6, 'okr anual', 'sas ', '2019-01-01', '2019-12-31', 0.00, b'0', 0.00);
INSERT INTO `objetivos` VALUES (60, '1', 7, 'Objetivo anual Prueba A', 'Prueba', '2019-01-01', '2019-01-31', 91.20, b'0', 91.20);
INSERT INTO `objetivos` VALUES (61, '1', 15, 'servicio formal del programa bilingüe ', 'Implementar el servicio formal del programa bilingüe beneficiando a 17,010 alumnos de educación básica del estado de Aguascalientes.', '2019-01-01', '2019-12-31', 6.88, b'1', 6.88);
INSERT INTO `objetivos` VALUES (62, '1', 16, 'Incrementar en un 10% la absorción en el ciclo esc', 'Incrementar en un 10% la absorción en el ciclo escolar 2019-2020\n', '2019-02-01', '2019-12-31', 0.00, b'0', 0.00);
INSERT INTO `objetivos` VALUES (63, '1', 16, 'Incrementar en un 10% la absorción en el ciclo esc', 'Incrementar en un 10% la absorción en el ciclo escolar 2019-2020\n', '2019-02-01', '2019-12-31', 0.00, b'1', 0.00);
INSERT INTO `objetivos` VALUES (64, '1', 17, 'conectividad  e infraestructura de cómputo a 25 pl', 'Dotar de conectividad e infraestructura de cómputo a 25 planteles educativos del estado\n', '2018-12-01', '2019-12-31', 0.00, b'1', 0.00);
INSERT INTO `objetivos` VALUES (65, '1', 14, 'Modelo de Formación Dual ', '658 Alumnos de Educación Superior ingresan al Modelo de Formación Dual \n', '2019-01-01', '2019-12-31', 0.00, b'1', 0.00);
INSERT INTO `objetivos` VALUES (66, '1', 18, 'Garantizar los servicios de atención 24/7 en 13 ce', 'Garantizar los servicios de atención 24/7 en 13 centros de salud\n', '2019-01-01', '2019-12-31', 0.00, b'1', 0.00);
INSERT INTO `objetivos` VALUES (67, '1', 19, 'Usuarios atendidos y recetados en consulta de prim', 'Garantizar la adquisición y el abastecimiento del 95% de los insumos requeridos  para la atención de los usuarios de los servicios de salud en el estado de Aguascalientes.\n', '2019-01-01', '2019-12-31', 0.00, b'1', 0.00);
INSERT INTO `objetivos` VALUES (68, '1', 20, 'Atención a personas con enfermedad renal crónica e', 'Seguimiento a pacientes con enfermedad renal crónica y aumentar a 44 trasplantes de riñones a los pacientes con enfermedad renal de quinto estudió de población de 18 a 30 años\n', '2019-01-01', '2019-12-31', 0.00, b'1', 0.00);
INSERT INTO `objetivos` VALUES (69, '1', 21, 'Los habitantes de Aguascalientes que generan denun', 'Planeación, gestión, validación y seguimiento de la ejecución del proyecto C5 SITEC durante el año del 2019.\n', '2018-12-01', '2019-08-31', 0.00, b'1', 0.00);
INSERT INTO `objetivos` VALUES (70, '1', 11, 'Realizar la conservación y mantenimiento del Tramo', '\n1,086,392 habitantes de la ZMAGS evitan la totalidad del tránsito pesado y el desvío de tráfico promedio anual de automóviles que van de largo itinerario. \n', '2018-08-01', '2019-11-30', 0.00, b'1', 0.00);
INSERT INTO `objetivos` VALUES (71, '1', 13, 'Construcción de 1 circuito y 4 ejes de movilidad c', 'Habitantes de la ZMAGS incrementan el uso de bicicleta como medio de transporte cotidiano.\n', '2018-01-01', '2019-12-31', 0.00, b'1', 0.00);
INSERT INTO `objetivos` VALUES (72, '0', 26, 'Objetivo no anual', 'Descripción', '2019-02-01', '2019-02-28', 0.00, b'0', 0.00);
INSERT INTO `objetivos` VALUES (73, '0', 21, 'crear algo ', 'crear', '2019-03-01', '2019-05-31', 0.00, b'0', 0.00);
INSERT INTO `objetivos` VALUES (74, '1', 29, 'Objetivo beta', 'Descripción beta', '2019-02-01', '2019-02-28', 50.00, b'1', 50.00);

-- ----------------------------
-- Table structure for objetivos_temp
-- ----------------------------
DROP TABLE IF EXISTS `objetivos_temp`;
CREATE TABLE `objetivos_temp`  (
  `idObjetivo` int(255) NOT NULL AUTO_INCREMENT,
  `idmv` int(255) NOT NULL,
  `objetivo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `descripcion` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `finicio` date NOT NULL,
  `ffinal` date NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `estado` bit(1) NOT NULL,
  PRIMARY KEY (`idObjetivo`) USING BTREE,
  INDEX `fk_objetivos_mv`(`idmv`) USING BTREE,
  CONSTRAINT `objetivos_temp_ibfk_1` FOREIGN KEY (`idmv`) REFERENCES `plan` (`idMv`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for pdfminutas
-- ----------------------------
DROP TABLE IF EXISTS `pdfminutas`;
CREATE TABLE `pdfminutas`  (
  `idPdfMinuta` int(255) NOT NULL AUTO_INCREMENT,
  `pdf` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `idMinuta` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`idPdfMinuta`) USING BTREE,
  INDEX `fk_pdf_minuta`(`idMinuta`) USING BTREE,
  CONSTRAINT `fk_pdf_minuta` FOREIGN KEY (`idMinuta`) REFERENCES `minuta` (`idMinuta`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pdfminutas
-- ----------------------------
INSERT INTO `pdfminutas` VALUES (8, 'Tablero _2019-02-12.pdf', 71);
INSERT INTO `pdfminutas` VALUES (10, 'CUMPLEAÑOS ENERO DE 20192019-02-12.docx', 114);
INSERT INTO `pdfminutas` VALUES (11, 'Matriz Estrate?gica Aguascalientes 181220182019-02-11.pdf', 115);

-- ----------------------------
-- Table structure for plan
-- ----------------------------
DROP TABLE IF EXISTS `plan`;
CREATE TABLE `plan`  (
  `idMv` int(255) NOT NULL AUTO_INCREMENT,
  `mv` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `codigo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lider` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `inversionT` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fin` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `poblacionPotencial` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `proposito` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `poblacionObjetivo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `presupuesto2018` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `presupuesto2019` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `indicadorEstrategico` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `inversion2018` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `inversion2019` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `metaIndicador` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `finicial` date NOT NULL,
  `ffinal` date NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `estado` bit(1) NOT NULL,
  `avanceIndicadores` float(10, 2) NOT NULL,
  PRIMARY KEY (`idMv`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of plan
-- ----------------------------
INSERT INTO `plan` VALUES (6, 'Plan de prueba', NULL, 'Activo', 'Marco Antonio Cardon', '10000', 'Prueba', '90', 'Prueba del sistema', '90', '1000', '2000', '3000', '4000', '5000', '6000', '2019-01-24', '2019-01-31', 0.00, b'0', 14.06);
INSERT INTO `plan` VALUES (7, 'Prueba A', NULL, 'Prueba A', 'Prueba A', '10000', 'Prueba A ', 'Prueba A', 'Prueba A', 'Prueba A', '100', '100', 'Prueba', '100', '100', 'Prueba', '2019-01-01', '2019-01-31', 0.00, b'0', 1.50);
INSERT INTO `plan` VALUES (8, 'Reuniones de Gabinete ', NULL, '0', 'Rocio Gonzalez', '0', 'MINUTAS', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2016-01-01', '2022-12-31', 0.00, b'1', 0.00);
INSERT INTO `plan` VALUES (9, 'SITMA', NULL, 'En proceso de planeación.', 'David Sánchez Padilla', '$2,949,730,327', 'Contribuir al desarrollo urbano de la ZMAGS por medio de la implementación de un Sistema Integrado de Transporte Público moderno, de calidad, eficiente y sustentable', '989,568 usuarios', 'Usuarios del transporte público en la ZMAGS mejoran su movilidad urbana a través de un Sistema Integrado de Transporte Público moderno, de calidad, eficiente y sustentable.', '270,000 usuarios (modelo de negocio)', '53,352,198', '1,180,555,532', 'Porcentaje de rutas que cumplen con el intervalo optimo programado', '66,195,784', '2,236,819,399', '10 rutas de transporte cumplen con el intervalo programado', '2016-06-05', '2022-06-05', 0.00, b'1', 0.00);
INSERT INTO `plan` VALUES (10, 'Corredor Tres centurias', NULL, '100% Plan Maestro40% proyecto ejecutivo 0% ejecución.', 'Ricardo Serrano', '539,140,000', 'Contribuir a mejorar la calidad de vida, propiciando la integración social, mediante la creación de un espacio público digno y seguro donde prevalezca la movilidad ciclo-peatonal, se fortalezca la dinámica turística, el rescate del patrimonio cultural y s', '1,086,392 habitantes de la ZMAGS', 'Visitantes que se apropian y hacen uso del espacio público para incrementar su calidad de vida y generar convivencia social en el ámbito urbano.', '750,000 visitantes', '29,000,000', '130,000,000', 'Por definir', '29,000,000', '156,390,000', 'N/D', '2016-06-05', '2022-06-05', 0.00, b'1', 0.00);
INSERT INTO `plan` VALUES (11, 'Libramiento Poniente', NULL, 'En proceso jurídico de revocación de concesión.', 'José de Jesús Altamira Acosta ', '100,000,000', '1, 086,392 habitantes de la ZMAGS evitan la totalidad del tránsito pesado y el desvío de tráfico promedio anual de automóviles que van de largo itinerario. ', '1,086,392 habitantes en la ZMAGS ', 'Contribuir a consolidar y modernizar los ejes troncales transversales y longitudinales estratégicos y concluir aquellos que se encuentren pendientes', '1,316,032 habitantes en el estado ', '9,106,000', '90,894,000 ', 'Tránsito promedio diario anual', '9,106,000 ', '90,894,000 ', '4% TPDS ', '2016-06-05', '2022-06-05', 0.00, b'1', 0.00);
INSERT INTO `plan` VALUES (12, 'Libramiento Poniente', NULL, 'En proceso jurídico de revocación de concesión.', 'José de Jesús Altamira Acosta ', '100,000,000', '1, 086,392 habitantes de la ZMAGS evitan la totalidad del tránsito pesado y el desvío de tráfico promedio anual de automóviles que van de largo itinerario. ', '1,086,392 habitantes en la ZMAGS ', 'Contribuir a consolidar y modernizar los ejes troncales transversales y longitudinales estratégicos y concluir aquellos que se encuentren pendientes', '1,316,032 habitantes en el estado ', '9,106,000', '90,894,000 ', 'Tránsito promedio diario anual', '9,106,000 ', '90,894,000 ', '4% TPDS ', '2016-06-05', '2022-06-05', 0.00, b'0', 0.00);
INSERT INTO `plan` VALUES (13, 'Ciclovías', NULL, 'Sólo una ciclovía en proceso al 80% de 21 ciclovías programadas.', 'David Sánchez Padilla', '$ 253,300,000', 'Promover y fortalecer la movilidad activa de forma cotidiana y segura, garantizando la calidad y accesibilidad en el ámbito urbano.', '42,262 usuarios frecuentes', 'Habitantes de la ZMAGS incrementan el uso de bicicleta como medio de transporte cotidiano', '63,394 habitantes usuarios', '8,400,000', '90,894,000', 'Incremento anual de la población ciclista en la ZMAGS', '12,000,000', '90,894,000', '6% anual de la población ciclista en la ZMAGS', '2016-06-05', '2022-06-05', 0.00, b'1', 0.00);
INSERT INTO `plan` VALUES (14, 'Educación Dual', NULL, 'Diseño del proyecto al 100%', 'Gustavo Martínez', 'N/D', 'Contribuir al fortalecimiento de un modelo común para que las instituciones de educación superior vinculen a los alumnos al sector productivo.', '25,699 alumnos inscritos al sistema superior OPDS, CECYTEA y CONALEP', 'Alumnos de educación superior y media superior aplicable del estado de Aguascalientes incrementan sus competencias laborales a través del modelo de educación dual para su vinculación productiva en la región.', '3,646 alumnos de cobertura', 'N/D', 'N/D', 'Alumnos que ingresen al Modelo de Educación Dual', 'N/D', 'N/D', '658 Alumnos de Educación Media Superior y Educación Superior', '2016-06-04', '2022-06-05', 0.00, b'1', 0.00);
INSERT INTO `plan` VALUES (15, 'Escuelas Bilingües', NULL, 'Capacitación al personal docente y administrativo.', 'Verónica Orozco Sánchez', 'N/D', 'Facilitar y brindar oportunidades personales, laborales y profesionales a la comunidad educativa de escuelas públicas bilingües.', '80,437 población entre 15 y 19 años en edad de estudiar el nivel medio superior', 'Alumnos de educación básica del estado de Aguascalientes incrementan sus competencias educativas del inglés como lengua extranjera', '80,437 población entre 15 y 19 años en edad de estudiar el nivel medio superior', 'N/D', '$ 10,540,000', 'Porcentaje de alumnos de educación básica del estado de Ags al menos con puntación A1', 'N/D', '$9,540,000', '14,010 alumnos', '2016-06-05', '2022-06-05', 0.00, b'1', 0.00);
INSERT INTO `plan` VALUES (16, 'Prepa para Todos', NULL, 'Se cuenta con el proyecto del programa.', 'Noé García Gómez', 'N/D', 'Contribuir al aseguramiento de una educación media superior suficiente, equitativa y pertinente que incida en la calidad de vida de la población estudiantil del estado de Aguascalientes.', '80,437 población entre 15 y 19 años en edad de estudiar el nivel medio superior', 'Jóvenes entre 15 y 19 años acceden a una educación media superior con permanencia y pertinencia educativa en el estado de Aguascalientes.', '80,437 población entre 15 y 19 años en edad de estudiar el nivel medio superior', 'N/D', '$5,000,000', 'Porcentaje de absorción escolar en el nivel de Educación Media Superior', 'N/D', '$5,000,000', 'Incrementar en un 10% la absorción en el ciclo escolar 2019-2020', '2016-06-05', '2022-06-05', 0.00, b'1', 0.00);
INSERT INTO `plan` VALUES (17, 'Tecnología a Educación Media Superior', NULL, 'En etapa del diseño del sistema.', 'Gustavo Martínez', 'N/D', 'Fortalecer el uso de las tecnologías de la información y comunicación (TIC) en planteles del Nivel Medio Superior.', '48,072 alumnos EMS', 'Alumnos de Educación Media Superior incrementan su acceso, conectividad y uso de TICS en los planteles públicos de bachillerato en el estado de Aguascalientes.', '22,707 alumnos EMS', 'N/D', 'N/D', 'Porcentaje de planteles públicos de bachillerato conectados.', 'N/D', 'N/D', '20% de planteles públicos de bachillerato conectados.', '2016-06-05', '2022-06-05', 0.00, b'1', 0.00);
INSERT INTO `plan` VALUES (18, 'Médico 24 Horas', NULL, 'Reingeniería del personal.', 'Jairo Marentes Betanzos', '$563,550,817.28', 'Fortalecer la atención con calidad y calidez en forma inmediata promoviendo la salud, mediante la prevención y resolución de las necesidades más frecuentes de salud de los usuarios, utilizando métodos y tecnologías simples en la unidad de salud más cercan', '609,270 usuarios de los servicios de salud', 'Usuarios de las áreas de influencia, son atendidos con una mayor capacidad resolutiva en centros de salud 24/7.', '295,718 usuarios de las áreas de influencia', '$ 138,020,952.32', 'N/D', 'Incremento de unidades de salud con atención médica 24/7', '$ 138,020,952', '$ 140,887,704', '50.0%', '2016-06-05', '2022-06-05', 0.00, b'1', 0.00);
INSERT INTO `plan` VALUES (19, 'Abasto de Medicamentos', NULL, 'Licitación pública de abasto de medicamentos. Proyecto al 80%', 'Yosef Guirao', '$444,107,020.22 ', 'Apoyo con la disminución del gasto de bolsillo por compra de medicamento y/o material de curación, por parte de los derechohabientes al Seguro Popular y población no derechohabiente en el 1er. Nivel de Atención.', '609,270 usuarios de los servicios de salud.', 'Usuarios atendidos y recetados en consulta de primer y segundo nivel con subespecialidades, obtienen los medicamentos requeridos para la atención de sus enfermedades en las unidades de salud del estado de Aguascalientes.', '380,000 personas afiliadas actualmente al Seguro Popular y al programa de inclusión social Prospera.', '100,996,301.31', '121,057,208.80', 'Porcentaje de usuarios atendidos en consulta con tratamiento garantizado.', '62,885,304', 'N/D', '85% de usuarios atendidos en consulta con tratamiento garantizado.', '2016-06-05', '2022-06-05', 0.00, b'1', 0.00);
INSERT INTO `plan` VALUES (20, 'Prevención de Enfermedades Renales', NULL, 'Proyecto ejecutivo elaborado.', 'José Manuel Arreola Guerra', '59,240,000', 'Contribuir a la prevención y tratamiento de la enfermedad renal crónico terminal en el estado de Aguascalientes.', '10,000 personas con algún nivel estudió de disfunción renal', 'Personas con enfermedad renal crónica en Aguascalientes', '1,400 enfermos renales en el estado de Aguascalientes', 'N/D', '10,000,000', 'Años de vida ganados promedio', '820,000', '$14,400,000', '1,760 años ganados de vida por el programa de trasplantes de riñón.', '2016-06-05', '2022-06-05', 0.00, b'1', 0.00);
INSERT INTO `plan` VALUES (21, 'C5', NULL, 'Avance de la obra al 40%; equipamiento y suministros al 50%', 'Manuel Díaz Becerra', '$ 812,395,93', 'Contribuir en la coordinación técnica y operativa de los tres niveles de gobierno en materia de seguridad.', '1,316,032 habitantes', 'Los habitantes de Aguascalientes que generan denuncias procedentes através del sistema de emergencia y denuncia anónima son atendidos en un tiempo de 90 segundos.', '243,968 habitantes denunciantes', '$ 155,220,000', '$ 273,052,401', 'Porcentaje de denuncias procedentes atendidas en un tiempo de 90 segundos.', '$ 155,220,000', '$ 273,052,401', '20% de denuncias procedentes atendidas en un tiempo de 90 segundos.', '2016-06-05', '2025-06-05', 0.00, b'1', 0.00);
INSERT INTO `plan` VALUES (22, 'Distrito de Riego 001', NULL, 'Se cuenta con los estudios técnicos y el proyecto ejecutivo.', 'Manuel Alejandro González', '$407,180,000', 'Promover la sustentabilidad del sector agroalimentario a través de apoyos y servicios que permitan incrementar la producción.', '1,960 Productores', 'Productores agrícolas del Distrito de Riego 001 disminuyen su consumo de agua de riego (m3/h).', '1,960 Productores', '14,520,000', '$80,000,000', 'Tasa de crecimiento de consumo de agua de riego', '14,520,000', '$149,480,000', 'En 2018 de 9,000 m3/ha/meta en 2019 de 8,000 m3/ha', '2016-06-05', '2022-06-05', 0.00, b'1', 0.00);
INSERT INTO `plan` VALUES (23, 'Tecnificación de presas', NULL, 'En etapa de planeación e integración del grupo de trabajo.', 'Manuel Alejandro González', '$ 106,076,800', 'Promover la sustentabilidad del sector agroalimentario a través de apoyos y servicios que permitan incrementar la producción.', '1,334 productores agrícolas de 5 presas del estado.', 'Productores agrícolas de 5 presas del estado, disminuyen su consumo de agua de riego (m3/h).', '1,334 productores agrícolas de 5 presas del estado.', '1,000,000', '12,000,000', 'Tasa de crecimiento de consumo de agua de riego', '1,000,000', '16,500,000', 'Ahorro de agua  de 2´778,065 m3', '2016-06-05', '2016-06-05', 0.00, b'1', 0.00);
INSERT INTO `plan` VALUES (24, 'Reusó de Aguas Tratadas', NULL, 'Se cuenta con el proyecto ejecutivo.', 'Alfredo Alonso Ruíz-Esparza', '$ 2\'051,000,000', 'Disminución en el consumo de agua proveniente del acuífero mediante el aprovechamiento de agua tratada.', '1,044,449', 'El Sector Industrial, Agrícola y Servicios utilizan agua tratada en sus procesos.', '320 empresas, 200 usuarios del sector agrícola y servicios.', 'N/D', 'N/D', 'Incremento en el intercambio de agua de primer uso por agua tratada.', 'N/D', '210\'000,000', 'Por definir', '2016-06-05', '2022-06-05', 0.00, b'1', 0.00);
INSERT INTO `plan` VALUES (25, 'Generación de energía sustentable', NULL, 'Se cuenta con los estudios técnicos y de factibilidad del proyecto.', 'Alfredo Alonso Ruíz Esparza', 'N/D', 'Contribuir al ahorro y uso de energías sustentables en el estado de Aguascalientes', '1,313,000 habitantes del estado de Aguascalientes', 'Usuarios de instalaciones de gobierno estatal de servicios municipales reducen su consumo de energía eléctrica por la generación y eficiencia de energía sustentable.', '6,000 servidores públicos.', 'N/D', 'N/D', 'Tasa de crecimiento anual de consumo de energía eléctrica (Gw/h)', 'N/D', 'N/D', 'Por definir', '2016-06-05', '2022-06-05', 0.00, b'1', 0.00);
INSERT INTO `plan` VALUES (26, 'Prueba beta', NULL, 'Prueba beta', 'Prueba beta', 'Prueba beta', 'Prueba beta', 'Prueba beta', 'Prueba beta', 'Prueba beta', '10', '10', '10', '10', '10', '10', '2019-02-01', '2019-02-28', 0.00, b'0', 70.00);
INSERT INTO `plan` VALUES (27, 'Plan Acuerdos', NULL, 'Acuerdos', 'Acuerdos', 'Acuerdos', 'Acuerdos', 'Acuerdos', 'Acuerdos', 'Acuerdos', '1', '10', '1', '10', '10', '10', '2019-03-31', '2019-02-27', 0.00, b'1', 0.00);
INSERT INTO `plan` VALUES (28, 'Nuevo plan', NULL, 'Nuevo plan', 'Nuevo plan', '1', 'Nuevo plan', 'Nuevo plan', 'Nuevo plan', 'Nuevo plan', '1', '1', '1', '1', '1', '1', '2019-02-01', '2019-02-28', 0.00, b'1', 0.00);
INSERT INTO `plan` VALUES (29, 'Beta', NULL, 'Beta', 'Beta', '10000', 'Beta', 'Beta', 'Beta', 'Beta', '1', '1', '1', '1', '1', '1', '2019-02-01', '2019-03-31', 0.00, b'1', 20.00);
INSERT INTO `plan` VALUES (30, 'Mesas de proyectos estrategicos ', NULL, '0', 'Bernardo Lopez ', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0000-00-00', '0000-00-00', 0.00, b'1', 0.00);
INSERT INTO `plan` VALUES (31, 'Mesas de proyectos estrategicos ', NULL, '0', 'Bernardo Lopez ', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0000-00-00', '0000-00-00', 0.00, b'1', 0.00);
INSERT INTO `plan` VALUES (32, 'Mesas de proyectos estrategicos ', NULL, '0', 'Bernardo Lopez ', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0000-00-00', '0000-00-00', 0.00, b'1', 0.00);
INSERT INTO `plan` VALUES (33, 'Mesas de proyectos estrategicos ', NULL, '0', 'Bernardo Lopez ', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0000-00-00', '0000-00-00', 0.00, b'1', 0.00);
INSERT INTO `plan` VALUES (34, 'Mesas de proyectos estrategicos ', NULL, '0', 'Bernardo Lopez ', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0000-00-00', '0000-00-00', 0.00, b'1', 0.00);

-- ----------------------------
-- Table structure for plan_temp
-- ----------------------------
DROP TABLE IF EXISTS `plan_temp`;
CREATE TABLE `plan_temp`  (
  `idMv` int(255) NOT NULL AUTO_INCREMENT,
  `mv` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lider` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `inversionT` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fin` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `poblacionPotencial` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `proposito` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `poblacionObjetivo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `presupuesto2018` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `presupuesto2019` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `indicadorEstrategico` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `inversion2018` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `inversion2019` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `metaIndicador` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `finicial` date NOT NULL,
  `ffinal` date NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `estado` bit(1) NOT NULL,
  PRIMARY KEY (`idMv`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for rol
-- ----------------------------
DROP TABLE IF EXISTS `rol`;
CREATE TABLE `rol`  (
  `idRol` int(255) NOT NULL,
  `rol` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nombrePuesto` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`idRol`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of rol
-- ----------------------------
INSERT INTO `rol` VALUES (1, 'lider', 'Gobernador');
INSERT INTO `rol` VALUES (2, 'sadmin', 'Superadmin');
INSERT INTO `rol` VALUES (3, 'admin', 'Lider');
INSERT INTO `rol` VALUES (4, 'capturista', 'Enlace');

-- ----------------------------
-- Table structure for sesiones
-- ----------------------------
DROP TABLE IF EXISTS `sesiones`;
CREATE TABLE `sesiones`  (
  `idSesiones` int(255) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fecha` datetime NOT NULL,
  `tipo` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`idSesiones`) USING BTREE,
  INDEX `fk_sesiones_usuario`(`usuario`) USING BTREE,
  CONSTRAINT `fk_sesiones_usuario` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 902 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sesiones
-- ----------------------------
INSERT INTO `sesiones` VALUES (434, 'sadmin', '2019-01-24 07:53:00', 'Inicio de Sesion', '187.235.95.157');
INSERT INTO `sesiones` VALUES (435, 'lider', '2019-01-24 08:17:00', 'Inicio de Sesion', '187.235.95.157');
INSERT INTO `sesiones` VALUES (436, 'sadmin', '2019-01-24 08:26:00', 'Inicio de Sesion', '187.235.95.157');
INSERT INTO `sesiones` VALUES (437, 'sadmin', '2019-01-24 09:18:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (438, 'sadmin', '2019-01-24 09:18:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (439, 'sadmin', '2019-01-24 09:35:00', 'Inicio de Sesion', '189.164.190.46');
INSERT INTO `sesiones` VALUES (440, 'sadmin', '2019-01-24 10:17:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (441, 'gob', '2019-01-24 10:45:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (442, 'sadmin', '2019-01-24 10:57:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (443, 'gob', '2019-01-24 11:02:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (444, 'sadmin', '2019-01-24 11:02:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (445, 'sadmin', '2019-01-24 11:02:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (446, 'sadmin', '2019-01-24 11:10:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (447, 'cap1', '2019-01-24 11:12:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (448, 'sadmin', '2019-01-24 11:13:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (449, 'lider', '2019-01-24 11:13:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (450, 'cap1', '2019-01-24 11:17:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (451, 'cap1', '2019-01-24 11:17:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (452, 'cap1', '2019-01-24 11:17:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (453, 'sadmin', '2019-01-24 11:55:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (454, 'sadmin', '2019-01-24 12:04:00', 'Inicio de Sesion', '189.164.190.46');
INSERT INTO `sesiones` VALUES (455, 'sadmin', '2019-01-24 12:09:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (456, 'sadmin', '2019-01-24 12:15:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (457, 'sadmin', '2019-01-24 12:16:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (458, 'sadmin', '2019-01-24 12:30:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (459, 'sadmin', '2019-01-24 12:43:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (460, 'sadmin', '2019-01-24 12:45:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (461, 'sadmin', '2019-01-24 12:57:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (462, 'sadmin', '2019-01-24 13:03:00', 'Inicio de Sesion', '189.164.190.46');
INSERT INTO `sesiones` VALUES (463, 'sadmin', '2019-01-24 13:14:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (464, 'sadmin', '2019-01-24 13:46:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (465, 'sadmin', '2019-01-24 13:46:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (466, 'cap1', '2019-01-24 13:49:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (467, 'sadmin', '2019-01-24 14:02:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (468, 'cap1', '2019-01-24 14:33:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (469, 'gob', '2019-01-24 15:04:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (470, 'sadmin', '2019-01-24 15:05:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (471, 'sadmin', '2019-01-24 15:38:00', 'Inicio de Sesion', '187.237.25.46');
INSERT INTO `sesiones` VALUES (472, 'cap1', '2019-01-24 16:20:00', 'Inicio de Sesion', '187.235.95.157');
INSERT INTO `sesiones` VALUES (473, 'sadmin', '2019-01-24 16:21:00', 'Inicio de Sesion', '189.164.190.46');
INSERT INTO `sesiones` VALUES (474, 'sadmin', '2019-01-24 16:22:00', 'Inicio de Sesion', '187.235.95.157');
INSERT INTO `sesiones` VALUES (475, 'sadmin', '2019-01-24 18:22:00', 'Inicio de Sesion', '189.164.190.46');
INSERT INTO `sesiones` VALUES (476, 'sadmin', '2019-01-24 22:45:00', 'Inicio de Sesion', '187.235.95.157');
INSERT INTO `sesiones` VALUES (477, 'sadmin', '2019-01-25 09:28:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (478, 'sadmin', '2019-01-25 09:31:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (479, 'sadmin', '2019-01-25 09:32:00', 'Inicio de Sesion', '189.164.190.46');
INSERT INTO `sesiones` VALUES (480, 'sadmin', '2019-01-25 09:55:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (481, 'sadmin', '2019-01-25 09:57:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (482, 'sadmin', '2019-01-25 11:12:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (483, 'sadmin', '2019-01-25 11:27:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (484, 'sadmin', '2019-01-25 14:40:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (485, 'sadmin', '2019-01-25 14:50:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (486, 'sadmin', '2019-01-25 16:12:00', 'Inicio de Sesion', '189.164.190.46');
INSERT INTO `sesiones` VALUES (487, 'sadmin', '2019-01-25 17:57:00', 'Inicio de Sesion', '187.235.95.157');
INSERT INTO `sesiones` VALUES (488, 'sadmin', '2019-01-25 18:43:00', 'Inicio de Sesion', '200.68.129.176');
INSERT INTO `sesiones` VALUES (489, 'sadmin', '2019-01-28 09:17:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (490, 'sadmin', '2019-01-28 09:24:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (491, 'sadmin', '2019-01-28 11:21:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (492, 'sadmin', '2019-01-28 11:21:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (493, 'sadmin', '2019-01-28 12:55:00', 'Inicio de Sesion', '189.164.190.46');
INSERT INTO `sesiones` VALUES (494, 'sadmin', '2019-01-28 13:03:00', 'Inicio de Sesion', '189.164.190.46');
INSERT INTO `sesiones` VALUES (495, 'sadmin', '2019-01-28 16:43:00', 'Inicio de Sesion', '189.164.190.46');
INSERT INTO `sesiones` VALUES (496, 'gob', '2019-01-28 16:44:00', 'Inicio de Sesion', '189.164.190.46');
INSERT INTO `sesiones` VALUES (497, 'sadmin', '2019-01-28 16:47:00', 'Inicio de Sesion', '189.164.190.46');
INSERT INTO `sesiones` VALUES (498, 'sadmin', '2019-01-28 18:02:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (499, 'sadmin', '2019-01-28 21:54:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (500, 'sadmin', '2019-01-29 10:41:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (501, 'sadmin', '2019-01-29 11:11:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (502, 'sadmin', '2019-01-29 11:11:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (503, 'sadmin', '2019-01-29 11:11:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (504, 'sadmin', '2019-01-29 11:11:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (505, 'sadmin', '2019-01-29 11:48:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (506, 'sadmin', '2019-01-29 12:17:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (507, 'sadmin', '2019-01-29 12:24:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (508, 'sadmin', '2019-01-29 12:25:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (509, 'sadmin', '2019-01-29 12:25:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (510, 'lider', '2019-01-29 12:25:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (511, 'sadmin', '2019-01-29 12:26:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (512, 'sadmin', '2019-01-29 12:26:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (513, 'cap1', '2019-01-29 12:26:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (514, 'lider', '2019-01-29 12:28:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (515, 'sadmin', '2019-01-29 12:34:00', 'Inicio de Sesion', '187.235.204.101');
INSERT INTO `sesiones` VALUES (516, 'sadmin', '2019-01-29 12:39:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (517, 'sadmin', '2019-01-29 12:39:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (518, 'gob', '2019-01-29 12:40:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (519, 'sadmin', '2019-01-29 12:46:00', 'Inicio de Sesion', '187.235.204.101');
INSERT INTO `sesiones` VALUES (520, 'sadmin', '2019-01-29 12:46:00', 'Inicio de Sesion', '187.235.204.101');
INSERT INTO `sesiones` VALUES (521, 'sadmin', '2019-01-29 12:50:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (522, 'sadmin', '2019-01-29 13:30:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (523, 'sadmin', '2019-01-29 13:30:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (524, 'sadmin', '2019-01-29 14:28:00', 'Inicio de Sesion', '187.235.204.101');
INSERT INTO `sesiones` VALUES (525, 'sadmin', '2019-01-29 14:53:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (526, 'sadmin', '2019-01-29 14:53:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (527, 'sadmin', '2019-01-29 16:40:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (528, 'sadmin', '2019-01-30 09:33:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (529, 'sadmin', '2019-01-30 09:34:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (530, 'sadmin', '2019-01-30 09:34:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (531, 'RocioGonzalez ', '2019-01-30 09:53:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (532, 'sadmin', '2019-01-30 09:55:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (533, 'sadmin', '2019-01-30 09:55:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (534, 'RocioGonzalez ', '2019-01-30 09:56:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (535, 'sadmin', '2019-01-30 09:58:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (536, 'sadmin', '2019-01-30 10:05:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (537, 'sadmin', '2019-01-30 10:05:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (538, 'sadmin', '2019-01-30 10:47:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (539, 'sadmin', '2019-01-30 10:47:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (540, 'sadmin', '2019-01-30 12:05:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (541, 'kevinreyes', '2019-01-30 12:13:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (542, 'sadmin', '2019-01-30 15:19:00', 'Inicio de Sesion', '187.235.204.101');
INSERT INTO `sesiones` VALUES (543, 'FernandaNavarro', '2019-01-30 15:31:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (544, 'sadmin', '2019-01-30 16:38:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (545, 'sadmin', '2019-01-30 16:40:00', 'Inicio de Sesion', '187.235.204.101');
INSERT INTO `sesiones` VALUES (546, 'gob', '2019-01-30 19:03:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (547, 'EderNoda', '2019-01-30 19:04:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (548, 'sadmin', '2019-01-31 15:30:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (549, 'kevinreyes', '2019-01-31 15:32:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (550, 'sadmin', '2019-01-31 17:12:00', 'Inicio de Sesion', '187.235.204.101');
INSERT INTO `sesiones` VALUES (551, 'sadmin', '2019-01-31 17:17:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (552, 'sadmin', '2019-01-31 17:34:00', 'Inicio de Sesion', '187.235.204.101');
INSERT INTO `sesiones` VALUES (553, 'sadmin', '2019-02-01 09:15:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (554, 'sadmin', '2019-02-01 09:36:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (555, 'kevinreyes', '2019-02-01 11:25:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (556, 'RocioGonzalez ', '2019-02-01 11:26:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (557, 'sadmin', '2019-02-01 16:24:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (558, 'sadmin', '2019-02-01 16:26:00', 'Inicio de Sesion', '187.235.204.101');
INSERT INTO `sesiones` VALUES (559, 'sadmin', '2019-02-01 16:30:00', 'Inicio de Sesion', '187.235.204.101');
INSERT INTO `sesiones` VALUES (560, 'sadmin', '2019-02-01 16:50:00', 'Inicio de Sesion', '187.235.204.101');
INSERT INTO `sesiones` VALUES (561, 'EderNoda', '2019-02-05 00:00:00', 'Inicio de Sesion', '187.217.195.144');
INSERT INTO `sesiones` VALUES (562, 'kevinreyes', '2019-02-05 09:34:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (563, 'RocioGonzalez ', '2019-02-05 09:35:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (564, 'sadmin', '2019-02-05 09:57:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (565, 'sadmin', '2019-02-05 11:59:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (566, 'kevinreyes', '2019-02-05 12:25:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (567, 'kevinreyes', '2019-02-05 12:40:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (568, 'JuanBarcenas ', '2019-02-05 13:59:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (569, 'sadmin', '2019-02-05 17:28:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (570, 'kevinreyes', '2019-02-05 18:36:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (571, 'sadmin', '2019-02-06 12:18:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (572, 'kevinreyes', '2019-02-06 14:13:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (573, 'sadmin', '2019-02-06 16:49:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (574, 'sadmin', '2019-02-06 16:49:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (575, 'sadmin', '2019-02-06 16:49:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (576, 'kevinreyes', '2019-02-06 17:09:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (577, 'sadmin', '2019-02-07 09:37:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (578, 'sadmin', '2019-02-07 09:38:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (579, 'sadmin', '2019-02-07 09:38:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (580, 'sadmin', '2019-02-07 09:38:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (581, 'kevinreyes', '2019-02-07 10:03:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (582, 'RocioGonzalez ', '2019-02-07 10:05:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (583, 'sadmin', '2019-02-07 13:06:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (584, 'sadmin', '2019-02-07 13:06:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (585, 'kevinreyes', '2019-02-07 15:10:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (586, 'sadmin', '2019-02-07 17:19:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (587, 'gob', '2019-02-07 17:45:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (588, 'sadmin', '2019-02-07 17:46:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (589, 'gob', '2019-02-07 17:49:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (590, 'sadmin', '2019-02-07 17:51:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (591, 'cap1', '2019-02-07 18:05:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (592, 'sadmin', '2019-02-07 18:07:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (593, 'cap1', '2019-02-07 18:11:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (594, 'sadmin', '2019-02-07 18:12:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (595, 'lider1', '2019-02-07 18:19:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (596, 'cap1', '2019-02-07 18:19:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (597, 'lider1', '2019-02-07 18:20:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (598, 'cap1', '2019-02-07 18:28:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (599, 'lider1', '2019-02-07 18:32:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (600, 'sadmin', '2019-02-07 18:33:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (601, 'sadmin', '2019-02-07 18:47:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (602, 'sadmin', '2019-02-07 22:13:00', 'Inicio de Sesion', '187.190.165.254');
INSERT INTO `sesiones` VALUES (603, 'sadmin', '2019-02-08 11:08:00', 'Inicio de Sesion', '187.146.109.172');
INSERT INTO `sesiones` VALUES (604, 'lider1', '2019-02-08 11:58:00', 'Inicio de Sesion', '187.146.109.172');
INSERT INTO `sesiones` VALUES (605, 'lider1', '2019-02-08 11:58:00', 'Inicio de Sesion', '187.146.109.172');
INSERT INTO `sesiones` VALUES (606, 'sadmin', '2019-02-08 12:21:00', 'Inicio de Sesion', '187.146.109.172');
INSERT INTO `sesiones` VALUES (607, 'sadmin', '2019-02-08 12:51:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (608, 'sadmin', '2019-02-08 15:42:00', 'Inicio de Sesion', '187.146.109.172');
INSERT INTO `sesiones` VALUES (609, 'EderNoda', '2019-02-08 17:08:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (610, 'kevinreyes', '2019-02-08 17:09:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (611, 'cap1', '2019-02-08 17:10:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (612, 'cap1', '2019-02-08 17:12:00', 'Inicio de Sesion', '187.146.109.172');
INSERT INTO `sesiones` VALUES (613, 'sadmin', '2019-02-08 17:13:00', 'Inicio de Sesion', '187.146.109.172');
INSERT INTO `sesiones` VALUES (614, 'EderNoda', '2019-02-08 17:18:00', 'Inicio de Sesion', '187.146.109.172');
INSERT INTO `sesiones` VALUES (615, 'sadmin', '2019-02-09 09:30:00', 'Inicio de Sesion', '187.235.204.101');
INSERT INTO `sesiones` VALUES (616, 'sadmin', '2019-02-09 10:34:00', 'Inicio de Sesion', '187.146.109.172');
INSERT INTO `sesiones` VALUES (617, 'cap1', '2019-02-09 11:43:00', 'Inicio de Sesion', '187.146.109.172');
INSERT INTO `sesiones` VALUES (618, 'cap1', '2019-02-09 14:54:00', 'Inicio de Sesion', '187.146.109.172');
INSERT INTO `sesiones` VALUES (619, 'gob', '2019-02-10 08:29:00', 'Inicio de Sesion', '189.168.121.59');
INSERT INTO `sesiones` VALUES (620, 'gob', '2019-02-10 09:26:00', 'Inicio de Sesion', '201.175.202.233');
INSERT INTO `sesiones` VALUES (621, 'gob', '2019-02-10 09:27:00', 'Inicio de Sesion', '201.175.202.233');
INSERT INTO `sesiones` VALUES (622, 'gob', '2019-02-10 09:27:00', 'Inicio de Sesion', '201.175.202.233');
INSERT INTO `sesiones` VALUES (623, 'sadmin', '2019-02-10 21:48:00', 'Inicio de Sesion', '201.130.5.125');
INSERT INTO `sesiones` VALUES (624, 'cap1', '2019-02-10 23:22:00', 'Inicio de Sesion', '201.130.5.125');
INSERT INTO `sesiones` VALUES (625, 'sadmin', '2019-02-10 23:39:00', 'Inicio de Sesion', '201.130.5.125');
INSERT INTO `sesiones` VALUES (626, 'cap1', '2019-02-10 23:52:00', 'Inicio de Sesion', '201.130.5.125');
INSERT INTO `sesiones` VALUES (627, 'gob', '2019-02-10 23:54:00', 'Inicio de Sesion', '201.130.5.125');
INSERT INTO `sesiones` VALUES (628, 'kevinreyes', '2019-02-11 00:03:00', 'Inicio de Sesion', '177.249.175.192');
INSERT INTO `sesiones` VALUES (629, 'sadmin', '2019-02-11 09:17:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (630, 'gob', '2019-02-11 09:17:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (631, 'kevinreyes', '2019-02-11 09:26:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (632, 'RocioGonzalez ', '2019-02-11 09:27:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (633, 'kevinreyes', '2019-02-11 09:28:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (634, 'cap1', '2019-02-11 09:29:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (635, 'sadmin', '2019-02-11 10:17:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (636, 'kevinreyes', '2019-02-11 11:22:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (637, 'RocioGonzalez ', '2019-02-11 11:22:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (638, 'RocioGonzalez ', '2019-02-11 11:39:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (639, 'RocioGonzalez ', '2019-02-11 11:40:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (640, 'sadmin', '2019-02-11 13:39:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (641, 'kevinreyes', '2019-02-11 16:02:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (642, 'gob', '2019-02-11 17:49:00', 'Inicio de Sesion', '187.176.149.183');
INSERT INTO `sesiones` VALUES (643, 'gob', '2019-02-11 23:09:00', 'Inicio de Sesion', '200.68.128.44');
INSERT INTO `sesiones` VALUES (644, 'gob', '2019-02-11 23:10:00', 'Inicio de Sesion', '200.68.128.44');
INSERT INTO `sesiones` VALUES (645, 'gob', '2019-02-12 03:50:00', 'Inicio de Sesion', '189.168.121.59');
INSERT INTO `sesiones` VALUES (646, 'sadmin', '2019-02-12 05:09:00', 'Inicio de Sesion', '187.190.165.254');
INSERT INTO `sesiones` VALUES (647, 'gob', '2019-02-12 05:11:00', 'Inicio de Sesion', '187.190.165.254');
INSERT INTO `sesiones` VALUES (648, 'sadmin', '2019-02-12 05:25:00', 'Inicio de Sesion', '187.190.165.254');
INSERT INTO `sesiones` VALUES (649, 'gob', '2019-02-12 05:44:00', 'Inicio de Sesion', '187.190.165.254');
INSERT INTO `sesiones` VALUES (650, 'gob', '2019-02-12 05:56:00', 'Inicio de Sesion', '187.190.165.254');
INSERT INTO `sesiones` VALUES (651, 'gob', '2019-02-12 05:57:00', 'Inicio de Sesion', '187.190.165.254');
INSERT INTO `sesiones` VALUES (652, 'gob', '2019-02-12 06:40:00', 'Inicio de Sesion', '187.190.165.254');
INSERT INTO `sesiones` VALUES (653, 'gob', '2019-02-12 06:40:00', 'Inicio de Sesion', '187.190.165.254');
INSERT INTO `sesiones` VALUES (654, 'gob', '2019-02-12 07:25:00', 'Inicio de Sesion', '187.190.165.254');
INSERT INTO `sesiones` VALUES (655, 'gob', '2019-02-12 07:26:00', 'Inicio de Sesion', '187.190.165.254');
INSERT INTO `sesiones` VALUES (656, 'gob', '2019-02-12 07:26:00', 'Inicio de Sesion', '187.190.165.254');
INSERT INTO `sesiones` VALUES (657, 'gob', '2019-02-12 07:27:00', 'Inicio de Sesion', '187.190.165.254');
INSERT INTO `sesiones` VALUES (658, 'gob', '2019-02-12 07:27:00', 'Inicio de Sesion', '187.190.165.254');
INSERT INTO `sesiones` VALUES (659, 'gob', '2019-02-12 07:29:00', 'Inicio de Sesion', '187.190.165.254');
INSERT INTO `sesiones` VALUES (660, 'gob', '2019-02-12 07:30:00', 'Inicio de Sesion', '187.190.165.254');
INSERT INTO `sesiones` VALUES (661, 'gob', '2019-02-12 08:26:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (662, 'sadmin', '2019-02-12 08:34:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (663, 'gob', '2019-02-12 08:35:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (664, 'sadmin', '2019-02-12 08:36:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (665, 'lider1', '2019-02-12 08:48:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (666, 'sadmin', '2019-02-12 08:58:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (667, 'acuerdos', '2019-02-12 08:59:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (668, 'gob', '2019-02-12 09:11:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (669, 'gob', '2019-02-12 09:12:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (670, 'acuerdos', '2019-02-12 09:18:00', 'Inicio de Sesion', '189.164.48.50');
INSERT INTO `sesiones` VALUES (671, 'sadmin', '2019-02-12 09:29:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (672, 'kevinreyes', '2019-02-12 09:29:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (673, 'RocioGonzalez ', '2019-02-12 09:30:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (674, 'gob', '2019-02-12 09:38:00', 'Inicio de Sesion', '200.68.143.205');
INSERT INTO `sesiones` VALUES (675, 'gob', '2019-02-12 09:38:00', 'Inicio de Sesion', '200.68.143.205');
INSERT INTO `sesiones` VALUES (676, 'kevinreyes', '2019-02-12 09:39:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (677, 'sadmin', '2019-02-12 09:40:00', 'Inicio de Sesion', '189.164.176.55');
INSERT INTO `sesiones` VALUES (678, 'sadmin', '2019-02-12 09:40:00', 'Inicio de Sesion', '189.164.176.55');
INSERT INTO `sesiones` VALUES (679, 'sadmin', '2019-02-12 09:40:00', 'Inicio de Sesion', '189.164.176.55');
INSERT INTO `sesiones` VALUES (680, 'sadmin', '2019-02-12 09:48:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (681, 'kevinreyes', '2019-02-12 09:51:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (682, 'acuerdos', '2019-02-12 09:53:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (683, 'gob', '2019-02-12 09:54:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (684, 'gob', '2019-02-12 09:58:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (685, 'gob', '2019-02-12 09:59:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (686, 'sadmin', '2019-02-12 09:59:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (687, 'sadmin', '2019-02-12 10:03:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (688, 'lider', '2019-02-12 10:05:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (689, 'sadmin', '2019-02-12 10:11:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (690, 'gob', '2019-02-12 10:16:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (691, 'gob', '2019-02-12 10:17:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (692, 'kevinreyes', '2019-02-12 10:20:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (693, 'EderNoda', '2019-02-12 10:29:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (694, 'sadmin', '2019-02-12 10:34:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (695, 'gob', '2019-02-12 10:36:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (696, 'EderNoda', '2019-02-12 10:37:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (697, 'gob', '2019-02-12 10:38:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (698, 'gob', '2019-02-12 10:39:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (699, 'sadmin', '2019-02-12 10:40:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (700, 'kevinreyes', '2019-02-12 10:40:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (701, 'EderNoda', '2019-02-12 10:41:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (702, 'gob', '2019-02-12 10:44:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (703, 'gob', '2019-02-12 10:58:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (704, 'kevinreyes', '2019-02-12 10:58:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (705, 'cap1', '2019-02-12 11:01:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (706, 'sadmin', '2019-02-12 11:01:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (707, 'cap1', '2019-02-12 11:01:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (708, 'sadmin', '2019-02-12 11:03:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (709, 'sadmin', '2019-02-12 11:03:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (710, 'cap1', '2019-02-12 11:38:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (711, 'sadmin', '2019-02-12 11:39:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (712, 'cap1', '2019-02-12 11:39:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (713, 'sadmin', '2019-02-12 11:41:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (714, 'kevinreyes', '2019-02-12 11:45:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (715, 'gob', '2019-02-12 11:55:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (716, 'sadmin', '2019-02-12 11:56:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (717, 'kevinreyes', '2019-02-12 12:12:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (718, 'gob', '2019-02-12 12:18:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (719, 'cap1', '2019-02-12 12:28:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (720, 'EderNoda', '2019-02-12 12:29:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (721, 'gob', '2019-02-12 12:37:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (722, 'EderNoda', '2019-02-12 12:38:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (723, 'gob', '2019-02-12 12:38:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (724, 'sadmin', '2019-02-12 12:40:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (725, 'sadmin', '2019-02-12 12:41:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (726, 'cap1', '2019-02-12 12:42:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (727, 'EderNoda', '2019-02-12 12:42:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (728, 'EderNoda', '2019-02-12 12:46:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (729, 'kevinreyes', '2019-02-12 12:54:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (730, 'Jorge.Uria', '2019-02-12 12:58:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (731, 'kevinreyes', '2019-02-12 12:59:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (732, 'Jorge.Uria', '2019-02-12 13:00:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (733, 'Jorge.Uria', '2019-02-12 13:06:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (734, 'Jorge.Uria', '2019-02-12 13:06:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (735, 'gob', '2019-02-12 13:09:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (736, 'Jorge.Uria', '2019-02-12 13:09:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (737, 'RocioGonzalez ', '2019-02-12 13:10:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (738, 'lider', '2019-02-12 13:12:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (739, 'EderNoda', '2019-02-12 13:12:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (740, 'gob', '2019-02-12 13:13:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (741, 'RocioGonzalez ', '2019-02-12 13:14:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (742, 'sadmin', '2019-02-12 13:15:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (743, 'RocioGonzalez ', '2019-02-12 13:19:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (744, 'kevinreyes', '2019-02-12 13:20:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (745, 'kevinreyes', '2019-02-12 13:20:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (746, 'Jorge.Uria', '2019-02-12 13:20:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (747, 'kevinreyes', '2019-02-12 13:25:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (748, 'sadmin', '2019-02-12 13:45:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (749, 'sadmin', '2019-02-12 13:48:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (750, 'acuerdos', '2019-02-12 13:49:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (751, 'sadmin', '2019-02-12 13:49:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (752, 'acuerdos', '2019-02-12 13:51:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (753, 'EderNoda', '2019-02-12 14:04:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (754, 'lider001', '2019-02-12 14:08:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (755, 'sadmin', '2019-02-12 14:09:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (756, 'lider001', '2019-02-12 14:09:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (757, 'acuerdos', '2019-02-12 14:10:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (758, 'EderNoda', '2019-02-12 14:11:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (759, 'EderNoda', '2019-02-12 14:26:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (760, 'EderNoda', '2019-02-12 14:32:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (761, 'EderNoda', '2019-02-12 14:33:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (762, 'gob', '2019-02-12 14:54:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (763, 'gob', '2019-02-12 14:55:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (764, 'gob', '2019-02-12 14:58:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (765, 'kevinreyes', '2019-02-12 14:58:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (766, 'gob', '2019-02-12 14:59:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (767, 'gob', '2019-02-12 14:59:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (768, 'David Sánchez P', '2019-02-12 14:59:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (769, 'gob', '2019-02-12 15:02:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (770, 'gob', '2019-02-12 15:02:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (771, 'sadmin', '2019-02-12 15:05:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (772, 'kevinreyes', '2019-02-12 15:10:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (773, 'sadmin', '2019-02-12 15:12:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (774, 'David Sánchez P', '2019-02-12 15:13:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (775, 'sadmin', '2019-02-12 15:14:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (776, 'David Sánchez P', '2019-02-12 15:14:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (777, 'David Sánchez P', '2019-02-12 15:28:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (778, 'gob', '2019-02-12 15:36:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (779, 'gob', '2019-02-12 16:47:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (780, 'gob', '2019-02-12 16:48:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (781, 'sadmin', '2019-02-12 16:57:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (782, 'sadmin', '2019-02-12 16:58:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (783, 'sadmin', '2019-02-12 17:02:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (784, 'gob', '2019-02-12 17:36:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (785, 'enlace-beta', '2019-02-12 18:14:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (786, 'enlace-beta', '2019-02-12 18:17:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (787, 'sadmin', '2019-02-12 18:28:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (788, 'enlace-beta', '2019-02-12 18:30:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (789, 'sadmin', '2019-02-12 18:30:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (790, 'enlace-beta', '2019-02-12 18:31:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (791, 'enlace-beta', '2019-02-12 18:33:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (792, 'enlace-beta', '2019-02-12 18:49:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (793, 'sadmin', '2019-02-12 18:51:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (794, 'lider-beta', '2019-02-12 18:52:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (795, 'EderNoda', '2019-02-12 18:55:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (796, 'sadmin', '2019-02-12 19:54:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (797, 'enlace-beta', '2019-02-12 19:56:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (798, 'sadmin', '2019-02-12 19:56:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (799, 'EderNoda', '2019-02-12 19:59:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (800, 'gob', '2019-02-12 20:01:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (801, 'gob', '2019-02-12 20:01:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (802, 'gob', '2019-02-12 20:02:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (803, 'sadmin', '2019-02-13 09:03:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (804, 'kevinreyes', '2019-02-13 09:04:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (805, 'gob', '2019-02-13 09:07:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (806, 'gob', '2019-02-13 09:07:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (807, 'RocioGonzalez ', '2019-02-13 09:08:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (808, 'kevinreyes', '2019-02-13 09:08:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (809, 'RocioGonzalez ', '2019-02-13 09:09:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (810, 'kevinreyes', '2019-02-13 09:09:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (811, 'RocioGonzalez ', '2019-02-13 09:10:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (812, 'sadmin', '2019-02-13 09:13:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (813, 'sadmin', '2019-02-13 09:15:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (814, 'acuerdos', '2019-02-13 09:18:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (815, 'sadmin', '2019-02-13 09:19:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (816, 'sadmin', '2019-02-13 09:19:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (817, 'sadmin', '2019-02-13 09:20:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (818, 'sadmin', '2019-02-13 09:21:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (819, 'sadmin', '2019-02-13 09:31:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (820, 'acuerdos', '2019-02-13 09:32:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (821, 'acuerdos', '2019-02-13 09:32:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (822, 'sadmin', '2019-02-13 09:38:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (823, 'kevinreyes', '2019-02-13 09:48:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (824, 'FernandaNavarro', '2019-02-13 09:49:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (825, 'kevinreyes', '2019-02-13 10:04:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (826, 'RocioGonzalez ', '2019-02-13 10:28:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (827, 'gob', '2019-02-13 11:05:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (828, 'EderNoda', '2019-02-13 11:09:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (829, 'gob', '2019-02-13 11:11:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (830, 'cap1', '2019-02-13 11:12:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (831, 'gob', '2019-02-13 11:12:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (832, 'gob', '2019-02-13 11:15:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (833, 'gob', '2019-02-13 11:15:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (834, 'kevinreyes', '2019-02-13 11:16:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (835, 'sadmin', '2019-02-13 11:16:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (836, 'sadmin', '2019-02-13 11:16:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (837, 'enlace-beta', '2019-02-13 11:17:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (838, 'EderNoda', '2019-02-13 11:18:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (839, 'cap1', '2019-02-13 11:19:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (840, 'cap1', '2019-02-13 11:19:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (841, 'sadmin', '2019-02-13 11:21:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (842, 'gob', '2019-02-13 11:22:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (843, 'RocioGonzalez ', '2019-02-13 11:26:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (844, 'gob', '2019-02-13 11:30:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (845, 'gob', '2019-02-13 11:31:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (846, 'gob', '2019-02-13 11:32:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (847, 'gob', '2019-02-13 11:34:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (848, 'sadmin', '2019-02-13 11:35:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (849, 'gob', '2019-02-13 11:35:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (850, 'gob', '2019-02-13 11:35:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (851, 'gob', '2019-02-13 11:37:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (852, 'gob', '2019-02-13 11:39:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (853, 'gob', '2019-02-13 11:40:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (854, 'gob', '2019-02-13 11:40:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (855, 'gob', '2019-02-13 11:41:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (856, 'gob', '2019-02-13 11:42:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (857, 'gob', '2019-02-13 11:43:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (858, 'gob', '2019-02-13 11:45:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (859, 'gob', '2019-02-13 11:45:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (860, 'gob', '2019-02-13 11:46:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (861, 'gob', '2019-02-13 11:46:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (862, 'gob', '2019-02-13 11:46:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (863, 'gob', '2019-02-13 11:47:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (864, 'gob', '2019-02-13 11:48:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (865, 'gob', '2019-02-13 11:51:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (866, 'gob', '2019-02-13 11:52:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (867, 'kevinreyes', '2019-02-13 12:07:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (868, 'kevinreyes', '2019-02-13 12:07:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (869, 'kevinreyes', '2019-02-13 12:07:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (870, 'kevinreyes', '2019-02-13 12:07:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (871, 'kevinreyes', '2019-02-13 12:07:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (872, 'kevinreyes', '2019-02-13 12:07:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (873, 'kevinreyes', '2019-02-13 12:07:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (874, 'kevinreyes', '2019-02-13 12:07:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (875, 'kevinreyes', '2019-02-13 12:07:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (876, 'kevinreyes', '2019-02-13 12:07:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (877, 'kevinreyes', '2019-02-13 12:07:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (878, 'gob', '2019-02-13 12:20:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (879, 'gob', '2019-02-13 12:21:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (880, 'gob', '2019-02-13 12:38:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (881, 'gob', '2019-02-13 12:40:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (882, 'cap1', '2019-02-13 12:52:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (883, 'cap1', '2019-02-13 13:56:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (884, 'sadmin', '2019-02-13 13:59:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (885, 'gob', '2019-02-13 14:18:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (886, 'gob', '2019-02-13 15:03:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (887, 'gob', '2019-02-13 16:12:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (888, 'kevinreyes', '2019-02-13 17:20:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (889, 'cap1', '2019-02-13 17:22:00', 'Inicio de Sesion', '177.234.22.220');
INSERT INTO `sesiones` VALUES (890, 'gob', '2019-02-13 17:23:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (891, 'gob', '2019-02-13 17:43:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (892, 'gob', '2019-02-13 17:44:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (893, 'gob', '2019-02-13 17:44:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (894, 'gob', '2019-02-13 17:45:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (895, 'sadmin', '2019-02-13 18:03:00', 'Inicio de Sesion', '189.164.53.192');
INSERT INTO `sesiones` VALUES (896, 'sadmin', '2019-02-13 18:53:00', 'Inicio de Sesion', '187.187.227.141');
INSERT INTO `sesiones` VALUES (897, 'sadmin', '2019-02-13 19:06:00', 'Inicio de Sesion', '187.187.227.141');
INSERT INTO `sesiones` VALUES (898, 'sadmin', '2019-02-13 20:01:00', 'Inicio de Sesion', '187.187.227.141');
INSERT INTO `sesiones` VALUES (899, 'sadmin', '2019-02-13 20:01:00', 'Inicio de Sesion', '187.187.227.141');
INSERT INTO `sesiones` VALUES (900, 'kevinreyes', '2019-02-14 09:09:00', 'Inicio de Sesion', '201.167.124.196');
INSERT INTO `sesiones` VALUES (901, 'kevinreyes', '2019-02-14 09:29:00', 'Inicio de Sesion', '201.167.124.196');

-- ----------------------------
-- Table structure for usuariokeyresult
-- ----------------------------
DROP TABLE IF EXISTS `usuariokeyresult`;
CREATE TABLE `usuariokeyresult`  (
  `idRelacion` int(255) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `kr` int(255) NOT NULL,
  PRIMARY KEY (`idRelacion`) USING BTREE,
  INDEX `fk_registro_user`(`usuario`) USING BTREE,
  INDEX `fk_registro_kr`(`kr`) USING BTREE,
  CONSTRAINT `fk_registro_kr` FOREIGN KEY (`kr`) REFERENCES `keyresult` (`idKeyResult`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_registro_user` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 59 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of usuariokeyresult
-- ----------------------------
INSERT INTO `usuariokeyresult` VALUES (54, 'cap1', 110);
INSERT INTO `usuariokeyresult` VALUES (55, 'cap1', 114);
INSERT INTO `usuariokeyresult` VALUES (56, 'cap1', 92);
INSERT INTO `usuariokeyresult` VALUES (58, 'enlace-beta', 122);

-- ----------------------------
-- Table structure for usuarioplanes
-- ----------------------------
DROP TABLE IF EXISTS `usuarioplanes`;
CREATE TABLE `usuarioplanes`  (
  `idRelacion` int(255) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `plan` int(255) NOT NULL,
  PRIMARY KEY (`idRelacion`) USING BTREE,
  INDEX `fk_registro_user`(`usuario`) USING BTREE,
  INDEX `fk_registro_kr`(`plan`) USING BTREE,
  CONSTRAINT `usuarioplanes_ibfk_1` FOREIGN KEY (`plan`) REFERENCES `plan` (`idMv`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `usuarioplanes_ibfk_2` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 71 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of usuarioplanes
-- ----------------------------
INSERT INTO `usuarioplanes` VALUES (50, 'RocioGonzalez ', 8);
INSERT INTO `usuarioplanes` VALUES (51, 'FernandaNavarro', 8);
INSERT INTO `usuarioplanes` VALUES (53, 'lider1', 26);
INSERT INTO `usuarioplanes` VALUES (54, 'lidera', 26);
INSERT INTO `usuarioplanes` VALUES (57, 'cap1', 26);
INSERT INTO `usuarioplanes` VALUES (58, 'cap1', 10);
INSERT INTO `usuarioplanes` VALUES (59, 'cap1', 16);
INSERT INTO `usuarioplanes` VALUES (60, 'cap1', 17);
INSERT INTO `usuarioplanes` VALUES (61, 'cap1', 19);
INSERT INTO `usuarioplanes` VALUES (62, 'cap1', 22);
INSERT INTO `usuarioplanes` VALUES (63, 'cap1', 15);
INSERT INTO `usuarioplanes` VALUES (65, 'David Sánchez P', 13);
INSERT INTO `usuarioplanes` VALUES (68, 'enlace-beta', 29);
INSERT INTO `usuarioplanes` VALUES (69, 'lider-beta', 29);
INSERT INTO `usuarioplanes` VALUES (70, 'acuerdos', 8);

-- ----------------------------
-- Table structure for usuarios
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios`  (
  `user` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `clave` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `idRol` int(255) NOT NULL,
  `nombre` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `apellidoP` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `apellidoM` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `telefono` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `celular` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `puesto` varchar(35) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fecha` date NOT NULL,
  `correo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` int(1) NOT NULL,
  `acuerdos` bit(1) NOT NULL,
  PRIMARY KEY (`user`) USING BTREE,
  INDEX `fk_usuario_rol`(`idRol`) USING BTREE,
  CONSTRAINT `fk_usuario_rol` FOREIGN KEY (`idRol`) REFERENCES `rol` (`idRol`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of usuarios
-- ----------------------------
INSERT INTO `usuarios` VALUES ('acuerdos', '123456', 3, 'Acuerdos', 'Acuerdos', 'Acuerdos', '2222', '2222', 'Acuerdos', '2019-02-12', 'mail@mail.com', 1, b'1');
INSERT INTO `usuarios` VALUES ('BernardoLópez  ', 'bernardolopez', 2, 'Bernardo ', 'López ', 'Villaseñor', '4491725282', '4491725282', 'Coordinador de Analisis Político', '2019-01-30', 'bernardo.lopez@aguascalientes.gob.mx', 1, b'1');
INSERT INTO `usuarios` VALUES ('cap1', 'cap1', 4, 'Capturista', 'Paterno', 'Materno', '2222', '2222', 'Puesto', '0000-00-00', 'correo@correo.com', 1, b'1');
INSERT INTO `usuarios` VALUES ('David Sánchez P', 'davidsanchez', 3, 'David', ' Sánchez ', 'Padilla', '4421425653', '4421425653', 'LIDER SITMA-EJES', '2019-02-12', 'david.sanchez@aguascalientes.gob.mx', 1, b'1');
INSERT INTO `usuarios` VALUES ('EderNoda', 'edernoda', 2, 'Eder ', 'Noda ', 'Ramírez ', '2281821080', '2281821080', 'Director  Gral. de Monitoreo de  P.', '2019-01-30', 'eder.noda@aguascalientes.gob.mx', 1, b'1');
INSERT INTO `usuarios` VALUES ('enlace-beta', '123456', 4, 'Enlace nombre', 'Enlace paterno', 'Enlace paterno', '2222', '222', 'Enlace puesto', '2019-02-12', 'beta@beta.com', 1, b'1');
INSERT INTO `usuarios` VALUES ('FernandaNavarro', 'fernandanavarro', 3, 'María Fernanda  ', 'Navarro ', 'Fernández', '4498943805', '4498943805', 'jefe del Departamento de Informació', '2019-01-30', 'fernanda.navarrof@gmail.com', 1, b'1');
INSERT INTO `usuarios` VALUES ('gob', '1234', 1, 'Gobernador', '', '', '', '', '', '0000-00-00', '', 1, b'1');
INSERT INTO `usuarios` VALUES ('Jorge.Uria', 'jorgeuria', 1, 'Jorge ', 'Uria ', 'Adame', '4498078108', '4498078108', 'Coordinador de Evaluación y Monitor', '2019-02-12', 'jorge.uria@aguascalientes.gob.mx', 1, b'1');
INSERT INTO `usuarios` VALUES ('JuanBarcenas ', 'toñobarcenas', 2, 'Juan Antonio', 'Barcenas ', '.', '4491271050', '4491271050', 'Control de información estratégica ', '2019-01-30', 'juan.barcenas@aguascalientes.gob.mx', 1, b'1');
INSERT INTO `usuarios` VALUES ('kevinreyes', 'kevinreyes', 2, 'Kevin Ruben', 'Reyes ', 'Viramontes ', '4491369864', '4494927526', 'Jefatura del departamento de admini', '2019-01-30', 'kevin.reyes@aguascalientes.gob.mx', 1, b'1');
INSERT INTO `usuarios` VALUES ('lider', '123456', 3, 'Lider', 'OK', 'ok', '4444', '12222', 'ok', '2019-01-16', 'ok@mail.com', 1, b'1');
INSERT INTO `usuarios` VALUES ('lider-beta', '123456', 3, 'Nombre', 'Paterno', 'Materno', '2222', '2222', 'Puesto', '2019-02-12', 'mail@mail.com', 1, b'1');
INSERT INTO `usuarios` VALUES ('lider001', 'lider001', 3, 'Marco', 'Antonio', 'Antonio', '2221346270', '2221346270', '1', '2019-02-12', 'ing.marco.cardona@gmail.com', 1, b'1');
INSERT INTO `usuarios` VALUES ('lider1', '123456', 3, 'Juan', 'Paterno', 'Materno', '2222', '2222', 'Puesto', '2019-02-07', 'correo@correo.com', 0, b'1');
INSERT INTO `usuarios` VALUES ('lidera', '123456', 3, 'Nombre', 'Paterno', 'Materno', '22222', '22222', 'Puesto', '2019-02-12', 'correo@mail.com', 0, b'1');
INSERT INTO `usuarios` VALUES ('RocioGonzalez ', 'RocioGonzalez', 3, 'Rocio ', 'Gonzalez ', 'Galvan', '4494159449', '4494159449', 'Directora General De Enlace Con Gab', '2019-01-30', 'rocio.gonzalezg@aguascalientes.gob.mx', 1, b'1');
INSERT INTO `usuarios` VALUES ('sadmin', 'sadmin', 2, 'Superadmin', '', '', '', '', '', '0000-00-00', '', 1, b'1');
INSERT INTO `usuarios` VALUES ('superadmin-a', '123456', 1, 'Beta', 'Beta Paterno', 'Beta Materno', '2222', '2222', 'Beta Puesto', '2019-02-12', 'beta@mail.com', 1, b'1');
INSERT INTO `usuarios` VALUES ('usuario-super-a', '123456', 4, 'Nombre', 'Paterno', 'Materno', '222222', '222222', 'Puesto', '2019-02-09', 'mail@mail.com', 0, b'0');

SET FOREIGN_KEY_CHECKS = 1;
