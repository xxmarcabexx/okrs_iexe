/*
 Navicat Premium Data Transfer

 Source Server         : Localhosto
 Source Server Type    : MySQL
 Source Server Version : 100110
 Source Host           : localhost:3306
 Source Schema         : okrs_iexe

 Target Server Type    : MySQL
 Target Server Version : 100110
 File Encoding         : 65001

 Date: 07/12/2018 01:34:19
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for indicadores_temp
-- ----------------------------
DROP TABLE IF EXISTS `indicadores_temp`;
CREATE TABLE `indicadores_temp`  (
  `idIndicadores` int(255) NOT NULL,
  `inicio` float(10, 2) NOT NULL,
  `final` float(10, 2) NOT NULL,
  `status` int(1) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of indicadores_temp
-- ----------------------------
INSERT INTO `indicadores_temp` VALUES (11, 12.00, 1232.00, 1, '2018-12-06 21:59:00.000000');
INSERT INTO `indicadores_temp` VALUES (11, 12.00, 1232.00, 1, '2018-12-07 21:59:00.000000');

SET FOREIGN_KEY_CHECKS = 1;
