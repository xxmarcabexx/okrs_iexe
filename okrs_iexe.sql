/*
 Navicat Premium Data Transfer

 Source Server         : Developer
 Source Server Type    : MySQL
 Source Server Version : 100137
 Source Host           : localhost:3306
 Source Schema         : okrs_iexe

 Target Server Type    : MySQL
 Target Server Version : 100137
 File Encoding         : 65001

 Date: 12/02/2019 10:50:24
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for acciones
-- ----------------------------
DROP TABLE IF EXISTS `acciones`;
CREATE TABLE `acciones`  (
  `idAcciones` int(11) NOT NULL AUTO_INCREMENT,
  `idKr` int(11) NOT NULL,
  `accion` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `status` bit(1) NOT NULL,
  PRIMARY KEY (`idAcciones`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 44 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of acciones
-- ----------------------------
INSERT INTO `acciones` VALUES (41, 78, 'Test', 10.00, b'1');
INSERT INTO `acciones` VALUES (42, 78, 'Test 1', 0.00, b'1');
INSERT INTO `acciones` VALUES (43, 79, 'Test 2.2', 100.00, b'1');

-- ----------------------------
-- Table structure for acuerdos
-- ----------------------------
DROP TABLE IF EXISTS `acuerdos`;
CREATE TABLE `acuerdos`  (
  `idAcuerdo` int(255) NOT NULL AUTO_INCREMENT,
  `idMInuta` int(255) NOT NULL,
  `actividad` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `responsable` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fecha` date NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `fechaAlta` date NOT NULL,
  PRIMARY KEY (`idAcuerdo`) USING BTREE,
  INDEX `fk_acuerdo_minuta`(`idMInuta`) USING BTREE,
  CONSTRAINT `fk_acuerdo_minuta` FOREIGN KEY (`idMInuta`) REFERENCES `minuta` (`idMinuta`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 88 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of acuerdos
-- ----------------------------
INSERT INTO `acuerdos` VALUES (85, 69, 'Nueva actividad', 'Marco', '2019-01-01', 10.00, '2019-02-08');
INSERT INTO `acuerdos` VALUES (86, 69, 'Activdad de base', 'Marco', '2019-01-01', 10.00, '2019-02-08');
INSERT INTO `acuerdos` VALUES (87, 71, '1', '1', '2019-01-01', 0.00, '2019-02-11');

-- ----------------------------
-- Table structure for bitacoraacciones
-- ----------------------------
DROP TABLE IF EXISTS `bitacoraacciones`;
CREATE TABLE `bitacoraacciones`  (
  `idBitacora` int(255) NOT NULL AUTO_INCREMENT,
  `idKeyResult` int(255) NOT NULL,
  `descripcion` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ultimoAvance` float(10, 2) NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `aprobado` int(1) NOT NULL,
  `fecha` datetime(0) NOT NULL,
  `user` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `motivo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `userNoAutorizo` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `userAprobado` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`idBitacora`) USING BTREE,
  INDEX `fk_bitacora_kr`(`idKeyResult`) USING BTREE,
  INDEX `fk_bitacora_kr_user`(`user`) USING BTREE,
  INDEX `bitaccion_fk_userno`(`userNoAutorizo`) USING BTREE,
  INDEX `bitaccion_fk_userauto`(`userAprobado`) USING BTREE,
  CONSTRAINT `bitaccion_fk_userauto` FOREIGN KEY (`userAprobado`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `bitaccion_fk_userno` FOREIGN KEY (`userNoAutorizo`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `bitacoraacciones_ibfk_1` FOREIGN KEY (`idKeyResult`) REFERENCES `acciones` (`idAcciones`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `bitacoraacciones_ibfk_2` FOREIGN KEY (`user`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 224 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of bitacoraacciones
-- ----------------------------
INSERT INTO `bitacoraacciones` VALUES (215, 41, 'Test', 0.00, 10.00, 1, '2019-02-06 18:47:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacciones` VALUES (216, 42, 'Test', 0.00, 40.00, 1, '2019-02-06 18:47:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacciones` VALUES (217, 43, 'Test', 0.00, 50.00, 1, '2019-02-06 18:47:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacciones` VALUES (221, 41, 'Test', 10.00, 25.00, 0, '2019-02-07 10:33:00', 'cap1', NULL, NULL, NULL);
INSERT INTO `bitacoraacciones` VALUES (222, 43, 'Test', 0.00, 10.00, 1, '2019-02-07 17:20:00', 'sadmin', NULL, NULL, NULL);
INSERT INTO `bitacoraacciones` VALUES (223, 43, 'Test', 10.00, 100.00, 1, '2019-02-07 17:20:00', 'sadmin', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for bitacoraacuerdo
-- ----------------------------
DROP TABLE IF EXISTS `bitacoraacuerdo`;
CREATE TABLE `bitacoraacuerdo`  (
  `idBitacoraGral` int(255) NOT NULL AUTO_INCREMENT,
  `idAcuerdo` int(255) NOT NULL,
  `descripcion` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ultimoAvance` float(10, 2) NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `aprobado` int(1) NOT NULL,
  `fecha` datetime(0) NOT NULL,
  `user` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `motivo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `userNoAutorizo` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `userAprobado` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`idBitacoraGral`) USING BTREE,
  INDEX `fk_bitacora_acuerdo`(`idAcuerdo`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of bitacoraacuerdo
-- ----------------------------
INSERT INTO `bitacoraacuerdo` VALUES (21, 83, 'Test', 0.00, 10.00, 1, '2019-02-08 16:42:00', 'sadmin', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for bitacoraindicadores
-- ----------------------------
DROP TABLE IF EXISTS `bitacoraindicadores`;
CREATE TABLE `bitacoraindicadores`  (
  `idBitacora` int(255) NOT NULL AUTO_INCREMENT,
  `idIndicador` int(255) NOT NULL,
  `descripcion` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ultimoAvance` float(10, 2) NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `aprobado` int(1) NOT NULL,
  `fecha` datetime(0) NOT NULL,
  `user` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `motivo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `userNoAutorizo` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `userAprobado` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`idBitacora`) USING BTREE,
  INDEX `fk_bitacora_kr`(`idIndicador`) USING BTREE,
  INDEX `bitacoraIndicador_user_fk`(`user`) USING BTREE,
  INDEX `fk_user_aprobado`(`userAprobado`) USING BTREE,
  INDEX `fk_user_noautorizo`(`userNoAutorizo`) USING BTREE,
  CONSTRAINT `bitacoraIndicador_user_fk` FOREIGN KEY (`user`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `bitacoraindicadores_ibfk_1` FOREIGN KEY (`idIndicador`) REFERENCES `indicadores` (`idIndicadores`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_user_aprobado` FOREIGN KEY (`userAprobado`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_user_noautorizo` FOREIGN KEY (`userNoAutorizo`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 77 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of bitacoraindicadores
-- ----------------------------
INSERT INTO `bitacoraindicadores` VALUES (72, 65, 'Test', 0.00, 15.00, 1, '2019-02-08 21:54:00', 'cap1', NULL, NULL, 'sadmin');
INSERT INTO `bitacoraindicadores` VALUES (73, 65, 'Test', 15.00, 16.00, 1, '2019-02-08 21:57:00', 'cap1', NULL, NULL, 'sadmin');
INSERT INTO `bitacoraindicadores` VALUES (74, 66, 'Test', 0.00, 50.00, 1, '2019-02-08 22:03:00', 'cap1', NULL, NULL, 'sadmin');
INSERT INTO `bitacoraindicadores` VALUES (75, 67, 'Test', 0.00, 15.00, 2, '2019-02-08 23:45:00', 'cap1', 'ok', 'sadmin', NULL);
INSERT INTO `bitacoraindicadores` VALUES (76, 65, 'Nuevo de bit', 16.00, 61.00, 0, '2019-02-11 18:09:00', 'lider', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for bitacorakeyresult
-- ----------------------------
DROP TABLE IF EXISTS `bitacorakeyresult`;
CREATE TABLE `bitacorakeyresult`  (
  `idBitacora` int(255) NOT NULL AUTO_INCREMENT,
  `idKeyResult` int(255) NOT NULL,
  `descripcion` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ultimoAvance` float(10, 2) NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `aprobado` int(1) NOT NULL,
  `fecha` datetime(0) NOT NULL,
  `user` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `motivo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `userNoAutorizo` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `userAprobado` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`idBitacora`) USING BTREE,
  INDEX `fk_bitacora_kr`(`idKeyResult`) USING BTREE,
  INDEX `fk_bitacora_kr_user`(`user`) USING BTREE,
  INDEX `fk_userNoautorizo_bkr`(`userNoAutorizo`) USING BTREE,
  INDEX `fk_userAutorizo_bkr`(`userAprobado`) USING BTREE,
  CONSTRAINT `fk_bitacora_kr` FOREIGN KEY (`idKeyResult`) REFERENCES `keyresult` (`idKeyResult`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_bitacora_kr_user` FOREIGN KEY (`user`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_userAutorizo_bkr` FOREIGN KEY (`userAprobado`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_userNoautorizo_bkr` FOREIGN KEY (`userNoAutorizo`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 160 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of bitacorakeyresult
-- ----------------------------
INSERT INTO `bitacorakeyresult` VALUES (158, 78, 'Test', 0.00, 50.00, 1, '2019-02-05 19:18:00', 'cap1', NULL, NULL, NULL);
INSERT INTO `bitacorakeyresult` VALUES (159, 80, 'Test', 0.00, 12.00, 1, '2019-02-08 21:55:00', 'cap1', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for bitacoramovimientos
-- ----------------------------
DROP TABLE IF EXISTS `bitacoramovimientos`;
CREATE TABLE `bitacoramovimientos`  (
  `idBitacora` int(255) NOT NULL AUTO_INCREMENT,
  `movimiento` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `usuario` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fecha` date NOT NULL,
  `hora` time(0) NOT NULL,
  PRIMARY KEY (`idBitacora`) USING BTREE,
  INDEX `fk_bitacora_usuario`(`usuario`) USING BTREE,
  CONSTRAINT `fk_bitacora_usuario` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 161 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of bitacoramovimientos
-- ----------------------------
INSERT INTO `bitacoramovimientos` VALUES (126, 'Alta de nuevo plan', 'sadmin', '2019-02-05', '11:07:00');
INSERT INTO `bitacoramovimientos` VALUES (127, 'Alta de objetivo', 'sadmin', '2019-02-05', '18:32:00');
INSERT INTO `bitacoramovimientos` VALUES (128, 'Alta de objetivo', 'sadmin', '2019-02-05', '18:32:00');
INSERT INTO `bitacoramovimientos` VALUES (129, 'Alta de usuario', 'sadmin', '2019-02-05', '11:36:00');
INSERT INTO `bitacoramovimientos` VALUES (130, 'Edicio de usuario: cap1', 'sadmin', '2019-02-05', '18:15:00');
INSERT INTO `bitacoramovimientos` VALUES (131, 'Alta de nuevo plan', 'sadmin', '2019-02-07', '11:18:00');
INSERT INTO `bitacoramovimientos` VALUES (132, 'Alta de usuario: lider01', 'sadmin', '2019-02-07', '12:05:00');
INSERT INTO `bitacoramovimientos` VALUES (133, 'Alta de objetivo', 'lider01', '2019-02-07', '23:04:00');
INSERT INTO `bitacoramovimientos` VALUES (134, 'Alta de usuario', 'lider01', '2019-02-07', '16:04:00');
INSERT INTO `bitacoramovimientos` VALUES (135, 'Alta de usuario: lider02', 'sadmin', '2019-02-07', '16:08:00');
INSERT INTO `bitacoramovimientos` VALUES (136, 'Alta de usuario: maria11', 'sadmin', '2019-02-08', '21:01:00');
INSERT INTO `bitacoramovimientos` VALUES (137, 'Alta de usuario: admin11', 'sadmin', '2019-02-08', '21:01:00');
INSERT INTO `bitacoramovimientos` VALUES (138, 'Alta de usuario: bety11', 'sadmin', '2019-02-08', '21:03:00');
INSERT INTO `bitacoramovimientos` VALUES (139, 'Alta de usuario: gob1243', 'sadmin', '2019-02-08', '21:05:00');
INSERT INTO `bitacoramovimientos` VALUES (140, 'Alta de usuario: gob12435', 'sadmin', '2019-02-08', '21:06:00');
INSERT INTO `bitacoramovimientos` VALUES (141, 'Alta de usuario: unouno', 'sadmin', '2019-02-08', '21:08:00');
INSERT INTO `bitacoramovimientos` VALUES (142, 'Alta de usuario: unouno', 'sadmin', '2019-02-08', '21:09:00');
INSERT INTO `bitacoramovimientos` VALUES (143, 'Alta de usuario: unouno', 'sadmin', '2019-02-08', '21:10:00');
INSERT INTO `bitacoramovimientos` VALUES (144, 'Alta de usuario: unouno', 'sadmin', '2019-02-08', '21:11:00');
INSERT INTO `bitacoramovimientos` VALUES (145, 'Edicio de usuario: cap1', 'sadmin', '2019-02-08', '21:18:00');
INSERT INTO `bitacoramovimientos` VALUES (146, 'Alta de usuario: unouno', 'sadmin', '2019-02-08', '21:18:00');
INSERT INTO `bitacoramovimientos` VALUES (147, 'Alta de usuario: unouno', 'sadmin', '2019-02-08', '21:18:00');
INSERT INTO `bitacoramovimientos` VALUES (148, 'Alta de usuario: unouno', 'sadmin', '2019-02-08', '21:21:00');
INSERT INTO `bitacoramovimientos` VALUES (149, 'Edicio de usuario: unouno', 'sadmin', '2019-02-08', '21:22:00');
INSERT INTO `bitacoramovimientos` VALUES (150, 'Edicio de usuario: unouno', 'sadmin', '2019-02-08', '21:22:00');
INSERT INTO `bitacoramovimientos` VALUES (151, 'Edicio de usuario: unouno', 'sadmin', '2019-02-08', '21:24:00');
INSERT INTO `bitacoramovimientos` VALUES (152, 'Edicio de usuario: unouno', 'sadmin', '2019-02-08', '21:24:00');
INSERT INTO `bitacoramovimientos` VALUES (153, 'Edicio de usuario: unouno', 'sadmin', '2019-02-08', '21:25:00');
INSERT INTO `bitacoramovimientos` VALUES (154, 'Edicio de usuario: cap1', 'sadmin', '2019-02-08', '21:55:00');
INSERT INTO `bitacoramovimientos` VALUES (155, 'Alta de usuario: mimimi', 'sadmin', '2019-02-08', '23:01:00');
INSERT INTO `bitacoramovimientos` VALUES (156, 'Edicio de usuario: unouno', 'sadmin', '2019-02-08', '23:03:00');
INSERT INTO `bitacoramovimientos` VALUES (157, 'Alta de nuevo plan', 'sadmin', '2019-02-08', '23:05:00');
INSERT INTO `bitacoramovimientos` VALUES (158, 'Alta de objetivo', 'sadmin', '2019-02-09', '06:13:00');
INSERT INTO `bitacoramovimientos` VALUES (159, 'Alta de usuario', 'sadmin', '2019-02-08', '23:13:00');
INSERT INTO `bitacoramovimientos` VALUES (160, 'Edicio de usuario: sadmin', 'sadmin', '2019-02-11', '11:52:00');

-- ----------------------------
-- Table structure for chat
-- ----------------------------
DROP TABLE IF EXISTS `chat`;
CREATE TABLE `chat`  (
  `idChat` int(255) NOT NULL AUTO_INCREMENT,
  `mensaje` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `idUsuario` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `tipo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `idTipo` int(255) NOT NULL,
  `fechahora` datetime(0) NOT NULL,
  PRIMARY KEY (`idChat`) USING BTREE,
  INDEX `fk_chat_usuario`(`idUsuario`) USING BTREE,
  CONSTRAINT `fk_chat_usuario` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 113 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of chat
-- ----------------------------
INSERT INTO `chat` VALUES (112, 'Chat de prueba', 'sadmin', 'objetivo', 62, '2019-02-05 19:11:00');

-- ----------------------------
-- Table structure for indicadores
-- ----------------------------
DROP TABLE IF EXISTS `indicadores`;
CREATE TABLE `indicadores`  (
  `idIndicadores` int(255) NOT NULL AUTO_INCREMENT,
  `nombreIndicador` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `inicio` float(10, 2) NOT NULL,
  `final` float(10, 2) NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `idPlan` int(255) NOT NULL,
  `status` bit(1) NOT NULL,
  `avancePorcentaje` float(10, 2) NOT NULL,
  PRIMARY KEY (`idIndicadores`) USING BTREE,
  INDEX `fk_indicador_plan`(`idPlan`) USING BTREE,
  CONSTRAINT `fk_indicador_plan` FOREIGN KEY (`idPlan`) REFERENCES `plan` (`idMv`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 89 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of indicadores
-- ----------------------------
INSERT INTO `indicadores` VALUES (65, 'Avance Financiero', 10.00, 100.00, 16.00, 9, b'0', 16.00);
INSERT INTO `indicadores` VALUES (66, 'Avance Físico', 10.00, 100.00, 50.00, 9, b'0', 50.00);
INSERT INTO `indicadores` VALUES (67, 'Gestión de Inversión', 10.00, 100.00, 0.00, 9, b'0', 0.00);
INSERT INTO `indicadores` VALUES (68, 'Efectividad Financiera', 10.00, 100.00, 0.00, 9, b'0', 0.00);
INSERT INTO `indicadores` VALUES (69, 'Efectividad Administrativa', 1.00, 100.00, 0.00, 9, b'0', 0.00);
INSERT INTO `indicadores` VALUES (70, 'Efectividad Jurídica', 1.00, 100.00, 0.00, 9, b'0', 0.00);
INSERT INTO `indicadores` VALUES (71, 'Resprogramación Presupuestaria', 1.00, 100.00, 0.00, 9, b'0', 0.00);
INSERT INTO `indicadores` VALUES (72, 'Satisfacción Ciudadana', 10.00, 100.00, 0.00, 9, b'0', 0.00);
INSERT INTO `indicadores` VALUES (73, 'Avance Financiero', 1.00, 10.00, 0.00, 10, b'0', 0.00);
INSERT INTO `indicadores` VALUES (74, 'Avance Físico', 1.00, 10.00, 0.00, 10, b'0', 0.00);
INSERT INTO `indicadores` VALUES (75, 'Gestión de Inversión', 1.00, 10.00, 0.00, 10, b'0', 0.00);
INSERT INTO `indicadores` VALUES (76, 'Efectividad Financiera', 1.00, 10.00, 0.00, 10, b'0', 0.00);
INSERT INTO `indicadores` VALUES (77, 'Efectividad Administrativa', 1.00, 10.00, 0.00, 10, b'0', 0.00);
INSERT INTO `indicadores` VALUES (78, 'Efectividad Jurídica', 1.00, 10.00, 0.00, 10, b'0', 0.00);
INSERT INTO `indicadores` VALUES (79, 'Resprogramación Presupuestaria', 1.00, 10.00, 0.00, 10, b'0', 0.00);
INSERT INTO `indicadores` VALUES (80, 'Satisfacción Ciudadana', 1.00, 10.00, 0.00, 10, b'0', 0.00);
INSERT INTO `indicadores` VALUES (81, 'Avance Financiero', 1.00, 2.00, 0.00, 11, b'0', 0.00);
INSERT INTO `indicadores` VALUES (82, 'Avance Físico', 1.00, 2.00, 0.00, 11, b'0', 0.00);
INSERT INTO `indicadores` VALUES (83, 'Gestión de Inversión', 1.00, 2.00, 0.00, 11, b'0', 0.00);
INSERT INTO `indicadores` VALUES (84, 'Efectividad Financiera', 1.00, 2.00, 0.00, 11, b'0', 0.00);
INSERT INTO `indicadores` VALUES (85, 'Efectividad Administrativa', 1.00, 2.00, 0.00, 11, b'0', 0.00);
INSERT INTO `indicadores` VALUES (86, 'Efectividad Jurídica', 1.00, 2.00, 0.00, 11, b'0', 0.00);
INSERT INTO `indicadores` VALUES (87, 'Resprogramación Presupuestaria', 1.00, 2.00, 0.00, 11, b'0', 0.00);
INSERT INTO `indicadores` VALUES (88, 'Satisfacción Ciudadana', 1.00, 2.00, 0.00, 11, b'0', 0.00);

-- ----------------------------
-- Table structure for indicadores_temp
-- ----------------------------
DROP TABLE IF EXISTS `indicadores_temp`;
CREATE TABLE `indicadores_temp`  (
  `idIndicadores` int(255) NOT NULL,
  `inicio` float(10, 2) NOT NULL,
  `final` float(10, 2) NOT NULL,
  `status` int(1) NOT NULL,
  `fecha` datetime(0) NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for indicadorespdfs
-- ----------------------------
DROP TABLE IF EXISTS `indicadorespdfs`;
CREATE TABLE `indicadorespdfs`  (
  `idPdfs` int(255) NOT NULL AUTO_INCREMENT,
  `idBitacoraIndicador` int(255) NOT NULL,
  `file` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `estatus` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `idIndicador` int(255) NOT NULL,
  PRIMARY KEY (`idPdfs`) USING BTREE,
  INDEX `fk_pdf_bitacora_kr`(`idBitacoraIndicador`) USING BTREE,
  INDEX `fk_pdf_kr`(`idIndicador`) USING BTREE,
  CONSTRAINT `indicadorespdfs_idBitacoraIndicador` FOREIGN KEY (`idBitacoraIndicador`) REFERENCES `bitacoraindicadores` (`idBitacora`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `indicadorespdfs_idIndicador` FOREIGN KEY (`idIndicador`) REFERENCES `indicadores` (`idIndicadores`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for keyresult
-- ----------------------------
DROP TABLE IF EXISTS `keyresult`;
CREATE TABLE `keyresult`  (
  `idKeyResult` int(255) NOT NULL AUTO_INCREMENT,
  `idObjetivo` int(255) NOT NULL,
  `descripcion` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `metrica` varchar(35) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `orden` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `medicioncomienzo` float(10, 2) NOT NULL,
  `medicionfinal` float(10, 2) NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `estado` bit(1) NOT NULL,
  `avancePorcentaje` float(10, 2) NOT NULL,
  PRIMARY KEY (`idKeyResult`) USING BTREE,
  INDEX `kf_kr_objetivos`(`idObjetivo`) USING BTREE,
  CONSTRAINT `kf_kr_objetivos` FOREIGN KEY (`idObjetivo`) REFERENCES `objetivos` (`idObjetivo`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 83 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of keyresult
-- ----------------------------
INSERT INTO `keyresult` VALUES (78, 62, 'Kr de prueba', 'Porcentaje', 'Ascendente', 0.00, 100.00, 10.00, b'1', 10.00);
INSERT INTO `keyresult` VALUES (79, 62, 'Key resutl test prueba', 'Porcentaje', 'Ascendente', 1.00, 100.00, 100.00, b'1', 100.00);
INSERT INTO `keyresult` VALUES (80, 63, 'Nueva', 'Porcentaje', 'Ascendente', 1.00, 100.00, 12.00, b'1', 12.00);
INSERT INTO `keyresult` VALUES (81, 62, 'Nueva', 'Financiero', 'Ascendente', 1.00, 100.00, 0.00, b'1', 0.00);
INSERT INTO `keyresult` VALUES (82, 65, 'as', 'Dicotomica', 's/o', 1.00, 100.00, 0.00, b'1', 0.00);

-- ----------------------------
-- Table structure for krpdfs
-- ----------------------------
DROP TABLE IF EXISTS `krpdfs`;
CREATE TABLE `krpdfs`  (
  `idPdfs` int(255) NOT NULL AUTO_INCREMENT,
  `idBitacoraKr` int(255) NOT NULL,
  `file` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `estatus` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `idKr` int(255) NOT NULL,
  PRIMARY KEY (`idPdfs`) USING BTREE,
  INDEX `fk_pdf_bitacora_kr`(`idBitacoraKr`) USING BTREE,
  INDEX `fk_pdf_kr`(`idKr`) USING BTREE,
  CONSTRAINT `fk_pdf_bitacora_kr` FOREIGN KEY (`idBitacoraKr`) REFERENCES `bitacorakeyresult` (`idBitacora`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_pdf_kr` FOREIGN KEY (`idKr`) REFERENCES `keyresult` (`idKeyResult`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of krpdfs
-- ----------------------------
INSERT INTO `krpdfs` VALUES (7, 158, 'Cotizacion2019-02-05.pdf', '1', 78);

-- ----------------------------
-- Table structure for minuta
-- ----------------------------
DROP TABLE IF EXISTS `minuta`;
CREATE TABLE `minuta`  (
  `idMinuta` int(255) NOT NULL AUTO_INCREMENT,
  `idPlan` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time(0) NOT NULL,
  `codigo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lugar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `objetivo` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `asunto` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `status` bit(1) NOT NULL,
  `avancePorcentaje` float(10, 2) NOT NULL,
  PRIMARY KEY (`idMinuta`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 73 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of minuta
-- ----------------------------
INSERT INTO `minuta` VALUES (69, 10, '2019-01-01', '11:27:00', '02019-01-01', 'Puebla', 'Objetivo acuerdo de prueba', 'Asunto de prueba', 0.00, b'1', 0.00);
INSERT INTO `minuta` VALUES (71, 9, '2019-01-01', '01:58:00', '02019-01-01', '1', '1', '1', 0.00, b'1', 0.00);
INSERT INTO `minuta` VALUES (72, 9, '0000-00-00', '10:40:00', '02019-01-01', 'Puebla', 'Plan de base ', 'Plan de base', 0.00, b'1', 0.00);

-- ----------------------------
-- Table structure for objetivos
-- ----------------------------
DROP TABLE IF EXISTS `objetivos`;
CREATE TABLE `objetivos`  (
  `idObjetivo` int(255) NOT NULL AUTO_INCREMENT,
  `anual` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `idmv` int(255) NOT NULL,
  `objetivo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `descripcion` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `finicio` date NOT NULL,
  `ffinal` date NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `estado` bit(1) NOT NULL,
  `avancePorcentaje` float(10, 2) NOT NULL,
  PRIMARY KEY (`idObjetivo`) USING BTREE,
  INDEX `fk_objetivos_mv`(`idmv`) USING BTREE,
  CONSTRAINT `fk_objetivos_mv` FOREIGN KEY (`idmv`) REFERENCES `plan` (`idMv`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 66 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of objetivos
-- ----------------------------
INSERT INTO `objetivos` VALUES (62, '1', 9, 'Objetivo de prueba', 'Descripcion de prueba', '2019-02-05', '2019-02-28', 55.00, b'1', 55.00);
INSERT INTO `objetivos` VALUES (63, '0', 10, 'De prueba marco', 'Objetivo de prueba', '2019-02-19', '2019-02-28', 12.00, b'1', 0.00);
INSERT INTO `objetivos` VALUES (64, '0', 10, 'De prueba marco1', 'Objetivo de prueba', '2019-02-19', '2019-02-28', 0.00, b'1', 0.00);
INSERT INTO `objetivos` VALUES (65, '0', 10, 'Obj', 'Obj', '2019-02-04', '2019-02-28', 0.00, b'1', 0.00);

-- ----------------------------
-- Table structure for objetivos_temp
-- ----------------------------
DROP TABLE IF EXISTS `objetivos_temp`;
CREATE TABLE `objetivos_temp`  (
  `idObjetivo` int(255) NOT NULL AUTO_INCREMENT,
  `idmv` int(255) NOT NULL,
  `objetivo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `descripcion` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `finicio` date NOT NULL,
  `ffinal` date NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `estado` bit(1) NOT NULL,
  PRIMARY KEY (`idObjetivo`) USING BTREE,
  INDEX `fk_objetivos_mv`(`idmv`) USING BTREE,
  CONSTRAINT `objetivos_temp_ibfk_1` FOREIGN KEY (`idmv`) REFERENCES `plan` (`idMv`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for pdfminutas
-- ----------------------------
DROP TABLE IF EXISTS `pdfminutas`;
CREATE TABLE `pdfminutas`  (
  `idPdfMinuta` int(255) NOT NULL AUTO_INCREMENT,
  `pdf` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `idMinuta` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`idPdfMinuta`) USING BTREE,
  INDEX `fk_pdf_minuta`(`idMinuta`) USING BTREE,
  CONSTRAINT `fk_pdf_minuta` FOREIGN KEY (`idMinuta`) REFERENCES `minuta` (`idMinuta`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pdfminutas
-- ----------------------------
INSERT INTO `pdfminutas` VALUES (1, 'Cotizacion2019-02-11.pdf', 71);
INSERT INTO `pdfminutas` VALUES (2, 'Curriculum-Marco-Cardona2019-02-12.pdf', 72);

-- ----------------------------
-- Table structure for plan
-- ----------------------------
DROP TABLE IF EXISTS `plan`;
CREATE TABLE `plan`  (
  `idMv` int(255) NOT NULL AUTO_INCREMENT,
  `mv` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `codigo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lider` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `inversionT` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fin` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `poblacionPotencial` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `proposito` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `poblacionObjetivo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `presupuesto2018` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `presupuesto2019` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `indicadorEstrategico` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `inversion2018` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `inversion2019` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `metaIndicador` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `finicial` date NOT NULL,
  `ffinal` date NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `estado` bit(1) NOT NULL,
  `avanceIndicadores` float(10, 2) NOT NULL,
  PRIMARY KEY (`idMv`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of plan
-- ----------------------------
INSERT INTO `plan` VALUES (9, 'Plan de prueba', NULL, 'Activo', 'Marco', '10000', 'Prueba', '1000', 'Proposito del plan', 'Población objetiva del plan', '1', '2', '3', '4', '45', '6', '0000-00-00', '0000-00-00', 0.00, b'1', 8.25);
INSERT INTO `plan` VALUES (10, 'COPIA', NULL, 'COPIA', 'COPIA', 'COPIA', 'COPIA', 'COPIA', 'COPIA', 'COPIA', 'COPIA', 'COPIA', 'COPIA', 'COPIA', 'COPIA', 'COPIA', '0000-00-00', '0000-00-00', 4.00, b'1', 0.00);
INSERT INTO `plan` VALUES (11, 'Uno', NULL, 'Uno', 'Uno', 'Uno', 'Uno', 'v', 'Uno', 'Uno', 'Uno', 'Uno', 'Uno', 'Uno', 'Uno', 'Uno', '0000-00-00', '0000-00-00', 0.00, b'1', 0.00);

-- ----------------------------
-- Table structure for plan_temp
-- ----------------------------
DROP TABLE IF EXISTS `plan_temp`;
CREATE TABLE `plan_temp`  (
  `idMv` int(255) NOT NULL AUTO_INCREMENT,
  `mv` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lider` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `inversionT` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fin` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `poblacionPotencial` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `proposito` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `poblacionObjetivo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `presupuesto2018` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `presupuesto2019` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `indicadorEstrategico` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `inversion2018` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `inversion2019` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `metaIndicador` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `finicial` date NOT NULL,
  `ffinal` date NOT NULL,
  `avance` float(10, 2) NOT NULL,
  `estado` bit(1) NOT NULL,
  PRIMARY KEY (`idMv`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for rol
-- ----------------------------
DROP TABLE IF EXISTS `rol`;
CREATE TABLE `rol`  (
  `idRol` int(255) NOT NULL,
  `rol` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nombrePuesto` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`idRol`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of rol
-- ----------------------------
INSERT INTO `rol` VALUES (1, 'lider', 'Gobernador');
INSERT INTO `rol` VALUES (2, 'sadmin', 'Superadmin');
INSERT INTO `rol` VALUES (3, 'admin', 'Lider');
INSERT INTO `rol` VALUES (4, 'capturista', 'Enlace');

-- ----------------------------
-- Table structure for sesiones
-- ----------------------------
DROP TABLE IF EXISTS `sesiones`;
CREATE TABLE `sesiones`  (
  `idSesiones` int(255) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fecha` datetime(0) NOT NULL,
  `tipo` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`idSesiones`) USING BTREE,
  INDEX `fk_sesiones_usuario`(`usuario`) USING BTREE,
  CONSTRAINT `fk_sesiones_usuario` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 661 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sesiones
-- ----------------------------
INSERT INTO `sesiones` VALUES (591, 'lider01', '2019-02-07 12:05:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (592, 'lider01', '2019-02-07 16:01:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (593, 'sadmin', '2019-02-07 16:07:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (594, 'lider02', '2019-02-07 16:08:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (595, 'lider01', '2019-02-07 16:08:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (596, 'lider01', '2019-02-07 16:15:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (597, 'lider02', '2019-02-07 16:17:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (598, 'cap1', '2019-02-07 17:08:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (599, 'sadmin', '2019-02-07 17:13:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (600, 'cap1', '2019-02-07 17:21:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (601, 'sadmin', '2019-02-07 17:21:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (602, 'gob', '2019-02-07 17:42:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (603, 'gob', '2019-02-07 17:42:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (604, 'sadmin', '2019-02-07 17:46:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (605, 'gob', '2019-02-07 17:46:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (606, 'sadmin', '2019-02-07 18:16:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (607, 'sadmin', '2019-02-08 10:41:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (608, 'cap1', '2019-02-08 12:05:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (609, 'sadmin', '2019-02-08 12:05:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (610, 'cap1', '2019-02-08 12:22:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (611, 'sadmin', '2019-02-08 12:22:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (612, 'cap1', '2019-02-08 12:47:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (613, 'sadmin', '2019-02-08 12:47:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (614, 'cap1', '2019-02-08 13:30:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (615, 'sadmin', '2019-02-08 13:31:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (616, 'gob', '2019-02-08 17:47:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (617, 'gob', '2019-02-08 17:55:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (618, 'sadmin', '2019-02-08 17:57:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (619, 'sadmin', '2019-02-08 17:57:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (620, 'sadmin', '2019-02-08 18:01:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (621, 'gob', '2019-02-08 18:09:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (622, 'gob', '2019-02-08 18:53:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (623, 'sadmin', '2019-02-08 19:18:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (624, 'gob', '2019-02-08 19:19:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (625, 'sadmin', '2019-02-08 19:51:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (626, 'sadmin', '2019-02-08 20:09:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (627, 'cap1', '2019-02-08 21:33:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (628, 'sadmin', '2019-02-08 21:43:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (629, 'cap1', '2019-02-08 21:54:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (630, 'sadmin', '2019-02-08 21:54:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (631, 'cap1', '2019-02-08 21:55:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (632, 'sadmin', '2019-02-08 21:56:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (633, 'cap1', '2019-02-08 21:57:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (634, 'sadmin', '2019-02-08 22:04:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (635, 'sadmin', '2019-02-08 22:10:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (636, 'bety11', '2019-02-08 22:13:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (637, 'bety11', '2019-02-08 22:13:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (638, 'sadmin', '2019-02-08 22:21:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (639, 'sadmin', '2019-02-08 23:28:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (640, 'cap1', '2019-02-08 23:45:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (641, 'sadmin', '2019-02-08 23:45:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (642, 'cap1', '2019-02-08 23:58:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (643, 'mimimi', '2019-02-09 00:04:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (644, 'unouno', '2019-02-09 00:04:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (645, 'bety11', '2019-02-09 00:04:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (646, 'sadmin', '2019-02-09 00:06:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (647, 'cap1', '2019-02-09 00:07:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (648, 'sadmin', '2019-02-09 00:10:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (649, 'cap1', '2019-02-11 10:24:00', 'Inicio de Sesion', '127.0.0.1');
INSERT INTO `sesiones` VALUES (650, 'cap1', '2019-02-11 10:26:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (651, 'cap1', '2019-02-11 11:33:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (652, 'cap1', '2019-02-11 11:47:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (653, 'sadmin', '2019-02-11 11:51:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (654, 'lider', '2019-02-11 13:21:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (655, 'sadmin', '2019-02-11 16:41:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (656, 'cap1', '2019-02-11 16:41:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (657, 'lider', '2019-02-11 16:41:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (658, 'sadmin', '2019-02-12 09:30:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (659, 'cap1', '2019-02-12 10:36:00', 'Inicio de Sesion', '::1');
INSERT INTO `sesiones` VALUES (660, 'sadmin', '2019-02-12 10:36:00', 'Inicio de Sesion', '::1');

-- ----------------------------
-- Table structure for usuariokeyresult
-- ----------------------------
DROP TABLE IF EXISTS `usuariokeyresult`;
CREATE TABLE `usuariokeyresult`  (
  `idRelacion` int(255) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `kr` int(255) NOT NULL,
  PRIMARY KEY (`idRelacion`) USING BTREE,
  INDEX `fk_registro_user`(`usuario`) USING BTREE,
  INDEX `fk_registro_kr`(`kr`) USING BTREE,
  CONSTRAINT `fk_registro_kr` FOREIGN KEY (`kr`) REFERENCES `keyresult` (`idKeyResult`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_registro_user` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 55 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of usuariokeyresult
-- ----------------------------
INSERT INTO `usuariokeyresult` VALUES (53, 'cap1', 78);
INSERT INTO `usuariokeyresult` VALUES (54, 'cap1', 80);

-- ----------------------------
-- Table structure for usuarioplanes
-- ----------------------------
DROP TABLE IF EXISTS `usuarioplanes`;
CREATE TABLE `usuarioplanes`  (
  `idRelacion` int(255) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `plan` int(255) NOT NULL,
  PRIMARY KEY (`idRelacion`) USING BTREE,
  INDEX `fk_registro_user`(`usuario`) USING BTREE,
  INDEX `fk_registro_kr`(`plan`) USING BTREE,
  CONSTRAINT `usuarioplanes_ibfk_1` FOREIGN KEY (`plan`) REFERENCES `plan` (`idMv`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `usuarioplanes_ibfk_2` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`user`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 58 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of usuarioplanes
-- ----------------------------
INSERT INTO `usuarioplanes` VALUES (53, 'lider01', 10);
INSERT INTO `usuarioplanes` VALUES (54, 'lider02', 9);
INSERT INTO `usuarioplanes` VALUES (56, 'lider', 9);
INSERT INTO `usuarioplanes` VALUES (57, 'lider', 10);

-- ----------------------------
-- Table structure for usuarios
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios`  (
  `user` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `clave` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `idRol` int(255) NOT NULL,
  `nombre` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `apellidoP` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `apellidoM` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `telefono` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `celular` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `puesto` varchar(35) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fecha` date NOT NULL,
  `correo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` int(1) NOT NULL,
  `acuerdos` bit(1) NOT NULL,
  PRIMARY KEY (`user`) USING BTREE,
  INDEX `fk_usuario_rol`(`idRol`) USING BTREE,
  CONSTRAINT `fk_usuario_rol` FOREIGN KEY (`idRol`) REFERENCES `rol` (`idRol`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of usuarios
-- ----------------------------
INSERT INTO `usuarios` VALUES ('admin11', 'admin11', 4, 'admin11', 'admin11', 'admin11', 'admin11', '2221346270', 'admin11', '2019-02-08', 'ing.marco.cardona@gmail.com', 1, b'1');
INSERT INTO `usuarios` VALUES ('bety11', 'bety11', 4, 'bety11', 'bety11', 'bety11', 'bety11', 'bety11', 'bety11', '2019-02-08', 'bety11', 1, b'1');
INSERT INTO `usuarios` VALUES ('cap1', 'cap1', 4, 'Marco', 'Antonio', 'Antonio', '2221346270', '2221346270', 'Marco', '0000-00-00', 'ing.marco.cardona@gmail.com', 1, b'1');
INSERT INTO `usuarios` VALUES ('gob', '1234', 1, 'Gobernador', '', '', '', '', '', '0000-00-00', '', 1, b'1');
INSERT INTO `usuarios` VALUES ('gob1243', 'gob1243', 2, 'gob1243', 'gob1243', 'gob1243', 'gob1243', 'gob1243', 'gob1243', '2019-02-08', 'gob1243', 1, b'1');
INSERT INTO `usuarios` VALUES ('gob12435', 'gob12435', 2, 'gob12435', 'gob12435', 'gob12435', 'gob12435', 'gob12435', 'gob12435', '2019-02-08', 'gob12435', 1, b'1');
INSERT INTO `usuarios` VALUES ('lider', 'lider', 3, 'Lider', '', '', '', '', '', '2019-01-16', '', 1, b'1');
INSERT INTO `usuarios` VALUES ('lider01', 'lider01', 3, 'lider01', 'lider01', 'lider01', 'lider01', 'lider01', 'lider01', '2019-02-07', 'lider01', 1, b'1');
INSERT INTO `usuarios` VALUES ('lider02', 'lider02', 3, 'lider02', 'lider02', 'lider02', 'lider02', 'lider02', 'lider02', '2019-02-07', 'lider02', 1, b'1');
INSERT INTO `usuarios` VALUES ('maria11', 'maria11', 1, 'maria11', 'maria11', 'maria11', 'maria11', 'maria11', 'maria11', '2019-02-08', 'maria11', 1, b'1');
INSERT INTO `usuarios` VALUES ('mimimi', 'mimimi', 1, 'mimimi', 'mimimi', 'mimimi', 'mimimi', 'mimimi', 'mimimi', '2019-02-08', 'mimimi', 1, b'1');
INSERT INTO `usuarios` VALUES ('sadmin', 'sadmin', 2, 'Superadmin', 'Superadmin', 'Superadmin', 'Superadmin', 'Superadmin', 'Superadmin', '0000-00-00', 'Superadmin', 1, b'1');
INSERT INTO `usuarios` VALUES ('unouno', 'unouno', 2, 'unouno', 'unouno', 'unouno', 'unouno', 'unouno', 'unouno', '2019-02-08', 'unouno', 1, b'1');

SET FOREIGN_KEY_CHECKS = 1;
